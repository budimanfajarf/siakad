<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailKrs extends Model
{
    protected $table = 'detail_krs';
    protected $guarded = ['id'];

    public function jadwal()
    {
        return $this->belongsTo('App\Models\Jadwal');
    } 
    
    public function krs()
    {
        return $this->belongsTo('App\Models\Krs');
    }
    
    public function nilai_assesment_() 
    {
        return $this->hasMany('App\Models\NilaiAssesment');
    }
    
    // public function getTotalNilaiBobotAttribute()
    // {
    //     return $this->nilai_assesment_->reduce(function ($total, $nilai_assesment) {
    //       return $total + $nilai_assesment->nilai_bobot;
    //     },0);
    // }     
}
