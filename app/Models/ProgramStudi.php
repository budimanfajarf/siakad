<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramStudi extends Model
{
    protected $table = 'program_studi';
    protected $guarded = ['id'];    

    public function fakultas()
    {
        return $this->belongsTo('App\Models\Fakultas');
    }

    public function dosen_() 
    {
        return $this->hasMany('App\Models\Dosen');
    }     

    public function mahasiswa_() 
    {
        return $this->hasMany('App\Models\Mahasiswa');
    } 
    
    public function tahun_akademik_() 
    {
        return $this->hasMany('App\Models\TahunAkademik');
    }     
}
