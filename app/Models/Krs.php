<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Krs extends Model
{
    protected $table = 'krs';
    protected $guarded = ['id'];

    public function mahasiswa()
    {
        return $this->belongsTo('App\Models\Mahasiswa');
    } 
    
    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen');
    } 
    
    public function tahun_akademik()
    {
        return $this->belongsTo('App\Models\TahunAkademik');
    }
    
    public function detail_krs_() 
    {
        return $this->hasMany('App\Models\DetailKrs');
    }     
}
