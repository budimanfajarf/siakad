<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matakuliah extends Model
{
    protected $table = 'matakuliah';
    protected $guarded = ['id'];

    public function learning_outcome_() 
    {
        return $this->hasMany('App\Models\LearningOutcome');
    }    

    public function assesment_()
    {
        return $this->hasManyThrough('App\Models\Assesment', 'App\Models\LearningOutcome');
    }

    public function jadwal_() 
    {
        return $this->hasMany('App\Models\Jadwal');
    }    
    
    public function detail_krs_()
    {
        return $this->hasManyThrough('App\Models\DetailKrs', 'App\Models\Jadwal');
    }    
    
    public function program_studi()
    {
        return $this->belongsTo('App\Models\ProgramStudi');
    }    
    
// ragan
    public function jadwal()
    {
        return $this->hasMany('App\Models\Jadwal');
    }    
}
