<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fakultas extends Model
{
    protected $table = 'fakultas';
    protected $guarded = ['id'];    

    public function program_studi_() 
    {
        return $this->hasMany('App\Models\ProgramStudi');
    }

    public function jenis_matakuliah_() 
    {
        return $this->hasMany('App\Models\JenisMatakuliah');
    }    
}
