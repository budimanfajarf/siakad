<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';
    protected $guarded = ['id'];    

    public function matakuliah()
    {
    	return $this->belongsTo('App\Models\Matakuliah');
    }

    public function tahun_akademik()
    {
    	return $this->belongsTo('App\Models\TahunAkademik');
    }

    public function dosen()
    {
    	return $this->belongsTo('App\Models\Dosen');
    }

    public function ruang()
    {
    	return $this->belongsTo('App\Models\Ruang');
    }

    public function detail_krs()
    {
        return $this->hasMany('App\Models\DetailKrs');
    }
    
    public function detail_krs_() 
    {
        return $this->hasMany('App\Models\DetailKrs');
    }    
     
}