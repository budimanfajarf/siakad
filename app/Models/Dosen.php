<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table = 'dosen';
    protected $guarded = ['id'];     

    public function program_studi()
    {
        return $this->belongsTo('App\Models\ProgramStudi');
    }

    public function jadwal()
    {
    	return $this->hasMany('App\Models\Dosen');
    }  

    public function krs()
    {
        return $this->hasMany('App\Models\Krs');
    }  
}
