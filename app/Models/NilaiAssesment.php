<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NilaiAssesment extends Model
{
    protected $table = 'nilai_assesment';
    protected $guarded = ['id'];
    protected $appends = ['nilai_bobot', 'bobot'];    

    public function detail_krs()
    {
        return $this->belongsTo('App\Models\DetailKrs');
    } 
    
    public function assesment()
    {
        return $this->belongsTo('App\Models\Assesment');
    } 
    
    public function getNilaiBobotAttribute()
    {
        return $this->nilai * ($this->assesment->bobot / 100);
    }

    public function getBobotAttribute()
    {
        return $this->assesment->bobot;
    }
}
