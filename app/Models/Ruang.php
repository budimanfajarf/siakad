<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ruang extends Model
{
    protected $table = 'ruang';
    protected $guarded = ['id'];    

    public function jadwal()
    {
        return $this->hasMany('App\Models\Jadwal');
    }    
}