<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assesment extends Model
{
    protected $table = 'assesment';
    protected $guarded = ['id'];     

    public function learning_outcome()
    {
        return $this->belongsTo('App\Models\LearningOutcome');
    }

    public function nilai_assesment_() 
    {
        return $this->hasMany('App\Models\NilaiAssesment');
    }     
}
