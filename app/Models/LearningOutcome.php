<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LearningOutcome extends Model
{
    protected $table = 'learning_outcome';
    protected $guarded = ['id'];

    public function matakuliah()
    {
        return $this->belongsTo('App\Models\Matakuliah');
    }    

    public function study_outcome()
    {
        return $this->belongsTo('App\Models\StudyOutcome');
    }   
    
    public function assesment_() 
    {
        return $this->hasMany('App\Models\Assesment');
    }      
}
