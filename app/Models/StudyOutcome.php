<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudyOutcome extends Model
{
    protected $table = 'study_outcome';
    protected $guarded = ['id'];

    public function learning_outcome_() 
    {
        return $this->hasMany('App\Models\LearningOutcome');
    }
}
