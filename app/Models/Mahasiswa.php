<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';
    protected $guarded = ['id'];

    public function program_studi()
    {
        return $this->belongsTo('App\Models\ProgramStudi');
    }  

    public function krs()
    {
    	return $this->hasMany('App\Models\Krs');
    }  

    public function detail_krs_()
    {
        return $this->hasManyThrough('App\Models\DetailKrs', 'App\Models\Krs');
    }     
}
