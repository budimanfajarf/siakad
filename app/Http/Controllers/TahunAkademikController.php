<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\TahunAkademik;
use DataTables;
use Validator;

class TahunAkademikController extends Controller
{
    public function index()
    {
        return view('tahun-akademik.index');
    }

    public function store(Request $request)
    {        
        $validation = Validator::make($request->all(), [          
            'tanggal_awal' => 'required',
            'tanggal_akhir' => 'required',
        ]);
          
        if ($validation->passes())
        {
            $nama  = date('Y', strtotime($request->tanggal_awal));
            $nama .= '/';
            $nama .= date('Y', strtotime($request->tanggal_akhir));
            $tahun_akademik = TahunAkademik::create([          
                'nama' => $nama,
                'tanggal_awal' => $request->tanggal_awal,
                'tanggal_akhir' => $request->tanggal_akhir,                                
                'program_studi_id' => 1,         
            ]);
    
            return response()->json([
                'data'    => $tahun_akademik,
                'message' => 'Data Tahun Akademik berhasil ditambah',
            ], 201);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);  
    }  
    
    public function edit($id)
    {
        $tahun_akademik=TahunAkademik::find($id);
        return $tahun_akademik;
    }

    public function update(Request $request, $id)
    {
        $tahun_akademik = TahunAkademik::findOrFail($id);

        $validation = Validator::make($request->all(), [
            'tanggal_awal' => 'required',
            'tanggal_akhir' => 'required',
        ]);        
        
        if ($validation->passes())
        {
            $nama  = date('Y', strtotime($request->tanggal_awal));
            $nama .= '/';
            $nama .= date('Y', strtotime($request->tanggal_akhir));

            $tahun_akademik->update([
                'nama' => $nama,
                'tanggal_awal' => $request->tanggal_awal,
                'tanggal_akhir' => $request->tanggal_akhir,                                
                'program_studi_id' => 1,         
            ]);
    
            return response()->json([
                'data'    => $tahun_akademik,
                'message' => 'Data Tahun Akademik berhasil diupdate',                
            ]);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);         
    }

    public function destroy($id)
    {
        $tahun_akademik = TahunAkademik::findOrFail($id);
        $errors = [];

        // if ($tahun_akademik->relations_()->count())
        // {
        //     $error = 'Data Tahun Akademik ini tidak bisa dihapus karena digunakan oleh data tahun_akademik';
        //     $errors[] = $error;
        // }

        // if ($errors) 
        // {
        //     return response()->json([
        //         'errors' => $errors
        //     ], 422);
        // } 
        // else {
            $tahun_akademik->delete();
            return response()->json([
                'message' => 'Data Tahun Akademik berhasil dihapus'
            ]);
        // }
    }

    public function data()
    {
        $tahun_akademik = TahunAkademik::query();

        return DataTables::of($tahun_akademik)       
            ->addColumn('action', function($tahun_akademik){
                return  '<a id="'.$tahun_akademik->id.'" onclick="editForm('.$tahun_akademik->id.')" class="btn btn-info waves-effect"><i class="material-icons">mode_edit</i></a> '.
                        '<a id="'.$tahun_akademik->id.'" onclick="deleteData('.$tahun_akademik->id.')" class="btn btn-danger waves-effect"><i class="material-icons">delete</i></a>';
            })        
            ->make(true);        
    }    
}
