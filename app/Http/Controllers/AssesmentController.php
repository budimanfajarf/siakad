<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\Assesment;
use DataTables;
use Validator;

class AssesmentController extends Controller
{    
    public function store(Request $request)
    {
        $this->validate($request, [
            'learning_outcome_id' => 'required',
            'metode' => 'required',
        ]);

        $assesment = Assesment::create([
            'learning_outcome_id' => $request->learning_outcome_id,
            'metode' => $request->metode,        
        ]);        

        return redirect('matakuliah/'.$request->matakuliah_id.'/learning-outcome')->with('message', 'Assesment berhasil ditambahkan');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'learning_outcome_id' => 'required',
            'metode' => 'required',
        ]);

        $assesment = Assesment::findOrFail($id);

        $assesment->update([
            'learning_outcome_id' => $request->learning_outcome_id,
            'metode' => $request->metode,
        ]);
        
        return redirect('matakuliah/'.$request->matakuliah_id.'/learning-outcome')->with('message', 'Assesment berhasil diupdate');
    }
    
    public function destroy(Request $request, $id)
    {
        $assesment = Assesment::findOrFail($id);

        // if ($assesment->nilai()->count())
        // {
        //     return redirect('matakuliah/'.$request->matakuliah_id.'/learning-outcome')->with('error', 'Assesment ini tidak bisa dihapus karena digunakan oleh nilai');
        // }     

        $assesment->delete();

        return redirect('matakuliah/'.$request->matakuliah_id.'/learning-outcome')->with('message', 'Assesment berhasil dihapus');        
    }
    
    public function bobot_bulk_update(Request $request)
    {
        $bobot_total = $this->bobot_total($request->bobot);
             
        if ($bobot_total != 100) {
            return redirect('matakuliah/'.$request->matakuliah_id.'/learning-outcome')->with('error', 'Total Bobot Assesment harus 100');
        }       
        
        foreach ($request->bobot as $id => $value) {
            $this->bobot_update($id, $value);
        }

        return redirect('matakuliah/'.$request->matakuliah_id.'/learning-outcome')->with('message', 'Bobot Assesment berhasil diupdate');
    }

    private function bobot_total($bobot)
    {
        $total = 0;
        foreach ($bobot as $value) {
            $total += $value;
        }

        return $total;
    }    

    private function bobot_update($id, $value) 
    {
        $assesment = Assesment::findOrFail($id);

        $assesment->update([
            'bobot' => $value,
        ]);        
    }
}
