<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\Ruang;
use DataTables;
use Validator;

class RuangController extends Controller
{
    public function index()
    {
        return view('ruang.index');
    }

    public function store(Request $request)
    {        
        $validation = Validator::make($request->all(), [
            'nama' => 'required|unique',           
            'kapasitas' => 'required',
        ]);
          
        if ($validation->passes())
        {
            $ruang = Ruang::create([
                'nama' => $request->nama,           
                'kapasitas' => $request->kapasitas,         
            ]);
    
            return response()->json([
                'data'    => $ruang,
                'message' => 'Data Ruang berhasil ditambah',
            ], 201);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);  
    }  
    
    public function edit($id)
    {
        $ruang=Ruang::find($id);
        return $ruang;
    }

    public function update(Request $request, $id)
    {
        $ruang = Ruang::findOrFail($id);

        // $validation = Validator::make($request->all(), [
        //     'kode' => [
        //         'required',
        //         Rule::unique('fakultas')->ignore($fakultas->id),
        //     ],           
        //     'nama' => 'required',
        // ]);        
        
        // if ($validation->passes())
        // {
            $ruang->update([
                'nama' => $request->nama,           
                'kapasitas' => $request->kapasitas,         
            ]);
    
            return response()->json([
                'data'    => $ruang,
                'message' => 'Data Ruang berhasil diupdate',                
            ]);
        // }   

        // return response()->json([
        //     'errors'  => $validation->errors()->all(),
        // ], 422);         
    }

    public function destroy($id)
    {
        $ruang = Ruang::findOrFail($id);

        $ruang->delete();
        return response()->json([
            'message' => 'Data Ruang berhasil dihapus'
        ]);

    }

    public function data()
    {
        $ruang = Ruang::query();

        return DataTables::of($ruang)
            ->addColumn('action', function($ruang){
                return  '<a id="'.$ruang->id.'" onclick="editForm('.$ruang->id.')" class="btn btn-info waves-effect"><i class="material-icons">mode_edit</i></a> '.
                        '<a id="'.$ruang->id.'" onclick="deleteData('.$ruang->id.')" class="btn btn-danger waves-effect"><i class="material-icons">delete</i></a>';
            })        
            ->make(true);        
    }    
}
