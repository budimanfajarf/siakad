<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\StudyOutcome;
use DataTables;
use Validator;

class StudyOutcomeController extends Controller
{
    public function index()
    {
        return view('study-outcome.index');
    }

    public function store(Request $request)
    {        
        $validation = Validator::make($request->all(), [
            'kode' => 'required|unique:study_outcome',           
            'deskripsi' => 'required',
        ]);
          
        if ($validation->passes())
        {
            $study_outcome = StudyOutcome::create([
                'kode' => $request->kode,           
                'deskripsi' => $request->deskripsi,
                'program_studi_id' => '1',         
            ]);
    
            return response()->json([
                'data'    => $study_outcome,
                'message' => 'Data StudyOutcome berhasil ditambah',
            ], 201);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);  
    }  
    
    public function edit($id)
    {
        $study_outcome = StudyOutcome::find($id);
        return $study_outcome;
    }

    public function update(Request $request, $id)
    {
        $study_outcome = StudyOutcome::findOrFail($id);

        $validation = Validator::make($request->all(), [
            'kode' => [
                'required',
                Rule::unique('study_outcome')->ignore($study_outcome->id),
            ],           
            'deskripsi' => 'required',
        ]);        
        
        if ($validation->passes())
        {
            $study_outcome->update([
                'kode' => $request->kode,           
                'deskripsi' => $request->deskripsi,         
                'program_studi_id' => '1',
            ]);
    
            return response()->json([
                'data'    => $study_outcome,
                'message' => 'Data StudyOutcome berhasil diupdate',                
            ]);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);         
    }

    public function destroy($id)
    {
        $study_outcome = StudyOutcome::findOrFail($id);
        $study_outcome->delete();
        return response()->json([
            'message' => 'Data StudyOutcome berhasil dihapus'
        ]);
    }

    public function data()
    {
        $study_outcome = StudyOutcome::query();

        return DataTables::of($study_outcome)
            ->addColumn('action', function($study_outcome){
                return  '<a id="'.$study_outcome->id.'" onclick="editForm('.$study_outcome->id.')" class="btn btn-info waves-effect"><i class="material-icons">mode_edit</i></a> '.
                        '<a id="'.$study_outcome->id.'" onclick="deleteData('.$study_outcome->id.')" class="btn btn-danger waves-effect"><i class="material-icons">delete</i></a>';
            })        
            ->make(true);        
    }    
}
