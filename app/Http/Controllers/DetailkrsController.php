<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\TahunAkademik;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use App\Models\Jadwal;
use App\Models\Krs;
use App\Models\DetailKrs;
use DataTables;
use Validator;
use PDF;


class DetailkrsController extends Controller
{
    public function index()
    {
        $tahun_akademik=TahunAkademik::all();
        $dosen=Dosen::all();
        $mahasiswa=Mahasiswa::all();
        return view('krs.index', compact('tahun_akademik','dosen','mahasiswa'));
    }

    public function store(Request $request)
    {        
        // $validation = Validator::make($request->all(), [
        //     'kode' => 'required|unique:fakultas',           
        //     'nama' => 'required',
        // ]);
          
        // if ($validation->passes())
        // {
        //     $fakultas = Fakultas::create([
        //         'kode' => $request->kode,           
        //         'nama' => $request->nama,         
        //     ]);
    
        //     return response()->json([
        //         'data'    => $fakultas,
        //         'message' => 'Data Fakultas berhasil ditambah',
        //     ], 201);
        // }   

        // return response()->json([
        //     'errors'  => $validation->errors()->all(),
        // ], 422);
        $data =$request->except('_token');
        $number=count($data['check_matkul']);
        for($i=0; $i<$number; $i++){
            $detail_krs=new DetailKrs;
            // if (isset($data['check_matkul'][$i])!=null) {
                $detail_krs->krs_id=$data['krs_id'];
                $detail_krs->jadwal_id=$data['check_matkul'][$i];
                $detail_krs->save();
                echo "berhasil";
                echo $data['check_matkul'][$i];
            }

            // echo $data['check_matkul'][$i];
            // echo $data['krs_id'];
        // }
        // return $data;

        // return $number;
        
        // return $request->all(); 
    } 

    public function show($id)
    {
        $data=array();
        $detail=DetailKrs::where('krs_id','=',$id)->get();
        foreach ($detail as $details) {
            $data[]=array(
                'id_detail_krs'=>$details->id,
                'krs_id'=>$details->krs_id,
                'thn_akademik'=>$details->krs->tahun_akademik->nama,
                'mahasiswa_nama'=>$details->krs->mahasiswa->nama,
                'mahasiswa_nim'=>$details->krs->mahasiswa->nim,
                'matkul_nama'=>$details->jadwal->matakuliah->nama,
                'matkul_semester'=>$details->jadwal->matakuliah->semester,
                'matkul_sks'=>$details->jadwal->matakuliah->sks,
            );
        
        }

        return $data;
        
    } 
    
    public function edit($id)
    {
        $detail=DetailKrs::find($id);
        return [
            'mahasiswa' => $detail->krs->mahasiswa->id,
            'tahun_akademik'=>$detail->krs->tahun_akademik->id,
            'dosen'=>$detail->krs->dosen->id
        ];
    }

    public function update(Request $request, $id)
    {
        $fakultas = Fakultas::findOrFail($id);

        $validation = Validator::make($request->all(), [
            'kode' => [
                'required',
                Rule::unique('fakultas')->ignore($fakultas->id),
            ],           
            'nama' => 'required',
        ]);        
        
        if ($validation->passes())
        {
            $fakultas->update([
                'kode' => $request->kode,           
                'nama' => $request->nama,         
            ]);
    
            return response()->json([
                'data'    => $fakultas,
                'message' => 'Data Fakultas berhasil diupdate',                
            ]);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);         
    }

    public function destroy(Request $request)
    {
        // $fakultas = Fakultas::findOrFail($id);

        // if ($fakultas->program_studi_()->count())
        // {
        //     $error = 'Data Fakultas ini tidak bisa dihapus karena digunakan oleh program studi: ';
        //     foreach ($fakultas->program_studi_ as $program_studi) 
        //     {
        //         $error .= $program_studi->nama . ', ';
        //     }
        //     $errors[] = $error;
        //     return response()->json([
        //         'errors' => $errors
        //     ], 422);
        // } else {
        //     $fakultas->delete();
        //     return response()->json([
        //         'message' => 'Data Fakultas berhasil dihapus'
        //     ]);
        // };
        $detail_krs=DetailKrs::whereIn('id',$request->id);
        if ($detail_krs->delete()) {
            return [
                'message'=>'Data Berhasil di Update'
            ];
        }
        else{
            return 'gagal';
        }
    }

    public function data()
    {
        $detail = DetailKrs::query();
        return DataTables::of($detail)
            ->addColumn('Nama',function ($detail)
            {
                 return $detail->krs->mahasiswa->nama;
            })
            ->addColumn('tahun_akademik',function ($detail)
            {
                 return $detail->krs->tahun_akademik->nama;
            })
            ->addColumn('action', function($detail){
                return  '<a id="'.$detail->id.'" onclick="editForm('.$detail->id.')" class="btn btn-info waves-effect"><i class="material-icons">mode_edit</i></a> '.
                        '<a id="'.$detail->id.'" onclick="viewData('.$detail->id.')" class="btn btn-info waves-effect"><i class="material-icons">visibility</i></a> '.
                        '<a id="'.$detail->id.'" onclick="deleteData('.$detail->id.')" class="btn btn-danger waves-effect"><i class="material-icons">delete</i></a>';
            })        
            ->make(true);        
    }

    public function jadwal()
    {
        $jadwal=Jadwal::all();
        $data=array();
        foreach ($jadwal as $jadwals ) {
            $data[]=array(
                'id_jadwal'=>$jadwals->id,
                'matkul_id'=>$jadwals->matakuliah->id,
                'matkul_nama'=>$jadwals->matakuliah->nama,
                'matkul_sks'=>$jadwals->matakuliah->sks,
                'matkul_semester'=>$jadwals->matakuliah->semester,
                'dosen_nama'=>$jadwals->dosen->nama,
                'dosen_id'=>$jadwals->dosen_id,
                'ruang_nama'=>$jadwals->ruang->nama,
                'ruang_id'=>$jadwals->ruang_id,
                'thnakademik'=>$jadwals->tahun_akademik->nama,
                'semester'=>$jadwals->semester,
                'hari'=>$jadwals->hari,
                'jam_mulai'=>$jadwals->jam_mulai,
                'jam_selesai'=>$jadwals->jam_selesai,
                'kelas'=>$jadwals->kelas,
            );
         }
         return json_encode($data);
        return 'jadwal krs';
    }

    public function pdf($id)
    {
        $data=array();
        $detail=DetailKrs::where('krs_id','=',$id)->get();
        foreach ($detail as $details) {
            $data[]=array(
                'id_detail_krs'=>$details->id,
                'krs_id'=>$details->krs_id,
                'dosen'=>$details->krs->dosen->nama,
                'dosen_nidn'=>$details->krs->dosen->nidn,
                'mahasiswa'=>$details->krs->mahasiswa->nama,
                'mahasiswa_nim'=>$details->krs->mahasiswa->nim,
                'thn_akademik'=>$details->krs->tahun_akademik->nama,
                'matkul_kode'=>$details->jadwal->matakuliah->kode,
                'matkul_nama'=>$details->jadwal->matakuliah->nama,
                'matkul_semester'=>$details->jadwal->matakuliah->semester,
                'matkul_sks'=>$details->jadwal->matakuliah->sks,
                'matkul_dosen'=>$details->jadwal->dosen->nama,
            );
        
        }
        // $view=View::make('krs.print', array('data'=>$data))->render();
        // dd($data);

        // $pdf=\App::make('dompdf.wrapper');
        // // $pdf=PDF::loadHTML('<h1>Test</h1>');
        // $pdf->loadView('krs.print', compact('data'));
        // ini_set('max_execution_time', 300); //300 seconds = 5 minutes 
        // return $pdf->download();
        return view('krs.print',compact('data'));
    }    
}
