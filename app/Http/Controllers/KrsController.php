<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\TahunAkademik;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use App\Models\Krs;
use DataTables;
use Validator;

class KrsController extends Controller
{
    public function index()
    {
        $tahun_akademik=TahunAkademik::all();
        $dosen=Dosen::all();
        $mahasiswa=Mahasiswa::all();
        return view('krs.index', compact('tahun_akademik','dosen','mahasiswa'));
    }

    public function store(Request $request)
    {        
        // $validation = Validator::make($request->all(), [
        //     'kode' => 'required|unique:fakultas',           
        //     'nama' => 'required',
        // ]);
          
        // if ($validation->passes())
        // {
        //     $fakultas = Fakultas::create([
        //         'kode' => $request->kode,           
        //         'nama' => $request->nama,         
        //     ]);
    
        //     return response()->json([
        //         'data'    => $fakultas,
        //         'message' => 'Data Fakultas berhasil ditambah',
        //     ], 201);
        // }   

        // return response()->json([
        //     'errors'  => $validation->errors()->all(),
        // ], 422);

        $krs=Krs::create([
            'tahun_akademik_id'=>$request->tahun_akademik,
            'dosen_id'=>$request->dosen,
            'mahasiswa_id'=>$request->mahasiswa
        ]);

        // $krs=$request->all();

        return response()->json([
                'data'=>$krs,
                'message' => 'Data Mahasiswa Untuk Pengambilan Krs berhasil ditambah',
            ], 201); 

        // return $request->all(); 
    }  
    
    public function edit($id)
    {
        $fakultas=Fakultas::find($id);
        return $fakultas;
    }

    // public function update(Request $request, $id)
    // {
    //     $fakultas = Fakultas::findOrFail($id);

    //     $validation = Validator::make($request->all(), [
    //         'kode' => [
    //             'required',
    //             Rule::unique('fakultas')->ignore($fakultas->id),
    //         ],           
    //         'nama' => 'required',
    //     ]);        
        
    //     if ($validation->passes())
    //     {
    //         $fakultas->update([
    //             'kode' => $request->kode,           
    //             'nama' => $request->nama,         
    //         ]);
    
    //         return response()->json([
    //             'data'    => $fakultas,
    //             'message' => 'Data Fakultas berhasil diupdate',                
    //         ]);
    //     }   

    //     return response()->json([
    //         'errors'  => $validation->errors()->all(),
    //     ], 422);         
    // }

    public function destroy($id)
    {
        $krs = Krs::findOrFail($id);

        if ($krs->delete()) {
            return response()->json([
                'message' => 'Data KRS berhasil dihapus'
            ]);
        }
        return $id;
  
            

    }

    public function data()
    {
        $krs = Krs::query();

        return DataTables::of($krs)
            ->addColumn('Nama',function ($krs)
            {
                 return $krs->mahasiswa->nama;
            })
            ->addColumn('tahun_akademik',function ($krs)
            {
                 return $krs->tahun_akademik->nama;
            })
            ->addColumn('action', function($krs){
                return '<a id="'.$krs->id.'" onclick="viewData('.$krs->id.')" class="btn btn-info waves-effect"><i class="material-icons">visibility</i></a> '.
                        '<a id="'.$krs->id.'" onclick="editData('.$krs->id.')" class="btn btn-info waves-effect"><i class="material-icons">edit</i></a> '.
                        '<a id="'.$krs->id.'" onclick="deleteData('.$krs->id.')" class="btn btn-danger waves-effect"><i class="material-icons">delete</i></a>';
            })        
            ->make(true);     
    }    
}
