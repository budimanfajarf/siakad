<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use App\Models\Matakuliah;
use App\Models\StudyOutcome;
use App\Models\NilaiAssesment;
use DataTables;
use Validator;
use PDF;

class MahasiswaController extends Controller
{
    public function index()
    {
        return view('mahasiswa.index');
    }

    public function store(Request $request)
    {        
        $validation = Validator::make($request->all(), [
            'nim' => 'required|unique:mahasiswa',           
            'nama' => 'required',
            'alamat' => 'required',
        ]);
          
        if ($validation->passes())
        {
            $mahasiswa = Mahasiswa::create([
                'nim' => $request->nim,           
                'nama' => $request->nama,
                'alamat' => $request->alamat,                
                'program_studi_id' => 1,         
            ]);
    
            return response()->json([
                'data'    => $mahasiswa,
                'message' => 'Data Mahasiswa berhasil ditambah',
            ], 201);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);  
    }  
    
    public function edit($id)
    {
        $mahasiswa=Mahasiswa::find($id);
        return $mahasiswa;
    }

    public function update(Request $request, $id)
    {
        $mahasiswa = Mahasiswa::findOrFail($id);

        $validation = Validator::make($request->all(), [
            'nim' => [
                'required',
                Rule::unique('mahasiswa')->ignore($mahasiswa->id),
            ],           
            'nama' => 'required',
            'alamat' => 'required',
        ]);        
        
        if ($validation->passes())
        {
            $mahasiswa->update([
                'nim' => $request->nim,           
                'nama' => $request->nama,
                'alamat' => $request->alamat,                 
                'program_studi_id' => 1,         
            ]);
    
            return response()->json([
                'data'    => $mahasiswa,
                'message' => 'Data Mahasiswa berhasil diupdate',                
            ]);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);         
    }

    public function destroy($id)
    {
        $mahasiswa = Mahasiswa::findOrFail($id);
        $errors = [];

        // if ($mahasiswa->mahasiswa_()->count())
        // {
        //     $error = 'Data Mahasiswa ini tidak bisa dihapus karena digunakan oleh data mahasiswa';
        //     $errors[] = $error;
        // }

        // if ($errors) 
        // {
        //     return response()->json([
        //         'errors' => $errors
        //     ], 422);
        // } 
        // else {
            $mahasiswa->delete();
            return response()->json([
                'message' => 'Data Mahasiswa berhasil dihapus'
            ]);
        // }
    }

    public function data()
    {
        $mahasiswa = Mahasiswa::query();

        return DataTables::of($mahasiswa)       
            ->addColumn('action', function($mahasiswa){
                return  '<a id="'.$mahasiswa->id.'" onclick="editForm('.$mahasiswa->id.')" class="btn btn-info waves-effect"><i class="material-icons">mode_edit</i></a> '.
                        '<a id="'.$mahasiswa->id.'" onclick="deleteData('.$mahasiswa->id.')" class="btn btn-danger waves-effect"><i class="material-icons">delete</i></a> '.
                        '<a id="'.$mahasiswa->id.'" href="/mahasiswa/'.$mahasiswa->id.'/study-outcome" class="btn bg-teal waves-effect"><i class="material-icons">school</i></a>';
            })        
            ->make(true);        
    }    

    public function study_outcome($id)
    {
        /** ambil data eloquent @var Mahasiswa berdasarkan @var id mahasiswa */
        $mahasiswa = Mahasiswa::with([
            'detail_krs_.nilai_assesment_.assesment.learning_outcome.study_outcome',
            'detail_krs_.jadwal.matakuliah',            
        ])
        ->findOrFail($id);

        /** ambil semua data eloquent @var StudyOutcome */        
        $study_outcome_ = StudyOutcome::All();

    /**
     * Coding create collection baru
     * Penampungan data @var mahasiswa_n
     * mahasiswa->study_outcome_->matakuliah_->nilai_assesment_ dengan format loop
     * loop @var mahasiswa_n['study_outcome_'] as @var study_outcome
     * loop @var study_outcome['matakuliah_'] as @var matakuliah
     * loop @var matakuliah['nilai_assesment_'] as @var nilai_assesment
     * dari pengolahan data @var mahasiswa dan @var study_outcome_
     */
    
    /** collection @var mahasiswa_n */
        $mahasiswa_n = collect();
        $mahasiswa_n->put('id', $mahasiswa->id);
        $mahasiswa_n->put('nim', $mahasiswa->nim);
        $mahasiswa_n->put('nama', $mahasiswa->nama);
        $mahasiswa_n->put('program_studi', $mahasiswa->program_studi->nama);

        /** prepare collection @var mahasiswa['study_outcome_'] */
        $mahasiswa_n->put('study_outcome_', collect());

        foreach($study_outcome_ as $study_outcome) { 
         /** collection @var mahasiswa[study_outcome_] */
            $study_outcome_n = Array();
            $study_outcome_n['id'] = $study_outcome->id;        
            $study_outcome_n['kode'] = $study_outcome->kode;        
            $study_outcome_n['deskripsi'] = $study_outcome->deskripsi;

            /** prepare collection @var study_outcome['matakuliah_'] */            
            $study_outcome_n['matakuliah_'] = collect();        

            foreach($mahasiswa->detail_krs_ as $detail_krs) {
             /** collection @var study_outcome['matakuliah_'] */
                $matakuliah_n = Array();
                $matakuliah_n['id'] = $detail_krs->jadwal->matakuliah->id;
                $matakuliah_n['kode'] = $detail_krs->jadwal->matakuliah->kode;
                $matakuliah_n['nama'] = $detail_krs->jadwal->matakuliah->nama;
                $matakuliah_n['sks'] = $detail_krs->jadwal->matakuliah->sks;
                $matakuliah_n['semester'] = $detail_krs->jadwal->matakuliah->semester;

                /** 
                 * sum semua bobot yg ada pada semua assesment yg ada pada matakuliah ini 
                 * walaupun study outcome berbeda
                 * @var matakuliah['sum_bobot_all']
                 */
                $matakuliah_n['sum_bobot_all'] = $detail_krs->nilai_assesment_->sum('bobot');

                /** prepare collection @var matakuliah['nilai_assesment_'] */
                $matakuliah_n['nilai_assesment_'] = collect();

                foreach($detail_krs->nilai_assesment_ as $nilai_assesment) {
                /** 
                 * Cek apakah nilai assesment matakuliah ini berkontribusi untuk study outcome yg sedang di loop 
                 * jika @var true, ambil data dan push/tampung data ke @var matakuliah['nilai_assesment_] 
                 */
                    if ($study_outcome->id == $nilai_assesment->assesment->learning_outcome->study_outcome->id) {
                    /** collection @var matakuliah['nilai_assesment_'] */
                        $nilai_assesment_n = Array();
                        $nilai_assesment_n['id'] = $nilai_assesment->id;
                        $nilai_assesment_n['nilai'] = $nilai_assesment->nilai;
                        $nilai_assesment_n['bobot'] = $nilai_assesment->bobot;
                        $nilai_assesment_n['nilai_bobot'] = $nilai_assesment->nilai_bobot;
                
                        $matakuliah_n['nilai_assesment_']->push($nilai_assesment_n);    
                    }
                }

                /** Push / tampung data jika @var matakuliah['sum_bobot_all'] dan @var matakuliah['nilai_assesment_'] tidak kosong / null */                
                if ($matakuliah_n['sum_bobot_all'] && $matakuliah_n['nilai_assesment_']->isNotEmpty()) {
                    $matakuliah_n['nilai'] = $this->nilai_mk_4_so($matakuliah_n);
                    $study_outcome_n['matakuliah_']->push($matakuliah_n);
                }

            }

            /** Push / tampung data jika @var study_outcome['matakuliah_'] tidak kosong / null */                            
            if($study_outcome_n['matakuliah_']->isNotEmpty()) {
                $study_outcome_n['nilai'] = $this->nilai_so($study_outcome_n);                
                $mahasiswa_n['study_outcome_']->push($study_outcome_n);
            } else {
                $mahasiswa_n['study_outcome_']->push($study_outcome_n);                
            }
        }
    /**
     * data tertampung di @var mahasiswa_n 
     */

    /**
     * Merge matakuliah
     */
        $matakuliah_merge_ = collect();
    
        foreach($mahasiswa_n['study_outcome_'] as $study_outcome) {
            foreach($study_outcome['matakuliah_'] as $matakuliah) {
                // $collection->push($matakuliah);
                $matakuliah_n = Array();
                $matakuliah_n['id'] = $matakuliah['id'];
                $matakuliah_n['kode'] = $matakuliah['kode'];
                $matakuliah_n['nama'] = $matakuliah['nama'];
                $matakuliah_n['sks'] = $matakuliah['sks'];
                $matakuliah_n['semester'] = $matakuliah['semester'];
                $matakuliah_merge_->push($matakuliah_n);
            }
        }

        $matakuliah_merge_ = $matakuliah_merge_->unique('id');

        /** put ke @var mahasiswa_n */
        $mahasiswa_n->put('matakuliah_merge_', $matakuliah_merge_);        
     /** 
     * End Merge matakuliah 
     */
        
        return $mahasiswa_n;

    /**
     * Example Coding dalam menampilkan data dan kalkulasi @var mahasiswa_n
        foreach($mahasiswa_n['study_outcome_']->sortByDesc('nilai') as $study_outcome) {
            echo '<br> Kode SO: '.$study_outcome['id'];
            echo '<br> Deskripsi SO: '.$study_outcome['deskripsi'];
            echo '<br> Nilai SO: '.$study_outcome['nilai'];
            
            foreach($study_outcome['matakuliah_'] as $matakuliah) {
                echo '<br> Kode MK: '.$matakuliah['kode'];
                echo '<br> Nama MK: '.$matakuliah['nama'];
                echo '<br> SKS: '.$matakuliah['sks'];
                echo '<br> Nilai MK 4 SO: '.$matakuliah['nilai'];
            }
            echo '<br>';
        }
     * End Example Coding dalam menampilkan data dan kalkulasi @var mahasiswa_n
     */        
    }

    /**
     * Fungsi untuk menghitung Nilai Matakuliah untuk suatu Study Outcome 
     * @param array
     * @return float
     * @return sum_nilai_bobot*sum_bobot_all/sum_bobot;
     * @var matakuliah['nilai']=nilai_mk_4_so()
     */     
    private function nilai_mk_4_so($matakuliah_n) {
        $sum_nilai_bobot = $matakuliah_n['nilai_assesment_']->sum('nilai_bobot');
        $sum_bobot = $matakuliah_n['nilai_assesment_']->sum('bobot');
        $sum_bobot_all = $matakuliah_n['sum_bobot_all'];        
        $nilai = $sum_nilai_bobot * $sum_bobot_all / $sum_bobot;
        return $nilai;
    }    

    /**
     * Fungsi untuk menghitung Nilai Study Outcome dari semua matakuliah
     * @param array
     * @return float
     * @return sum_nilai_mk_x_sks_mk/sum_sks_mk
     * @var study_outcome[nilai]=nilai_so()
     */     
    private function nilai_so($study_outcome_n) {
        $sum_nilai_mk_x_sks_mk = $study_outcome_n['matakuliah_']
                            ->sum(function ($matakuliah){                    
                                return $matakuliah['nilai'] * $matakuliah['sks'];
                            });
        $sum_sks_mk = $study_outcome_n['matakuliah_']->sum('sks');
        $nilai = $sum_nilai_mk_x_sks_mk / $sum_sks_mk;        
        return $nilai;
    }

    public function study_outcome_index($id) {
        $mahasiswa_n = $this->study_outcome($id);        
        return view('mahasiswa.study-outcome', compact('mahasiswa_n'));        
    }    

    public function study_outcome_pdf($id) {
        $mahasiswa_n = $this->study_outcome($id);

        return PDF::loadView('mahasiswa.study-outcome-pdf', compact('mahasiswa_n'))
                ->setPaper('a4', 'landscape')
                ->setOptions(['defaultFont' => 'sans-serif'])
                ->stream('Study Outcome - '.$mahasiswa_n['nim'].'_'.$mahasiswa_n['nama'].'.pdf');                    
    }
       
}
