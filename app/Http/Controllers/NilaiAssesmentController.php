<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\NilaiAssesment;
use App\Models\Jadwal;
use App\Models\Matakuliah;
use Validator;
use PDF;

class NilaiAssesmentController extends Controller
{
    public function index($jadwal_id)
    {        
        $jadwal = Jadwal::with([
            'detail_krs_.krs.mahasiswa',            
            'detail_krs_.nilai_assesment_.assesment',                                        
            'matakuliah.learning_outcome_.assesment_',
            'matakuliah.assesment_',
            'tahun_akademik',     
        ])->findOrFail($jadwal_id);

        foreach ($jadwal->detail_krs_ as $detail_krs) {

            foreach($jadwal->matakuliah->learning_outcome_ as $learning_outcome) {
                foreach($learning_outcome->assesment_ as $assesment) {

                    $detail_krs->nilai_assesment = NilaiAssesment::firstOrCreate([
                        'detail_krs_id' => $detail_krs->id,
                        'assesment_id' => $assesment->id,
                    ]);

                }
            }

        }       


        return view('nilai-assesment.index', compact('jadwal'));
    }

    public function nilai_bulk_update(Request $request, $jadwal_id)
    {                
        $this->validate($request, [
            'nilai.*' => 'nullable|numeric|min:0|max:100',
        ]);     

        foreach ($request->nilai as $id => $value) {
            $nilai_update = $this->nilai_update($id, $value);

            /* note: Jika auth sudah berjalan, refactor
                if (!$nilai_update)
                    return 'ada patch nilai yg gagal di update';
            */
        }

        // return 'berhasil';
        return redirect('jadwal/'.$request->jadwal_id.'/nilai-assesment')->with('message', 'Nilai Assesment berhasil diupdate');        
    }    

    private function nilai_update($id, $value)
    {
        $nilai_assesment = NilaiAssesment::findOrFail($id);

        $nilai_assesment->update([
            'nilai' => $value,
        ]);

        /* note: buat authentikasi dosen nanti
            $tahun_akademik_id  = $nilai_assesment->tahun_akademik_id;
            $matakuliah_id  = $nilai_assesment->asesment->learning_outcome->matakuliah_id;        
            
            $jadwal = Jadwal::where('matakuliah_id', $matakuliah_id)
                                            ->where('tahun_akademik_id', $tahun_akademik_id)
                                            ->get();
            if (authentikasi_dosen_id == $jadwal->dosen_id ) {
                // boleh update
                // jika berhasil return true
            } else {
                // tidak boleh update
                // return false
            }
        */
    }

    public function matakuliah($matakuliah_id)
    {        
        // dd($matakuliah_id);
        $matakuliah = Matakuliah::with([
            'jadwal_.tahun_akademik',
            'jadwal_.dosen',
            'detail_krs_.krs.mahasiswa',            
            'detail_krs_.nilai_assesment_.assesment',                                        
            'learning_outcome_.assesment_',
            'assesment_',
            // 'tahun_akademik',     
        ])->findOrFail($matakuliah_id);

        foreach ($matakuliah->detail_krs_ as $detail_krs) {

            foreach($matakuliah->learning_outcome_ as $learning_outcome) {
                foreach($learning_outcome->assesment_ as $assesment) {

                    $detail_krs->nilai_assesment = NilaiAssesment::firstOrCreate([
                        'detail_krs_id' => $detail_krs->id,
                        'assesment_id' => $assesment->id,
                    ]);

                }
            }

        }       

        // dd($matakuliah);
        return view('matakuliah.nilai-assesment', compact('matakuliah'));
    }
    
    public function matakuliah_nilai_bulk_update(Request $request, $matakuliah_id)
    {        
        // dd($matakuliah_id);        
        $this->validate($request, [
            'nilai.*' => 'nullable|numeric|min:0|max:100',
        ]);     

        foreach ($request->nilai as $id => $value) {
            $nilai_update = $this->nilai_update($id, $value);

            /* note: Jika auth sudah berjalan, refactor
                if (!$nilai_update)
                    return 'ada patch nilai yg gagal di update';
            */
        }

        // return 'berhasil';
        return redirect('matakuliah/'.$request->matakuliah_id.'/nilai-assesment')->with('message', 'Nilai Assesment berhasil diupdate');        
    }
    
    public function pdf($jadwal_id) 
    {
        $jadwal = Jadwal::with([
            'detail_krs_.krs.mahasiswa',            
            'detail_krs_.nilai_assesment_.assesment',                                        
            'matakuliah.learning_outcome_.assesment_',
            'matakuliah.assesment_',
            'tahun_akademik',     
        ])->findOrFail($jadwal_id);

        // dd($jadwal);
        // return view('nilai-assesment.pdf', compact('jadwal'));
        return PDF::loadView('nilai-assesment.pdf', compact('jadwal'))
                ->setPaper('a4', 'landscape')
                ->setOptions(['defaultFont' => 'sans-serif'])
                ->stream('Nilai Assesment - '.$jadwal->matakuliah->kode.'_'.
                                              $jadwal->matakuliah->nama.' - '.
                                              $jadwal->tahun_akademik->nama.'.pdf');
    }

    public function matakuliah_pdf($matakuliah_id) 
    {
        $matakuliah = Matakuliah::with([
            // 'jadwal_.tahun_akademik',
            // 'jadwal_.dosen',
            'detail_krs_.krs.mahasiswa',            
            'detail_krs_.nilai_assesment_.assesment',                                        
            'learning_outcome_.assesment_',
            'assesment_',
        ])->findOrFail($matakuliah_id);

        // dd($matakuliah);
        // return view('matakuliah.nilai-assesment-pdf', compact('matakuliah'));
        return PDF::loadView('matakuliah.nilai-assesment-pdf', compact('matakuliah'))
                ->setPaper('a4', 'landscape')
                ->setOptions(['defaultFont' => 'sans-serif'])
                ->stream('Nilai Assesment - '.$matakuliah->kode.'_'.
                                              $matakuliah->nama.'.pdf');
    }    
}
