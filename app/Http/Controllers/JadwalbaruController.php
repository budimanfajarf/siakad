<?php 

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Database;
use App\Models\Jadwal;
use App\Models\Matakuliah;
use App\Models\TahunAkademik;
use App\Models\Dosen;
use App\Models\Ruang;
use DataTables;
use Validator;

class JadwalbaruController extends Controller
{
    public function index()
    {
        $thnakademik = TahunAkademik::All();
        return view('jadwalbaru.index', compact('thnakademik'));

    }

    public function create()
    {
        return view('jadwal.create');
    }

    public function select($id)
    {
        $matkul=Matakuliah::where('semester','=',$id)->get();
        $dosen=Dosen::all();
        $ruang=Ruang::all();
        return json_encode([
                        'matkul' =>$matkul,
                        'dosen'  =>$dosen,
                        'ruang'  =>$ruang
        ]);
    }

    // public function selectdosen()
    // {
    //     $dosen=Dosen::all();
    //     return [
    //         'dosen'=>$dosen
    //     ];        
    // }

    public function store(Request $request)
    {        
        $data = $request->except('_token');
        $number=count($data['matkul_id']);
        $jadwal=new Jadwal;
        // $jadwal->all();
        // foreach ($jadwal as $key) {
            // echo js$jadwal->jam_selesai;
        // }

        for ($i=0; $i <$number ; $i++) {
            
            if ($jadwal->where('jam_selesai','<',$data['jam_mulai'][$i])->get()) {
                return 'ruangan sudah digunakan';
            }
            return 'bisa';
            // $jadwal->kelas=$data['kelas'];
            // $jadwal->tahun_akademik_id=$data['tahun_akademik_id'];
            // $jadwal->semester=$data['semester'];
            // $jadwal->dosen_id=$data['dosen'][$i];
            // $jadwal->matakuliah_id=$data['matkul_id'][$i];
            // $jadwal->ruang_id=$data['ruang'][$i];
            // $jadwal->hari=$data['hari'][$i];
            // $jadwal->jam_mulai=$data['jam_mulai'][$i];
            // $jadwal->jam_selesai=$data['jam_selesai'][$i];

            // $jadwal->save();
        }

        // return 'berhasil';
        // return $request->matkul_id[0];
        // return $data['matkul_id'][0];
          
    }  
    
    public function edit($id)
    {
        $fakultas=Fakultas::find($id);
        return $fakultas;
    }

    public function update(Request $request, $idt,$idk,$ids)
    {
        $data = $request->except('_token');
        $number=count($data['matkul_id']);
        $jadwal=Jadwal::where('tahun_akademik_id',$idt)->where('kelas',$idk)->where('semester',$ids);
        for ($i=0; $i <$number ; $i++) { 
            $jadwal->update([
                'dosen_id'=>$data['dosen'][$i],
                'ruang_id'=>$data['ruang'][$i],
                'hari'=>$data['hari'][$i],
                'jam_mulai'=>$data['jam_mulai'][$i],
                'jam_selesai'=>$data['jam_selesai'][$i],
            ]);
        }
        return '$jadwal';
        // $number=count($data['matkul_id']);
        // $jadwal->update([
        //     'dosen_id'=>$data['dosen'][0][3],
        // ]);
        // return $request->dosen[0];
        // for ($i=0; $i <$number ; $i++) { 
            // $jadwal->update([
                // 'dosen_id'=>$data['dosen'][$i]
            // ]);
        // }
        // return 'gooooood';
        // $idt=1;
        // $idk=a;
        // $ids=1;
        // $jadwal=Jadwal::where('tahun_akademik_id','=',$idt)->where('kelas','=',$idk)->where('semester','=',$ids)->get();
        // $data = $request->except('_token');
        // $data['dosen']=6;
        // $data['ruang']=6
        // $dosen= $data['dosen'];
        // $ruang=$data['ruang'];
        // $number=count($data['matkul_id']);
        // for ($i=0; $i <$number ; $i++) { 
            // $jadwal->update([
            //     'dosen_id'=>$dosen,
            //     'ruang_id'=>$ruang,
                // 'hari'=>$data['hari'][$i],
                // 'jam_mulai'=>$data['jam_mulai'][$i],
                // 'jam_selesai'=>$data['jam_selesai'][$i],
            // ]);
        // }

        // $fakultas = Fakultas::findOrFail($id);

        // $validation = Validator::make($request->all(), [
        //     'kode' => [
        //         'required',
        //         Rule::unique('fakultas')->ignore($fakultas->id),
        //     ],           
        //     'nama' => 'required',
        // ]);        
        
        // if ($validation->passes())
        // {
        //     $fakultas->update([
        //         'kode' => $request->kode,           
        //         'nama' => $request->nama,         
        //     ]);
    
        //     return response()->json([
        //         'data'    => $fakultas,
        //         'message' => 'Data Fakultas berhasil diupdate',                
        //     ]);
        // }   

        // return response()->json([
        //     'errors'  => $validation->errors()->all(),
        // ], 422);         
    }

    public function destroy($id)
    {
        $fakultas = Fakultas::findOrFail($id);

        if ($fakultas->program_studi_()->count())
        {
            $error = 'Data Fakultas ini tidak bisa dihapus karena digunakan oleh program studi: ';
            foreach ($fakultas->program_studi_ as $program_studi) 
            {
                $error .= $program_studi->nama . ', ';
            }
            $errors[] = $error;
            return response()->json([
                'errors' => $errors
            ], 422);
        } else {
            $fakultas->delete();
            return response()->json([
                'message' => 'Data Fakultas berhasil dihapus'
            ]);
        };
    }

    public function data()
    {
        $jadwal = Jadwal::query();

        // return DataTables::of($jadwal)
            // ->addColumn('tahun_akademik', function($jadwal){
            //     return  $jadwal->tahun_akademik->nama;
            // }) 
            // ->addColumn('matakuliah', function($jadwal){
            //     return  $jadwal->matakuliah->nama;
            // }) 
            // ->addColumn('dosen', function($jadwal){
            //     return  $jadwal->dosen->nama;
            // })
            // ->addColumn('semester', function($jadwal){
            //     return  $jadwal->matakuliah->semester;
            // }) 
            // ->addColumn('ruang', function($jadwal){
            //     return  $jadwal->ruang->nama;
            // })      
            // ->addColumn('action', function($jadwal){
            //     return  '<a id="'.$jadwal->id.'" onclick="editForm('.$jadwal->id.')" class="btn btn-info waves-effect"><i class="material-icons">mode_edit</i></a> '.
            //             '<a id="'.$jadwal->id.'" onclick="deleteData('.$jadwal->id.')" class="btn btn-danger waves-effect"><i class="material-icons">delete</i></a>';
            // })        
            // ->make(true);    
            // $jadwal = Jadwal::query();

            return DataTables::of($jadwal)
            ->addColumn('tahun_akademik', function($jadwal){
                return  $jadwal->tahun_akademik->nama;
            }) 
            ->addColumn('matakuliah', function($jadwal){
                return  $jadwal->matakuliah->nama;
            }) 
            ->addColumn('dosen', function($jadwal){
                return  $jadwal->dosen->nama;
            })
            ->make(true);  
    } 

    public function getview($idt,$idk,$ids)
      {
        // $id=1;
          $jadwal = Jadwal::where('tahun_akademik_id','=',$idt)
                            ->where('kelas','=',$idk)
                            ->where('semester','=',$ids)
                            ->get();
          $dosen=Dosen::all();
          $ruang=Ruang::all();
          $data[]=array(
            'dosen_table' => $dosen,
            'ruang_table' => $ruang
          );
          foreach ($jadwal as $jadwals ) {
              $data[]=array(
                'id'=>$jadwals->id,
                'matkul_id'=>$jadwals->matakuliah->id,
                'matkul'=>$jadwals->matakuliah->nama,
                'dosen_nama'=>$jadwals->dosen->nama,
                'dosen_id'=>$jadwals->dosen_id,
                'ruang_nama'=>$jadwals->ruang->nama,
                'ruang_id'=>$jadwals->ruang_id,
                'thnakademik'=>$jadwals->tahun_akademik->nama,
                'semester'=>$jadwals->semester,
                'hari'=>$jadwals->hari,
                'jam_mulai'=>$jadwals->jam_mulai,
                'jam_selesai'=>$jadwals->jam_selesai,
                'kelas'=>$jadwals->kelas,
            );
          }
          return ($data);
        // return 'id ke-'.$id.' : '.'id ke-'.$id1.' : '.'id ke-'.$id2;
        // dd($jadwal);
        // $jadwal=Jadwal::where('tahun_akademik_id',$idt)->where('kelas',$idk)->where('semester',$ids)->get();
        // return $jadwal;
      }

    public function bulkdelete(Request $request)
    {
        $id_jadwal=$request->id;
        $id_jadwal=Jadwal::whereIn('id', $id_jadwal);
        if ($id_jadwal->delete()) {
            echo "delete berhasil";
        }else{
            echo "gagal";
        }
    }  
}
