<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\Matakuliah;
use App\Models\StudyOutcome;
use App\Models\LearningOutcome;
use DataTables;
use Validator;
use PDF;

class LearningOutcomeController extends Controller
{
    public function index($matakuliah_id)
    {
        $matakuliah = Matakuliah::with([
                                        'learning_outcome_.study_outcome', 
                                        'learning_outcome_.assesment_', 
                                        'program_studi', 
                                        'assesment_'
                                    ])
                                    ->findOrFail($matakuliah_id);

        $study_outcome_all_ = StudyOutcome::All();
        
        $study_outcome_ = StudyOutcome::whereHas('learning_outcome_', function($query) use($matakuliah_id){
            $query->where('matakuliah_id', $matakuliah_id);
        });  

        $study_outcome_ = $study_outcome_->with('learning_outcome_.assesment_')->get();

        return view('matakuliah.learning-outcome', compact('matakuliah', 'study_outcome_', 'study_outcome_all_'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'matakuliah_id' => 'required',
            'study_outcome_id' => 'required',
            'kode' => 'required',           
            'deskripsi' => 'required',
        ]);

        $learning_outcome = LearningOutcome::create([
            'matakuliah_id' => $request->matakuliah_id,
            'study_outcome_id' => $request->study_outcome_id,
            'kode' => $request->kode,           
            'deskripsi' => $request->deskripsi,        
        ]);        

        return redirect('matakuliah/'.$request->matakuliah_id.'/learning-outcome')->with('message', 'Learning Outcome berhasil ditambahkan');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'matakuliah_id' => 'required',
            'study_outcome_id' => 'required',
            'kode' => 'required',           
            'deskripsi' => 'required',
        ]);

        $learning_outcome = LearningOutcome::findOrFail($id);

        $learning_outcome->update([
            'matakuliah_id' => $request->matakuliah_id,
            'study_outcome_id' => $request->study_outcome_id,
            'kode' => $request->kode,           
            'deskripsi' => $request->deskripsi,
        ]);
        
        return redirect('matakuliah/'.$request->matakuliah_id.'/learning-outcome')->with('message', 'Learning Outcome berhasil diupdate');
    }
    
    public function destroy(Request $request, $id)
    {
        $learning_outcome = LearningOutcome::findOrFail($id);

        if ($learning_outcome->assesment_()->count())
        {
            return redirect('matakuliah/'.$request->matakuliah_id.'/learning-outcome')->with('error', 'Learning Outcome ini tidak bisa dihapus karena digunakan oleh assesment');
        }     

        $learning_outcome->delete();

        return redirect('matakuliah/'.$request->matakuliah_id.'/learning-outcome')->with('message', 'Learning Outcome berhasil dihapus');        
    }

    public function pdf($matakuliah_id)
    {
        // dd($matakuliah_id);
        $matakuliah = Matakuliah::with([
            'learning_outcome_.study_outcome', 
            'learning_outcome_.assesment_', 
            'program_studi', 
            'assesment_'
        ])
        ->findOrFail($matakuliah_id);
        
        $study_outcome_ = StudyOutcome::whereHas('learning_outcome_', function($query) use($matakuliah_id){
            $query->where('matakuliah_id', $matakuliah_id);
        });  

        $study_outcome_ = $study_outcome_->with('learning_outcome_.assesment_')->get();

        return PDF::loadView('matakuliah.learning-outcome-pdf', compact('matakuliah', 'study_outcome_'))
                ->setPaper('a4', 'landscape')
                ->setOptions(['defaultFont' => 'sans-serif'])
                ->stream($matakuliah->kode.' - '.$matakuliah->nama.'.pdf');             

    }
}
