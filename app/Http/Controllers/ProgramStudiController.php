<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\ProgramStudi;
use App\Models\Fakultas;
use DataTables;
use Validator;

class ProgramStudiController extends Controller
{
    public function index()
    {
        $fakultas_ = Fakultas::All();
        return view('program-studi.index', compact('fakultas_'));
    }

    public function store(Request $request)
    {        
        $validation = Validator::make($request->all(), [
            'kode' => 'required|unique:program_studi',           
            'nama' => 'required',
            'fakultas_id' => 'required',
        ]);
          
        if ($validation->passes())
        {
            $program_studi = ProgramStudi::create([
                'kode' => $request->kode,           
                'nama' => $request->nama,
                'fakultas_id' => $request->fakultas_id,         
            ]);
    
            return response()->json([
                'data'    => $program_studi,
                'message' => 'Data Program Studi berhasil ditambah',
            ], 201);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);  
    }  
    
    public function edit($id)
    {
        $program_studi=ProgramStudi::find($id);
        // dd($program_studi);
        return $program_studi;
    }

    public function show($id)
    {
        return 'show ' .$id;
    }

    public function update(Request $request, $id)
    {
        $program_studi = ProgramStudi::findOrFail($id);

        $validation = Validator::make($request->all(), [
            'kode' => [
                'required',
                Rule::unique('program_studi')->ignore($program_studi->id),
            ],           
            'nama' => 'required',
            'fakultas_id' => 'required',
        ]);        
        
        if ($validation->passes())
        {
            $program_studi->update([
                'kode' => $request->kode,           
                'nama' => $request->nama,
                'fakultas_id' => $request->fakultas_id,         
            ]);
    
            return response()->json([
                'data'    => $program_studi,
                'message' => 'Data Program Studi berhasil diupdate',                
            ]);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);         
    }

    public function destroy($id)
    {
        $program_studi = ProgramStudi::findOrFail($id);
        $errors = [];

        if ($program_studi->mahasiswa_()->count())
        {
            $error = 'Data Program Studi ini tidak bisa dihapus karena digunakan oleh data mahasiswa';
            $errors[] = $error;
        }

        if ($errors) 
        {
            return response()->json([
                'errors' => $errors
            ], 422);
        } 
        else {
            $program_studi->delete();
            return response()->json([
                'message' => 'Data Program Studi berhasil dihapus'
            ]);
        }
    }

    public function data()
    {
        $program_studi = ProgramStudi::query();

        return DataTables::of($program_studi)
            ->addColumn('fakultas', function($program_studi){
                return  $program_studi->fakultas->nama;
            })        
            ->addColumn('action', function($program_studi){
                return  '<a id="'.$program_studi->id.'" onclick="editForm('.$program_studi->id.')" class="btn btn-info waves-effect"><i class="material-icons">mode_edit</i></a> '.
                        '<a id="'.$program_studi->id.'" onclick="deleteData('.$program_studi->id.')" class="btn btn-danger waves-effect"><i class="material-icons">delete</i></a>';
            })        
            ->make(true);        
    }    
}
