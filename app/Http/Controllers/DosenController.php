<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\Dosen;
use DataTables;
use Validator;

class DosenController extends Controller
{
    public function index()
    {
        return view('dosen.index');
    }

    public function store(Request $request)
    {        
        $validation = Validator::make($request->all(), [
            'nidn' => 'required|unique:dosen',           
            'nama' => 'required',
            'alamat' => 'required',
        ]);
          
        if ($validation->passes())
        {
            $dosen = Dosen::create([
                'nidn' => $request->nidn,           
                'nama' => $request->nama,
                'alamat' => $request->alamat,                
                'program_studi_id' => 1,         
            ]);
    
            return response()->json([
                'data'    => $dosen,
                'message' => 'Data Dosen berhasil ditambah',
            ], 201);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);  
    }  
    
    public function edit($id)
    {
        $dosen=Dosen::find($id);
        return $dosen;
    }

    public function update(Request $request, $id)
    {
        $dosen = Dosen::findOrFail($id);

        $validation = Validator::make($request->all(), [
            'nidn' => [
                'required',
                Rule::unique('dosen')->ignore($dosen->id),
            ],           
            'nama' => 'required',
            'alamat' => 'required',
        ]);        
        
        if ($validation->passes())
        {
            $dosen->update([
                'nidn' => $request->nidn,           
                'nama' => $request->nama,
                'alamat' => $request->alamat,                 
                'program_studi_id' => 1,         
            ]);
    
            return response()->json([
                'data'    => $dosen,
                'message' => 'Data Dosen berhasil diupdate',                
            ]);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);         
    }

    public function destroy($id)
    {
        $dosen = Dosen::findOrFail($id);
        $errors = [];

        // if ($dosen->mahasiswa_()->count())
        // {
        //     $error = 'Data Dosen ini tidak bisa dihapus karena digunakan oleh data mahasiswa';
        //     $errors[] = $error;
        // }

        // if ($errors) 
        // {
        //     return response()->json([
        //         'errors' => $errors
        //     ], 422);
        // } 
        // else {
            $dosen->delete();
            return response()->json([
                'message' => 'Data Dosen berhasil dihapus'
            ]);
        // }
    }

    public function data()
    {
        $dosen = Dosen::query();

        return DataTables::of($dosen)        
            ->addColumn('action', function($dosen){
                return  '<a id="'.$dosen->id.'" onclick="editForm('.$dosen->id.')" class="btn btn-info waves-effect"><i class="material-icons">mode_edit</i></a> '.
                        '<a id="'.$dosen->id.'" onclick="deleteData('.$dosen->id.')" class="btn btn-danger waves-effect"><i class="material-icons">delete</i></a>';
            })        
            ->make(true);        
    }    
}
