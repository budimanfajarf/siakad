<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\Matakuliah;
use DataTables;
use Validator;

class MatakuliahController extends Controller
{
    public function index()
    {
        return view('matakuliah.index');
    }

    public function store(Request $request)
    {        
        $validation = Validator::make($request->all(), [
            'kode' => 'required|unique:matakuliah',           
            'nama' => 'required',
            'sks' => 'required',
            'semester' => 'required',            
        ]);
          
        if ($validation->passes())
        {
            $matakuliah = Matakuliah::create([
                'kode' => $request->kode,           
                'nama' => $request->nama,
                'sks'  => $request->sks,           
                'semester' => $request->semester,                  
                'program_studi_id' => '1',        
            ]);
    
            return response()->json([
                'data'    => $matakuliah,
                'message' => 'Data Matakuliah berhasil ditambah',
            ], 201);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);  
    }  
    
    public function edit($id)
    {
        $matakuliah = Matakuliah::find($id);
        return $matakuliah;
    }

    public function update(Request $request, $id)
    {
        $matakuliah = Matakuliah::findOrFail($id);

        $validation = Validator::make($request->all(), [
            'kode' => [
                'required',
                Rule::unique('matakuliah')->ignore($matakuliah->id),
            ],           
            'nama' => 'required',
            'sks' => 'required',
            'semester' => 'required',
        ]);        
        
        if ($validation->passes())
        {
            $matakuliah->update([
                'kode' => $request->kode,           
                'nama' => $request->nama,  
                'sks'  => $request->sks,           
                'semester' => $request->semester,                  
                'program_studi_id' => '1',
            ]);
    
            return response()->json([
                'data'    => $matakuliah,
                'message' => 'Data Matakuliah berhasil diupdate',                
            ]);
        }   

        return response()->json([
            'errors'  => $validation->errors()->all(),
        ], 422);         
    }

    public function destroy($id)
    {
        $matakuliah = Matakuliah::findOrFail($id);
        $matakuliah->delete();
        return response()->json([
            'message' => 'Data Matakuliah berhasil dihapus'
        ]);
    }

    public function data()
    {
        $matakuliah = Matakuliah::query();

        return DataTables::of($matakuliah)
            ->addColumn('action', function($matakuliah){
                return  '<a id="'.$matakuliah->id.'" onclick="editForm('.$matakuliah->id.')" class="btn btn-info waves-effect"><i class="material-icons">mode_edit</i></a> '.
                        '<a id="'.$matakuliah->id.'" onclick="deleteData('.$matakuliah->id.')" class="btn btn-danger waves-effect"><i class="material-icons">delete</i></a> '.
                        '<a href="/matakuliah/'.$matakuliah->id.'/learning-outcome" class="btn bg-teal waves-effect"><i class="material-icons">settings</i></a> '.
                        '<a href="/matakuliah/'.$matakuliah->id.'/nilai-assesment" class="btn btn-primary waves-effect"><i class="material-icons">assessment</i></a> ';
            })  
            ->make(true);        
    }    
}
