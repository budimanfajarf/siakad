@extends('layouts.wrap')

@section('title', 'Dosen')

@section('css-plugins')
    @parent
    <!-- Sweetalert Css -->
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <button type="button" id="modal-options" class="btn bg-teal btn-block btn-lg waves-effect" onclick="addForm()">
            <i class="material-icons">add</i>
        </button>        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            DOSEN
                        </h2> 
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable" id="dosen-table" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NIDN</th>
                                        <th>Nama Dosen</th>
                                        <th>Alamat</th>                                        
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>                                      
                                        <th>NIDN</th>
                                        <th>Nama Dosen</th>
                                        <th>Alamat</th>       
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- Modal -->
      <div class="modal fade" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
              <form method="post" id="modal-form">
                {{csrf_field()}} {{method_field('post')}}
                <input type="hidden" name="id" id="id">                                                     
                <div class="row clearfix">                                
                  <div class="col-sm-12">
                      <div class="form-group form-float">
                          <div class="form-line">
                              <input type="text" class="form-control" id="nidn" name="nidn">
                              <label class="form-label">NIDN</label>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-12">
                      <div class="form-group form-float">
                          <div class="form-line">
                              <input type="text" class="form-control" id="nama" name="nama">
                              <label class="form-label">Nama Dosen</label>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea rows="4" class="form-control no-resize" id="alamat" name="alamat"></textarea>
                            <label class="form-label">Alamat</label>
                        </div>
                    </div>
                  </div>                 
                </div>
              </form>                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <input type="submit" class="btn btn-primary" value="Simpan" id="action">
            </div>

          </div>
        </div>
      </div>  
      {{-- End Modal --}}
    </div>
</section>
@endsection

@section('script-plugins')
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <script src="plugins/sweetalert/sweetalert.min.js"></script>
    <script src="plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>    
@endsection

@section('script-custom')
    @parent
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <script src="js/pages/ui/notifications.js"></script>
    <script src="js/pages/forms/basic-form-elements.js"></script>   
@endsection

@section('script-bottom')
    <script type="text/javascript">  
        $(document).ready(function(){
            var table = $('#dosen-table').DataTable({
                processing : true,
                serverSide : true,
                ajax       : "{{ route('dosen.data') }}",
                order      :  [[ 0, 'desc' ], ],  
                aLengthMenu: [[ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ]],                              
                columns:[
                    {data: 'id', name: 'id', visible: false, searchable: false},                                 
                    {data: 'nidn', name: 'nidn'},
                    {data: 'nama', name: 'nama'},
                    {data: 'alamat', name: 'alamat'},                    
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            }) 

            $('#action').on('click',function () {
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('dosen') }}";
                else url = "{{ url('dosen') . '/' }}" + id;
                $.ajax({
                    url:url,
                    type:'POST',
                    data:$('#modal-form').serialize(),
                    success:function (data) {
                        $('#modal-form')[0].reset();                        
                        table.ajax.reload();
                        $('#modal').modal('hide');
                        swal({
                            title: 'Berhasil!',
                            text: data.message,
                            type: 'success',
                            timer: '3000'
                        })                        
                    },
                    error:function (data) {
                        dataResponseJson = data.responseJSON;
                        errors = dataResponseJson.errors
                        $.each( errors , function( key, value ) {
                            $.notify({
                                // options
                                message: value
                            },{
                                // settings
                                type: 'danger',
                                z_index: 9999,
                                delay: 3000,
                                placement: {
                                    from: "top",
                                    align: "center"
                                },                                
                            });
                        });                       
                    }

                })
            })

    });

    var url='';
    function addForm() {
        save_method = "add";  
        $('#modal-form .form-group .form-line').removeClass('focused');          
        $('#modal').modal('show');
        $('#id').val("");
        $('#nidn').val("");
        $('#nama').val("");
        $('#alamat').val("");        
        $('#alamat').css("height", "auto");        
        $('input[name=_method').val('POST');
        $('#modal-form')[0].reset();            
        $('#modal-title').text('Tambah Dosen'); 
        $('#action').val('Simpan');                                                
    }     

    function editForm(id) {
        save_method="edit";
        $('#modal-form .form-group .form-line').addClass('focused');
        $('#modal').modal('show');
        $('input[name=_method').val('PUT');
        $('#modal-form')[0].reset();
        $('#modal-title').text('Edit Dosen'); 
        $('#action').val('Update');               
        $.ajax({
            url:"{{url('dosen')}}"+"/"+id+"/edit",
            type:"get",
            dataType:"JSON",
            success:function (data) {
                $('#id').val(data.id);
                $('#nidn').val(data.nidn);
                $('#nama').val(data.nama);
                $('#alamat').val(data.alamat);
            },
            error:function (errors){
                console.log(errors)
            }
        })      
    }

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
                title: "Apa Anda Yakin Akan Menghapus Data ini?",
                text:"Data yang sudah dihapus tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Saya Yakin!",
                cancelButtonText: "Tidak!",
                closeOnConfirm: true,
                closeOnCancel: true 
            },
            function () {
                $.ajax({
                url : "{{ url('dosen') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    swal({
                        title: 'Berhasil!',
                        text: data.message,
                        type: 'success',
                        timer: '3000'
                    })                
                    $('#dosen-table').DataTable().ajax.reload();
                },
                error : function (data) {
                    dataResponseJson = data.responseJSON;
                    errors = dataResponseJson.errors
                    $.each( errors , function( key, value ) {
                        $.notify({
                            // options
                            message: value
                        },{
                            // settings
                            type: 'danger',
                            z_index: 9999,
                            delay: 3000,
                            placement: {
                                from: "top",
                                align: "center"
                            },                                
                        });
                    }); 
                }
              });
            }
        )
    }    
    </script>    
@endsection  