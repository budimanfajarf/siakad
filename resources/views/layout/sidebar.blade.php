<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="{{ asset('images/user.png') }}" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
                <div class="email">{{ Auth::user()->email }}</div>
                {{-- <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div> --}}
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MENU</li>
                <li {{ (starts_with(Route::currentRouteName(), 'dashboard')) ? 'class=active':'' }}>
                    <a href="/">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li {{ (starts_with(Route::currentRouteName(), 'study-outcome')) || (starts_with(Route::currentRouteName(), 'tahun-akademik')) || (starts_with(Route::currentRouteName(), 'matakuliah')) ? 'class=active':'' }}>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">location_city</i>
                        <span>Akademik</span>
                    </a>
                    <ul class="ml-menu">
                        <li {{ (starts_with(Route::currentRouteName(), 'study-outcome')) ? 'class=active':'' }}>
                            <a href="{{ route('study-outcome.index') }}">
                                Study Outcome
                            </a>
                        </li>                  
                        <li {{ (starts_with(Route::currentRouteName(), 'tahun-akademik')) ? 'class=active':'' }}>
                            <a href="{{ route('tahun-akademik.index') }}">
                                Tahun Akademik
                            </a>
                        </li>                
                        <li {{ (starts_with(Route::currentRouteName(), 'matakuliah')) ? 'class=active':'' }}>
                            <a href="{{ route('matakuliah.index') }}">
                                Matakuliah
                            </a>
                        </li>
                    </ul>
                </li>                
                <li {{ (starts_with(Route::currentRouteName(), 'dosen')) || (starts_with(Route::currentRouteName(), 'mahasiswa'))  ? 'class=active':'' }}>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">people</i>
                        <span>Civitas</span>
                    </a>
                    <ul class="ml-menu">
                        <li {{ (starts_with(Route::currentRouteName(), 'dosen')) ? 'class=active':'' }}>
                            <a href="{{ route('dosen.index') }}">
                                Dosen
                            </a>
                        </li> 
                        <li {{ (starts_with(Route::currentRouteName(), 'mahasiswa')) ? 'class=active':'' }}>
                            <a href="{{ route('mahasiswa.index') }}">
                                Mahasiswa
                            </a>
                        </li>                         
                    </ul>
                </li>
                <li {{ (starts_with(Route::currentRouteName(), 'ruang')) || (starts_with(Route::currentRouteName(), 'jadwal')) || (starts_with(Route::currentRouteName(), 'krs')) ? 'class=active':'' }}>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">school</i>
                        <span>Perkuliahan</span>
                    </a>
                    <ul class="ml-menu">
                        <li {{ (starts_with(Route::currentRouteName(), 'ruang')) ? 'class=active':'' }}>
                            <a href="{{ route('ruang.index') }}">
                                Ruang
                            </a>
                        </li>                  
                        <li {{ (starts_with(Route::currentRouteName(), 'jadwal')) ? 'class=active':'' }}>
                            <a href="{{ route('jadwal.index') }}">
                                Jadwal
                            </a>
                        </li>                
                        <li {{ (starts_with(Route::currentRouteName(), 'krs')) ? 'class=active':'' }}>
                            <a href="{{ route('krs.index') }}">
                                KRS
                            </a>
                        </li>
                    </ul>
                </li>                                                                                
            </ul>
        </div>
        <!-- #Menu -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>