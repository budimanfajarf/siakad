<!-- background -->
    <img class="mybg" src="{{ asset('images/background.jpg') }}" alt="" style="z-index: -1;">
<!-- end background -->  

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader pl-size-xl">
            <div class="spinner-layer pl-teal">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>        
    </div>
</div>
<!-- #END# Page Loader -->