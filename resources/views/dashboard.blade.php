@extends('layout.wrap')

@section('title', 'Dashboard')

@section('css-plugins')
    @parent
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
    <style>
        .info-box {
            cursor: pointer
        }
    </style>
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        
        <div class="row">
            
            <a href="/study-outcome">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect waves-effect">
                        <div class="icon bg-teal bg-gradient">
                            <i class="material-icons">school</i>
                        </div>
                        <div class="content">
                            <div class="text">STUDY OUTCOME</div>
                            <div class="number count-to" 
                                 data-from="0" 
                                 data-to="{{ $count['study_outcome'] }}" 
                                 data-speed="1000" 
                                 data-fresh-interval="20"
                            >{{ $count['study_outcome'] }}</div>
                        </div>
                    </div>
                </div>
            </a>

            <a href="/tahun-akademik">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect waves-effect">
                        <div class="icon bg-teal bg-gradient">
                            <i class="material-icons">date_range</i>
                        </div>
                        <div class="content">
                            <div class="text">TAHUN AKADEMIK</div>
                            <div class="number count-to" 
                                 data-from="0" 
                                 data-to="{{ $count['tahun_akademik'] }}" 
                                 data-speed="1000" 
                                 data-fresh-interval="20"
                            >{{ $count['tahun_akademik'] }}</div>
                        </div>
                    </div>
                </div>
            </a>
            
            <a href="/matakuliah">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect waves-effect">
                        <div class="icon bg-teal bg-gradient">
                            <i class="material-icons">library_books</i>
                        </div>
                        <div class="content">
                            <div class="text">MATAKULIAH</div>
                            <div class="number count-to" 
                                 data-from="0" 
                                 data-to="{{ $count['matakuliah'] }}" 
                                 data-speed="1000" 
                                 data-fresh-interval="20"
                            >{{ $count['matakuliah'] }}</div>
                        </div>
                    </div>
                </div>
            </a>
                
            <a href="/dosen">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect waves-effect">
                        <div class="icon bg-teal bg-gradient">
                            <i class="material-icons">supervisor_account</i>
                        </div>
                        <div class="content">
                            <div class="text">DOSEN</div>
                            <div class="number count-to" 
                                 data-from="0" 
                                 data-to="{{ $count['dosen'] }}" 
                                 data-speed="1000" 
                                 data-fresh-interval="20"
                            >{{ $count['dosen'] }}</div>
                        </div>
                    </div>
                </div>
            </a>
            
            <a href="/mahasiswa">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect waves-effect">
                        <div class="icon bg-teal bg-gradient">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content">
                            <div class="text">MAHASISWA</div>
                            <div class="number count-to" 
                                 data-from="0" 
                                 data-to="{{ $count['mahasiswa'] }}" 
                                 data-speed="1000" 
                                 data-fresh-interval="20"
                            >{{ $count['mahasiswa'] }}</div>
                        </div>
                    </div>
                </div>
            </a>

            <a href="/ruang">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect waves-effect">
                        <div class="icon bg-teal bg-gradient">
                            <i class="material-icons">grid_on</i>
                        </div>
                        <div class="content">
                            <div class="text">RUANG</div>
                            <div class="number count-to" 
                                 data-from="0" 
                                 data-to="{{ $count['ruang'] }}" 
                                 data-speed="1000" 
                                 data-fresh-interval="20"
                            >{{ $count['jadwal'] }}</div>
                        </div>
                    </div>
                </div>
            </a>            
            
            <a href="/jadwal">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect waves-effect">
                        <div class="icon bg-teal bg-gradient">
                            <i class="material-icons">today</i>
                        </div>
                        <div class="content">
                            <div class="text">JADWAL</div>
                            <div class="number count-to" 
                                 data-from="0" 
                                 data-to="{{ $count['jadwal'] }}" 
                                 data-speed="1000" 
                                 data-fresh-interval="20"
                            >{{ $count['jadwal'] }}</div>
                        </div>
                    </div>
                </div>
            </a>
            
            <a href="/krs">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box hover-zoom-effect waves-effect">
                        <div class="icon bg-teal bg-gradient">
                            <i class="material-icons">note</i>
                        </div>
                        <div class="content">
                            <div class="text">KRS</div>
                            <div class="number count-to" 
                                 data-from="0" 
                                 data-to="{{ $count['krs'] }}" 
                                 data-speed="1000" 
                                 data-fresh-interval="20"
                            >{{ $count['krs'] }}</div>
                        </div>
                    </div>
                </div>
            </a>

        </div>
    </div>
</section>
@endsection

@section('script-plugins')
    @parent
    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('plugins/jquery-countto/jquery.countTo.js') }}"></script>    
@endsection

@section('script-custom')
    @parent
@endsection

@section('script-bottom')
    @parent  
    <script>
        $(function () {
            initCounters();
        });

        //Widgets count plugin
        function initCounters() {
            $('.count-to').countTo();
        }    
    </script>
@endsection  