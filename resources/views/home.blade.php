<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SIAKAD - UNJANI</title>

    <!-- Bootstrap -->
    <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    html{
      overflow-x: hidden;
    }
      body{
        background-image: url(/images/bg.jpg);
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
      }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">SIAKAD - UNJANI</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Link</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <h2 class="text-center">Selamat Datang Di SIAKAD UNJANI</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-2 col-content-left">
          <p class="text-justify">
            Bagian ini terdiri dari Input Fakultas, Input Prodi, Input Dosen, Input Mahasiswa, Input Jenis Mata Kuliah, Input Tahun Akademik.
          </p>
        </div>
        <div class="col-md-1 col-image-left">
          <img src="/images/start.png" class="img-responsive">
        </div>
        <div class="col-sm-6 col-md-2 text-center">
          <a href="/fakultas">
          <div class="lab_item first">    
            <div class="hexagon hexagon2">
              <div class="hexagon-in1">
                <div class="hexagon-in2" style="background-image: url('/images/Academik.png');">                
                </div>
              </div>
            </div>
          </div>
          </a>
        </div>
        <div class="col-sm-6 col-md-2 col-md-offset-2 text-center">
          <div class="lab_item first res">    
            <div class="hexagon hexagon2">
              <div class="hexagon-in1">
                <div class="hexagon-in2" style="background-image: url('/images/Kuliah.png');">                
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-1 col-image-right">
          <img src="/images/start-right.png" class="img-responsive">
        </div>
        <div class="col-sm-6 col-md-2 col-content-right">
          <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-2 col-md-offset-1 col-content-left-bottom">
          <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
        </div>
        <div class="col-md-1 col-image-left-bottom">
          <img src="/images/start.png" class="img-responsive">
        </div>
        <div class="col-sm-6 col-md-2 text-center">
          <div class="lab_item second">    
            <div class="hexagon hexagon2">
              <div class="hexagon-in1">
                <div class="hexagon-in2" style="background-image: url('/images/KRS.png');">                
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-2 text-center">
          <div class="lab_item second">    
            <div class="hexagon hexagon2">
              <div class="hexagon-in1">
                <div class="hexagon-in2" style="background-image: url('images/Nilai.png');">                
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-1 col-image-right-bottom">
          <img src="/images/start-right.png" class="img-responsive">
        </div>
        <div class="col-sm-6 col-md-2 col-content-right-bottom">
          <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
        </div>
      </div>
    </div>

    <!-- <div class="lab_item">    
      <div class="hexagon hexagon2">
        <div class="hexagon-in1">
          <div class="hexagon-in2" style="background-image: url('http://placekitten.com/200/305');">                
          </div>
        </div>
      </div>
    </div> -->
  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <script>
      
      // $(document).ready(function () {
      //   if (window.matchMedia('(max-width: 767px)').matches) {
      //       $('.col-image-right', '.col-image-left', '.col-image-left-bottom', '.col-image-right-bottom').css('display','none')
      //       console.log('hai')
      //   } 
      // })

      // $(window).resize(function(){
      //   if ($(window).width() >= 768 && $(window).width() <= 992){  
      //     $('.col-image-right').css('display','none');
      //     $('.col-image-left').css('display','none');
      //     $('.col-image-left-bottom').css('display','none');
      //     $('.col-image-right-bottom').css('display','none');
      //     $('.col-sm-12').removeClass('col-sm-12')
      //   }
         
      // });

    </script>
  </body>
</html>

