@extends('layouts.wrap')

@section('title', 'Jadwal')

@section('css-plugins')
    @parent
    <!-- Sweetalert Css -->
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <button type="button" id="modal-options" class="btn bg-teal btn-block btn-lg waves-effect">
            <i class="material-icons">apps</i>
        </button>
        <button type="button" id="modal-edit" class="btn bg-teal btn-block btn-lg waves-effect" onclick="editForm()">
            <i class="material-icons">border_color</i>
        </button>
        <button type="button" id="modal-delete" class="btn bg-teal btn-block btn-lg waves-effect" onclick="deleteData();">
            <i class="material-icons">delete</i>
        </button>
        <button type="button" id="modal-view" class="btn bg-teal btn-block btn-lg waves-effect" onclick="modalView()">
            <i class="material-icons">visibility</i>
        </button>
        <button type="button" id="modal-button" class="btn bg-teal btn-block btn-lg waves-effect" onclick="addForm()">
            <i class="material-icons">add</i>
        </button>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            JADWAL KULIAH
                        </h2> 
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable" id="jadwal-table" width="100%">
                                <thead>
                                    <tr>
                                    	<th>Kelas</th>
                                    	<th>Hari</th>
                                    	<th>Jam Mulai</th>
                                    	<th>Jam Selesai</th>
                                        <th>Tahun Akaddemik</th>
                                        <th>Matakuliah</th>
                                        <th>Dosen</th>
                                        <th>Nilai</th>
                                        <!-- <th>Semester</th>
                                        <th>Ruang</th> -->
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    	<th>Kelas</th>
                                    	<th>Hari</th>
                                    	<th>Jam Mulai</th>
                                    	<th>Jam Selesai</th>
                                        <th>Tahun Akaddemik</th>
                                        <th>Matakuliah</th>
                                        <th>Dosen</th>
                                        <th>Nilai</th>
                                        <!-- <th>Semester</th>
                                        <th>Ruang</th> -->
                                </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- Modal -->
      <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
              <div class="row clearfix">
                <form method="post" id="modal-form">
                  {{csrf_field()}} {{method_field('post')}}
                  <input type="hidden" name="id" id="id">                  
                  <div class="col-sm-4 mar-bot">
                    <select class="form-control show-tick" name="tahun_akademik_id" id="tahun_akademik_id">
                        <option value="">-- Pilih Tahun Akademik --</option>
                        @foreach ($thnakademik as $thn)
                            <option value="{{ $thn->id }}">{{ $thn->nama }}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="col-sm-4 mar-bot">
	                 <select class="form-control show-tick" name="kelas" id="kelas">
                        <option value="">-- Pilih Kelas --</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
	                </select>
                  </div>
                  <div class="col-sm-4 mar-bot">
                    <select class="form-control show-tick semester" name="semester" id="semester">
                        <option value="" id="pilih-semester">-- Pilih Semester --</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                    </select>
                  </div>
                  <div class="col-sm-12">
                  	<div class="table-responsive" style="display: none;" id="wrap-table-jadwal">
                  		<table class="table table-bordered table-striped table-hover" id="create-jadwal-table">
	                      	<tr>
                                <th class="hide-th-jadwal bulk-delete-option">
                                    <button class="btn btn-sm btn-danger" id="button-bulk-delete">x</button>
                                </th>
	                      		<th>Matakuliah</th>
	                      		<th>Dosen</th>
                                <th class="hide-th-jadwal" style="display: none;">Tahun Akademik</th>
	                      		<th>Ruang</th>
                                <th class="hide-th-jadwal" style="display: none;">Semester</th>
                                <th>Hari</th>
                                <th class="hide-th-jadwal" style="display: none;">Kelas</th>
	                      		<th>Mulai</th>
	                      		<th>Selesai</th>
	                      	</tr>
	                      	<tbody id="body-create-jadwal">
	                      		
	                      	</tbody>
                      </table>
                  	</div>
                  </div>
                </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <input type="submit" class="btn btn-primary" value="Update" id="update-jadwal" style="display: none">
              <input type="submit" class="btn btn-primary button-action" value="Simpan" id="action">
            </div>

          </div>
        </div>
      </div>  
      {{-- End Modal --}}
    </div>
</section>
@endsection

@section('script-plugins')
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <!-- SweetAlert Plugin Js -->
    <script src="plugins/sweetalert/sweetalert.min.js"></script>
@endsection

@section('script-custom')
    @parent
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <script src="js/pages/ui/notifications.js"></script>
@endsection

@section('script-bottom')
    <script type="text/javascript">
        var url='';
        var save_method='';
        var semesterChange='';
        function modalView() {
            save_method='view';
            semesterChange='view';
            $('#update-jadwal').css('display','none')
            $('.hide-th-jadwal').css('display','inline-block')
            $('.bulk-delete-option').css('display','none')
            $('#wrap-table-jadwal').css('display','none')
            $('#semester').selectpicker('deselectAll'); 
            $('#kelas').selectpicker('deselectAll'); 
            $('#tahun_akademik_id').selectpicker('deselectAll'); 
            $('input[name=_method').val('PUT');
            $('#modal-form')[0].reset();
            $('#modal-title').text('View Detail Jadwal'); 
            $('#action').val('Tampilkan'); 
            // $('#pilih-semester').selected=true;
            $('#semester').removeClass('semester');
            $('#semester').addClass('semester-view');
            $('#modal').modal('show');
            // $('.button-action').attr('id','view-jadwal');
            $('#body-create-jadwal').remove('tr')

            
        }  

        $(document).ready(function(){
            var table = $('#jadwal-table').DataTable({
                processing : true,
                serverSide : true,
                ajax       : "{{ route('jadwal.data') }}",
                // order      :  [[ 0, 'desc' ], ],  
                // aLengthMenu: [[ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ]],                              
                columns:[
                    // {data: 'id', name: 'id', visible: false, searchable: false},                                 
                    // {data: 'kode', name: 'kode'},
                    {data:'kelas'},
                    {data:'hari'},
                    {data:'jam_mulai'},
                    {data:'jam_selesai'},
                    {data: 'tahun_akademik'},
                    {data: 'matakuliah'},
                    {data: 'dosen'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                    // {data: 'semester'},
                    // {data: 'ruang'},
                    // {data:'jam_mulai'},
                    // {data:'jam_selesai'}
                    // {data: 'tahun_akademik', name: 'tahun_akademik'},
                    // {data: 'matakuliah', name: 'matakuliah'},
                ],
            }) 

            // $('#view-jadwal').on('click',function () {
            //     $.ajax({
            //         url:"{{url('jadwal')}}"+"/"+1+"/"+2+"/"+3+"/getview",
            //         type:"get",
            //         dataType:"JSON",
            //         success:function (data) {
            //             console.log(data)
            //         }
            //     })
            // })

            $('#action').on('click',function () {
                // var id = $('#id').val();
                var id='';
                if (save_method == 'add'){
                    url = "{{ url('jadwal') }}";
                    $.ajax({
                        url:url,
                        type:'POST',
                        data:$('#modal-form').serialize(),
                        success:function (data) {
                            $('#modal-form')[0].reset();                        
                            table.ajax.reload();
                            $('#modal').modal('hide');
                            swal({
                                title: 'Berhasil!',
                                text: data.message,
                                type: 'success',
                                timer: '3000'
                            })
                            console.log(data) 
                            // $('#jadwal-table').DataTable().ajax.reload();                       
                        },
                        error:function (data) {
                            dataResponseJson = data.responseJSON;
                            errors = dataResponseJson.errors
                            $.each( errors , function( key, value ) {
                                $.notify({
                                    // options
                                    message: value
                                },{
                                    // settings
                                    type: 'danger',
                                    z_index: 9999,
                                    delay: 3000,
                                    placement: {
                                        from: "top",
                                        align: "center"
                                    },                                
                                });
                            });                       
                        }

                    })
                }
                else if(save_method=='view'){
                    var tr='';
                    $('#wrap-table-jadwal').css('display','none')
                    var idsemester=$('#semester').val();
                    var idThnAkademik=$('#tahun_akademik_id').val();
                    var idKelas=$('#kelas').val();
                    $('input[name=_method').val('PUT');
                    $.ajax({
                        url:"{{url('jadwal')}}"+"/"+idThnAkademik+"/"+idKelas+"/"+idsemester+"/getview",
                        type:"get",
                        dataType:"JSON",
                        success:function (data) {
                            for(var i=1; i<data.length;i++){
                                tr+='<tr id="tr-add">'+
                                        '<td>'+data[i]['matkul']+'</td>'+
                                        '<td>'+data[i]['dosen_nama']+'</td>'+
                                        '<td>'+data[i]['thnakademik']+'</td>'+
                                        '<td>'+data[i]['ruang_nama']+'</td>'+
                                        '<td>'+data[i]['semester']+'</td>'+
                                        '<td>'+data[i]['hari']+'</td>'+
                                        '<td>'+data[i]['kelas']+'</td>'+
                                        '<td>'+data[i]['jam_mulai']+'</td>'+
                                        '<td>'+data[i]['jam_selesai']+'</td>'+
                                    '</tr>'
                            }
                            $('#wrap-table-jadwal').css('display','block')
                            $('#body-create-jadwal').html(tr)
                        }
                    })
                }

                else if(save_method=='delete'){
                    var tr='';
                    // $('#wrap-table-jadwal').css('display','none')
                    var idsemester=$('#semester').val();
                    var idThnAkademik=$('#tahun_akademik_id').val();
                    var idKelas=$('#kelas').val();
                    $('input[name=_method').val('PUT');
                    $.ajax({
                        url:"{{url('jadwal')}}"+"/"+idThnAkademik+"/"+idKelas+"/"+idsemester+"/getview",
                        type:"get",
                        dataType:"JSON",
                        success:function (data) {
                            for(var i=1; i<data.length;i++){
                                tr+='<tr id="tr-add">'+
                                        '<td><input type="checkbox" name="jadwal-id[]" style="left:auto; opacity:1;" value="'+data[i]['id_jadwal']+'" class="jadwal-check"/></td>'+
                                        '<td>'+
                                            data[i]['matkul']+
                                        '</td>'+
                                        '<td>'+
                                            data[i]['dosen_nama']+
                                        '</td>'+
                                        '<td>'+
                                            data[i]['thnakademik']+
                                        '</td>'+
                                        '<td>'+data[i]['ruang_nama']+'</td>'+
                                        '<td>'+data[i]['semester']+'</td>'+
                                        '<td>'+data[i]['hari']+'</td>'+
                                        '<td>'+data[i]['kelas']+'</td>'+
                                        '<td>'+data[i]['jam_mulai']+'</td>'+
                                        '<td>'+data[i]['jam_selesai']+'</td>'+
                                    '</tr>'
                            }
                            $('#wrap-table-jadwal').css('display','block')
                            $('#body-create-jadwal').html(tr)
                            // console.log(data)
                            // console.log('hai view');
                            // console.log($('#semester').val())
                        }
                    })
                }

                else if(save_method=='edit'){
                    var tr='';
                    $('#wrap-table-jadwal').css('display','block')
                    var idsemester=$('#semester').val();
                    var idThnAkademik=$('#tahun_akademik_id').val();
                    var idKelas=$('#kelas').val();
                    var optionDosen='';
                    var optionRuang='';
                    var optionHari='<option value="senin">Senin</option>'+
                                    '<option value="selasa">Selasa</option>'+
                                    '<option value="rabu">Rabu</option>'+
                                    '<option value="kamis">Kamis</option>'+
                                    '<option value="jumat">Jumat</option>'+
                                    '<option value="sabtu">Sabtu</option>';

                    $('input[name=_method').val('PUT');
                    $.ajax({
                        url:"{{url('jadwal')}}"+"/"+idThnAkademik+"/"+idKelas+"/"+idsemester+"/getview",
                        type:"get",
                        dataType:"JSON",
                        success:function (data) {
                            console.log(data)
                            // console.log(data['dosen_table'])
                            for(let j=0; j<data[0]['dosen_table'].length; j++){
                                // console.log(data[0]['dosen_table'][j])
                                optionDosen+='<option value="'+data[0]['dosen_table'][j]['id']+'">'+data[0]['dosen_table'][j]['nama']+'</option>';
                            }

                            for(let j=0; j<data[0]['ruang_table'].length; j++){
                                // console.log(data[0]['ruang_table'][j])
                                optionRuang+='<option value="'+data[0]['ruang_table'][j]['id']+'">'+data[0]['ruang_table'][j]['nama']+'</option>';
                            }


                            for(var i=1; i<data.length;i++){
                                // console.log(data[0]['dosen_table'])
                                tr+='<tr id="tr-add">'+
                                        '<input type="hidden" name="id_jadwal_update[]" value="'+data[i]['id_jadwal']+'">'+
                                        '<td><input type="hidden" data-id="'+data[i]['id_jadwal']+'" name="matkul_id[]" value="'+data[i]['matkul_id']+'" class="id-update">'+
                                            data[i]['matkul']+
                                        '</td>'+
                                        '<td>'+
                                            '<select name="dosen[]" class="select-ajax">'+
                                                '<option value="'+data[i]['dosen_id']+'">'+data[i]['dosen_nama']+"</option>"+
                                                optionDosen+
                                            '</select>'+
                                        '</td>'+
                                        '<td>'+
                                            data[i]['thnakademik']+
                                        '</td>'+
                                        '<td>'+
                                            '<select name="ruang[]" class="select-ajax">'+
                                                '<option value="'+data[i]['ruang_id']+'">'+data[i]['ruang_nama']+"</option>"+
                                                optionRuang+
                                            '</select>'+
                                        '</td>'+
                                        '<td>'+data[i]['semester']+'</td>'+
                                        '<td>'+
                                            '<select name="hari[]" class="select-ajax">'+
                                                '<option value="'+data[i]['hari']+'">'+data[i]['hari']+"</option>"+
                                                optionHari+
                                            '</select>'+
                                        '</td>'+
                                        '<td>'+data[i]['kelas']+'</td>'+
                                        '<td><input type="text" name="jam_mulai[]" value="'+data[i]['jam_mulai']+'" class="time-jadwal"></td>'+
                                        '<td><input type="text" name="jam_selesai[]" value="'+data[i]['jam_selesai']+'" class="time-jadwal"></td>'+
                                    '</tr>'
                            }
                            // $('.modal-footer').append('<input type="submit" class="btn btn-primary" value="Update" id="update-jadwal">')
                            $('#update-jadwal').css('display','inline-block')
                            
                            $('#body-create-jadwal').html(tr)
                        }
                    })
                }
                
                
            })

            $('#update-jadwal').on('click',function () {

                var idThn=$('#tahun_akademik_id').val()
                var idKls=$('#kelas').val()
                var idSmstr=$('#semester').val()
                var idj=$('.id-update');
                var idValue=[];
                for (var i = 0; i < idj.length; i++) {
                    idValue[i]=idj[i].getAttribute('data-id');
                }
                $.ajax({
                    url:"{{url('jadwal')}}",
                    type:'post',
                    data:$('#modal-form').serialize(),
                    success:function (data) {
                        table.ajax.reload();
                        $('#modal').modal('hide');
                        swal({
                            title: 'Berhasil!',
                            text: 'Jadwal Berhasil di Update',
                            type: 'success',
                            timer: '3000'
                        })
                        console.log(data)
                        console.log(idValue)
                        // ($('.id-update'))
                    },
                    error:function(xhr) {
                        var err = JSON.parse(xhr.responseText);
                        alert(err.message);
                        console.log(err.message)
                    }              
                })
            })
             

    });

    
    function addForm() {
        save_method = "add";
        semesterChange='add';
        $('#update-jadwal').css('display','none')
        $('.hide-th-jadwal').css('display','none')
        $('#semester').selectpicker('deselectAll'); 
        $('#kelas').selectpicker('deselectAll'); 
        $('#tahun_akademik_id').selectpicker('deselectAll'); 
        $('#semester').removeClass('semester-view').addClass('semester')
        $('#wrap-table-jadwal').css('display','none'); 
        $('#modal-form .form-group .form-line').removeClass('focused');          
        $('#modal').modal('show');
        $('#id').val("");
        $('#kode').val("");
        $('#nama').val("");
        $('input[name=_method').val('POST');
        $('#modal-form')[0].reset();            
        $('#modal-title').text('Tambah Jadwal'); 
        $('#action').val('Simpan');  

        // $('.semester').on('change',function(e){
        //     $('#wrap-table-jadwal').css('display','block');
        //     var body=$('#body-create-jadwal');
        //     var tr='';
        //     var id=e.target.value;
        //     var dosen='<option>Pilih Dosen</option>';
        //     var ruang='<option>Pilih Ruang</option>';
        //     $.ajax({
        //         url:"{{url('jadwal')}}"+"/"+id+"/select",
        //         type:'get',
        //         dataType:"JSON",
        //         success:function (data) {
        //             $.each(data.ruang,function (i,e) {
        //                 ruang+='<option value="'+e.id+'">'+e.nama+'</option>';
        //             })
        //             $.each(data.dosen,function (i,e) {
        //                 dosen+='<option value="'+e.id+'">'+e.nama+'</option>';
        //             })
        //             $.each(data.matkul,function (i,e) {
        //                 tr+='<tr id="tr-add">'+
                    
        //                     '<td><input type="hidden" name="matkul_id[]" value="'+e.id+'">'+e.nama+'</td>'+
        //                     '<td>'+
        //                         '<select name="dosen[]">'+
        //                             dosen+
        //                         '</select>'+
        //                     '</td>'+
        //                     '<td>'+
        //                         '<select name="ruang[]">'+
        //                             ruang+
        //                         '</select>'+
        //                     '</td>'+
        //                     '<td>'+
        //                         '<select name="hari[]">'+
        //                             '<option value="senin">Senin</option>'+
        //                             '<option value="selasa">Selasa</option>'+
        //                             '<option value="rabu">Rabu</option>'+
        //                             '<option value="kamis">Kamis</option>'+
        //                             '<option value="jumat">Jumat</option>'+
        //                             '<option value="sabtu">Sabtu</option>'+
        //                         '</select>'+
        //                     '</td>'+
        //                     '<td><input type="text" name="jam_mulai[]"></td>'+
        //                     '<td><input type="text" name="jam_selesai[]"></td>'+
        //                 '</tr>'
        //             })
        //             body.html(tr)

        //         }

        //     })
        // })                                              
    }     

    function editForm() {
        save_method="edit";
        semesterChange='edit'
        // $('#create-jadwal-table').css('display','none')
        $('#update-jadwal').css('display','none')
        $('.hide-th-jadwal').css('display','inline-block')
        $('.bulk-delete-option').css('display','none')
        $('#semester').removeClass('semester')
        $('#semester').selectpicker('deselectAll'); 
        $('#kelas').selectpicker('deselectAll'); 
        $('#tahun_akademik_id').selectpicker('deselectAll');
        $('#wrap-table-jadwal').css('display','none')
        // $('#modal-form .form-group .form-line').addClass('focused');
        $('#modal').modal('show');
        $('input[name=_method').val('PUT');
        $('#modal-form')[0].reset();
        $('#modal-title').text('Edit Jadwal'); 
        $('#action').val('Tampilkan');               
        // $.ajax({
        //     url:"{{url('fakultas')}}"+"/"+id+"/edit",
        //     type:"get",
        //     dataType:"JSON",
        //     success:function (data) {
        //         $('#id').val(data.id);
        //         $('#kode').val(data.kode);
        //         $('#nama').val(data.nama);
        //     }
        // })      
    }

    function deleteData(){
        save_method="delete";
        semesterChange='delete'
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        // $('#create-jadwal-table').css('display','none')
        $('#update-jadwal').css('display','none');
        $('.hide-th-jadwal').css('display','inline-block')
        $('#wrap-table-jadwal').css('display','none')
        $('#semester').selectpicker('deselectAll'); 
        $('#kelas').selectpicker('deselectAll'); 
        $('#wrap-table-jadwal').css('display','none')
        $('#tahun_akademik_id').selectpicker('deselectAll'); 
        $('#modal').modal('show');
        $('#modal-title').text('Delete Jadwal'); 
        $('#action').val('Tampilkan'); 
        // swal({
        //         title: "Apa Anda Yakin Akan Menghapus Data ini?",
        //         text:"Data yang sudah dihapus tidak bisa dikembalikan!",
        //         type: "warning",
        //         showCancelButton: true,
        //         confirmButtonColor: "#DD6B55",
        //         confirmButtonText: "Ya, Saya Yakin!",
        //         cancelButtonText: "Tidak!",
        //         closeOnConfirm: true,
        //         closeOnCancel: true 
        //     },
        //     function () {
        //         $.ajax({
        //         url : "{{ url('fakultas') }}" + '/' + id,
        //         type : "POST",
        //         data : {'_method' : 'DELETE', '_token' : csrf_token},
        //         success : function(data) {
        //             swal({
        //                 title: 'Berhasil!',
        //                 text: data.message,
        //                 type: 'success',
        //                 timer: '3000'
        //             })                
        //             $('#fakultas-table').DataTable().ajax.reload();
        //         },
        //         error : function (data) {
        //             dataResponseJson = data.responseJSON;
        //             errors = dataResponseJson.errors
        //             $.each( errors , function( key, value ) {
        //                 $.notify({
        //                     // options
        //                     message: value
        //                 },{
        //                     // settings
        //                     type: 'danger',
        //                     z_index: 9999,
        //                     delay: 3000,
        //                     placement: {
        //                         from: "top",
        //                         align: "center"
        //                     },                                
        //                 });
        //             }); 
        //         }
        //       });
        //     }
        // )
    }

    $('#semester').on('change',function(e){
        if (semesterChange=='add') {
            $('#wrap-table-jadwal').css('display','block');
            var body=$('#body-create-jadwal');
            var tr='';
            var id=e.target.value;
            var dosen;
            var ruang;
            var number=0;
            $.ajax({
                url:"{{url('jadwal')}}"+"/"+id+"/select",
                type:'get',
                dataType:"JSON",
                success:function (data) {
                    $.each(data.ruang,function (i,e) {
                        ruang+='<option value="'+e.id+'">'+e.nama+'</option>';
                    })
                    $.each(data.dosen,function (i,e) {
                        dosen+='<option value="'+e.id+'">'+e.nama+'</option>';
                    })
                    $.each(data.matkul,function (i,e) {
                        number++;
                        tr+='<tr id="tr-add">'+
                            '<input type="hidden" name="number" value="'+number+'">'+
                            '<td><input type="hidden" name="matkul_id[]" value="'+e.id+'">'+e.nama+'</td>'+
                            '<td>'+
                                '<select name="dosen[]" class="select-ajax">'+
                                    dosen+
                                '</select>'+
                            '</td>'+
                            '<td>'+
                                '<select name="ruang[]" class="select-ajax">'+
                                    ruang+
                                '</select>'+
                            '</td>'+
                            '<td>'+
                                '<select name="hari[]" class="select-ajax">'+
                                    '<option value="senin">Senin</option>'+
                                    '<option value="selasa">Selasa</option>'+
                                    '<option value="rabu">Rabu</option>'+
                                    '<option value="kamis">Kamis</option>'+
                                    '<option value="jumat">Jumat</option>'+
                                    '<option value="sabtu">Sabtu</option>'+
                                '</select>'+
                            '</td>'+
                            '<td><input type="text" name="jam_mulai[]" class="time-jadwal"></td>'+
                            '<td><input type="text" name="jam_selesai[]" class="time-jadwal"></td>'+
                        '</tr>'
                    })
                    body.html(tr)
                }

            })
        } 
        else if(semesterChange=='view' || semesterChange=='edit' || semesterChange=='delete'){
            console.log('...')
        }
    		
	})

    $(document).on('click','#button-bulk-delete',function (e) {
        var id=[];
        e.preventDefault();
        if (confirm('Apakah Anda Yakin?')) 
        {
            $('.jadwal-check:checked').each(function () {
                id.push($(this).val())
                console.log(id)
            })
            if (id.length>0) 
            {
                $.ajax({
                  url:"{{url('jadwal/bulkdelete')}}",
                  method:'get',
                  data:{id:id},
                  success:function (data) {
                      console.log(data)
                      $('.modal').modal('hide');
                      $('#jadwal-table').DataTable().ajax.reload();
                  },
                  error:function (data) {
                      console.log(data)
                  }
                })
            }
            else 
            {
                alert('Mohon diceklis salah satu untuk menghapus')
            }
        } 
        
    })

    // $(document).on('click','#update-jadwal',function () {
    //     var idThn=$('#tahun_akademik_id').val()
    //     var idKls=$('#kelas').val()
    //     var idSmstr=$('#semester').val()

    //     $.ajax({
    //         url:"{{url('jadwal')}}"+"/"+idThn+"/"+idKls+"/"+idSmstr,
    //         type:'POST',
    //         data:$('#modal-form').serialize(),
    //         success:function (data) {
    //             table.ajax.reload();
    //             $('#modal').modal('hide');
    //             swal({
    //                 title: 'Berhasil!',
    //                 text: 'Jadwal Berhasil di Update',
    //                 type: 'success',
    //                 timer: '3000'
    //             })
    //             console.log(data)
    //         },
    //         error:function(xhr) {
    //             var err = JSON.parse(xhr.responseText);
    //             alert(err.message);

    //         }              
    //     })
    // })



    $('#modal-options').click(function () {
        $('#modal-button').slideToggle(200)
        $('#modal-delete').slideToggle(400)
        $('#modal-edit').slideToggle(600)
        $('#modal-view').slideToggle(800)
    }) 


   
 
    </script>    
@endsection  