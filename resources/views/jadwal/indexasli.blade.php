@extends('layouts.wrap')

@section('title', 'Jadwal')

@section('css-plugins')
    @parent
    <!-- Sweetalert Css -->
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header"><h2>JADWAL</h2></div>
        <button type="button" id="modal-options" class="btn bg-teal btn-block btn-lg waves-effect">
            <i class="material-icons">build</i>
        </button>
        <button type="button" id="modal-edit" class="btn bg-teal btn-block btn-lg waves-effect">
            <i class="material-icons">border_color</i>
        </button>
        <button type="button" id="modal-delete" class="btn bg-teal btn-block btn-lg waves-effect">
            <i class="material-icons">delete</i>
        </button>
        <button type="button" id="modal-view" class="btn bg-teal btn-block btn-lg waves-effect">
            <i class="material-icons">visibility</i>
        </button>
        <button type="button" id="modal-button" class="btn bg-teal btn-block btn-lg waves-effect" onclick="addForm()">
            <i class="material-icons">add</i>
        </button>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            DAFTAR JADWAL
                        </h2> 
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable" id="jadwal-table" width="100%">
                                <thead>
                                    <tr>
                                    	<th>Kelas</th>
                                    	<th>Hari</th>
                                    	<th>Jam Mulai</th>
                                    	<th>Jam Selesai</th>
                                        <th>Tahun Akaddemik</th>
                                        <th>Matakuliah</th>
                                        <th>Dosen</th>
                                        <!-- <th>Semester</th>
                                        <th>Ruang</th> -->
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    	<th>Kelas</th>
                                    	<th>Hari</th>
                                    	<th>Jam Mulai</th>
                                    	<th>Jam Selesai</th>
                                        <th>Tahun Akaddemik</th>
                                        <th>Matakuliah</th>
                                        <th>Dosen</th>
                                        <!-- <th>Semester</th>
                                        <th>Ruang</th> -->
                                </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- Modal -->
      <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
              <div class="row clearfix">
                <form method="post" id="modal-form">
                  {{csrf_field()}} {{method_field('post')}}
                  <input type="hidden" name="id" id="id">                  
                  <div class="col-sm-4 mar-bot">
                    <select class="form-control show-tick" name="tahun_akademik_id" id="tahun_akademik_id">
                        <option value="">-- Pilih Tahun Akademik --</option>
                        @foreach ($thnakademik as $thn)
                            <option value="{{ $thn->id }}">{{ $thn->nama }}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="col-sm-4 mar-bot">
	                 <select class="form-control show-tick" name="kelas" id="kelas">
                        <option value="">-- Pilih Kelas --</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
	                </select>
                  </div>
                  <div class="col-sm-4 mar-bot">
                    <select class="form-control show-tick semester" name="semester" id="semester">
                        <option value="">-- Pilih Semester --</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                    </select>
                  </div>
                  <div class="col-sm-12">
                  	<div class="table-responsive" style="display: none;" id="wrap-table-jadwal">
                  		<table class="table table-bordered table-striped table-hover" id="create-jadwal-table">
	                      	<tr>
	                      		<th>Matakuliah</th>
	                      		<th>Dosen</th>
	                      		<th>Ruang</th>
	                      		<th>Hari</th>
	                      		<th>Jam Mulai</th>
	                      		<th>Jam Selesai</th>
	                      	</tr>
	                      	<tbody id="body-create-jadwal">
	                      		
	                      	</tbody>
                      </table>
                  	</div>
                  </div>
                </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <input type="submit" class="btn btn-primary" value="Simpan" id="action">
            </div>

          </div>
        </div>
      </div>  
      {{-- End Modal --}}
    </div>
</section>
@endsection

@section('script-plugins')
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <!-- SweetAlert Plugin Js -->
    <script src="plugins/sweetalert/sweetalert.min.js"></script>
@endsection

@section('script-custom')
    @parent
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <script src="js/pages/ui/notifications.js"></script>
@endsection

@section('script-bottom')
    <script type="text/javascript">  
        $(document).ready(function(){
            var table = $('#jadwal-table').DataTable({
                processing : true,
                serverSide : true,
                ajax       : "{{ route('jadwal.data') }}",
                // order      :  [[ 0, 'desc' ], ],  
                // aLengthMenu: [[ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ]],                              
                columns:[
                    // {data: 'id', name: 'id', visible: false, searchable: false},                                 
                    // {data: 'kode', name: 'kode'},
                    {data:'kelas'},
                    {data:'hari'},
                    {data:'jam_mulai'},
                    {data:'jam_selesai'},
                    {data: 'tahun_akademik'},
                    {data: 'matakuliah'},
                    {data: 'dosen'},
                    // {data: 'semester'},
                    // {data: 'ruang'},
                    // {data:'jam_mulai'},
                    // {data:'jam_selesai'}
                    // {data: 'tahun_akademik', name: 'tahun_akademik'},
                    // {data: 'matakuliah', name: 'matakuliah'},
                    // {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            }) 

            $('#action').on('click',function () {
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('jadwal') }}";
                else url = "{{ url('fakultas') . '/' }}" + id;
                $.ajax({
                    url:url,
                    type:'POST',
                    data:$('#modal-form').serialize(),
                    success:function (data) {
                        // $('#modal-form')[0].reset();                        
                        // table.ajax.reload();
                        // $('#modal').modal('hide');
                        // swal({
                        //     title: 'Berhasil!',
                        //     text: data.message,
                        //     type: 'success',
                        //     timer: '3000'
                        // })
                        console.log(data) 
                        $('#jadwal-table').DataTable().ajax.reload();                       
                    },
                    error:function (data) {
                        dataResponseJson = data.responseJSON;
                        errors = dataResponseJson.errors
                        $.each( errors , function( key, value ) {
                            $.notify({
                                // options
                                message: value
                            },{
                                // settings
                                type: 'danger',
                                z_index: 9999,
                                delay: 3000,
                                placement: {
                                    from: "top",
                                    align: "center"
                                },                                
                            });
                        });                       
                    }

                })
            })

    });

    var url='';
    function addForm() {
        save_method = "add"; 
        $('#semester-view').attr('id','semester')
        $('#wrap-table-jadwal').css('display','none'); 
        $('#modal-form .form-group .form-line').removeClass('focused');          
        $('#modal').modal('show');
        $('#id').val("");
        $('#kode').val("");
        $('#nama').val("");
        $('input[name=_method').val('POST');
        $('#modal-form')[0].reset();            
        $('#modal-title').text('Tambah Jadwal'); 
        $('#action').val('Simpan');                                                
    }     

    function editForm(id) {
        save_method="edit";
        $('#modal-form .form-group .form-line').addClass('focused');
        $('#modal').modal('show');
        $('input[name=_method').val('PUT');
        $('#modal-form')[0].reset();
        $('#modal-title').text('Edit Fakultas'); 
        $('#action').val('Update');               
        $.ajax({
            url:"{{url('fakultas')}}"+"/"+id+"/edit",
            type:"get",
            dataType:"JSON",
            success:function (data) {
                $('#id').val(data.id);
                $('#kode').val(data.kode);
                $('#nama').val(data.nama);
            }
        })      
    }

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
                title: "Apa Anda Yakin Akan Menghapus Data ini?",
                text:"Data yang sudah dihapus tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Saya Yakin!",
                cancelButtonText: "Tidak!",
                closeOnConfirm: true,
                closeOnCancel: true 
            },
            function () {
                $.ajax({
                url : "{{ url('fakultas') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    swal({
                        title: 'Berhasil!',
                        text: data.message,
                        type: 'success',
                        timer: '3000'
                    })                
                    $('#fakultas-table').DataTable().ajax.reload();
                },
                error : function (data) {
                    dataResponseJson = data.responseJSON;
                    errors = dataResponseJson.errors
                    $.each( errors , function( key, value ) {
                        $.notify({
                            // options
                            message: value
                        },{
                            // settings
                            type: 'danger',
                            z_index: 9999,
                            delay: 3000,
                            placement: {
                                from: "top",
                                align: "center"
                            },                                
                        });
                    }); 
                }
              });
            }
        )
    }

    $('.semester').on('change',function(e){
    		$('#wrap-table-jadwal').css('display','block');
    		var body=$('#body-create-jadwal');
    		var tr='';
			var id=e.target.value;
			var dosen='<option>Pilih Dosen</option>';
			var ruang='<option>Pilih Ruang</option>';
            var number=0;
			$.ajax({
				url:"{{url('jadwal')}}"+"/"+id+"/select",
				type:'get',
				dataType:"JSON",
				success:function (data) {
					$.each(data.ruang,function (i,e) {
						ruang+='<option value="'+e.id+'">'+e.nama+'</option>';
					})
					$.each(data.dosen,function (i,e) {
						dosen+='<option value="'+e.id+'">'+e.nama+'</option>';
					})
					$.each(data.matkul,function (i,e) {
                        number++;
						tr+='<tr id="tr-add">'+
                            '<input type="hidden" name="number" value="'+number+'">'+
						 	'<td><input type="hidden" name="matkul_id[]" value="'+e.id+'">'+e.nama+'</td>'+
						 	'<td>'+
						 		'<select name="dosen[]">'+
						 			dosen+
						 		'</select>'+
						 	'</td>'+
						 	'<td>'+
						 		'<select name="ruang[]">'+
						 			ruang+
						 		'</select>'+
						 	'</td>'+
						 	'<td>'+
						 		'<select name="hari[]">'+
						 			'<option value="senin">Senin</option>'+
						 			'<option value="selasa">Selasa</option>'+
						 			'<option value="rabu">Rabu</option>'+
						 			'<option value="kamis">Kamis</option>'+
						 			'<option value="jumat">Jumat</option>'+
						 			'<option value="sabtu">Sabtu</option>'+
						 		'</select>'+
						 	'</td>'+
						 	'<td><input type="text" name="jam_mulai[]"></td>'+
						 	'<td><input type="text" name="jam_selesai[]"></td>'+
						'</tr>'
					})
					body.html(tr)
				}

			})
		})


    $('#modal-options').click(function () {
        $('#modal-button').slideToggle(500)
        $('#modal-delete').slideToggle(1000)
        $('#modal-edit').slideToggle(1500)
        $('#modal-view').slideToggle(2000)
    }) 


    $('#modal-view').click(function(){
        $('#semester').removeClass('semester');
        $('#semester').addClass('semester-view');
        $('#modal').modal('show');

        $('.semester-view').on('change',function () {
            $('#wrap-table-jadwal').css('display','block');
            $('#body-create-jadwal').html('haiiii')
        })
    })




 		 
    </script>    
@endsection  