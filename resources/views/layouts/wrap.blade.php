<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">    
    
    <title>SIAKAD | @yield('title')</title>

    <!-- Favicon-->
    <link href="{{ asset('favicon.ico') }}" rel="icon" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    {{-- CSS Plugins for AdminBSB --}}
    @section('css-plugins')
        <!-- Bootstrap Core Css -->
        <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Waves Effect Css -->
        <link href="plugins/node-waves/waves.css" rel="stylesheet" />
        <!-- Animation Css -->
        <link href="plugins/animate-css/animate.css" rel="stylesheet" />
        <!-- Bootstrap Select Css -->
        <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        <!-- JQuery DataTable Css -->
        <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
        <!-- Bootstrap Material Datetime Picker Css -->
        <link href="plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />        
    @show

    {{-- CSS Custom from AdminBSB --}}
    @section('css-custom')
        <!-- Custom AdminBSB CSS -->
        <link href="css/style.css" rel="stylesheet">
        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="css/themes/all-themes.css" rel="stylesheet" />        
    @show

    {{-- My CSS Custom --}}
    @section('css-bottom')
        <!-- Custom CSS -->
        <link href="css/custom.css" rel="stylesheet">             
    @show
    </head>

<body class="theme-teal">

    @include('layout.loader')

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    @include('layout.header')   

    @include('layout.sidebar')

    @yield('content')
    
    {{-- JS Plugins for AdminBSB --}}
    @section('script-plugins')
        <!-- Jquery Core Js -->
        <script src="plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap Core Js -->
        <script src="plugins/bootstrap/js/bootstrap.js"></script>
        <!-- Select Plugin Js -->
        <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>
        <!-- Slimscroll Plugin Js -->
        <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
        <!-- Bootstrap Notify Plugin Js -->
        <script src="plugins/bootstrap-notify/bootstrap-notify.js"></script>        
        <!-- Waves Effect Plugin Js -->
        <script src="plugins/node-waves/waves.js"></script>   
        <script src="js/pages/ui/tooltips-popovers.js"></script>        
    @show

    {{-- JS Custom from AdminBSB --}}
    @section('script-custom')
        <!-- Custom Js -->
        <script src="js/admin.js"></script>       
    @show        

    {{-- JS Demo from AdminBSB --}}    
    @section('script-demo')
        <!-- Demo Js -->
        <script src="js/demo.js"></script>          
    @show    

    {{-- My JS Custom --}}
    @section('script-bottom')
    @show

    <script>
        $(window).resize(function(){
            if($(window).width() <= 769){
                $('.navbar-brand').text('SIAKAD - TEKNIK INDUSTRI')
            }else {
                $('.navbar-brand').text('SIAKAD UNJANI - PROGRAM STUDI TEKNIK INDUSTRI')
            }
        })
    </script>     
</body>
</html>
