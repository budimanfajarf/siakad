<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrator</div>
                <div class="email">administrator@gmail.com</div>
                {{-- <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div> --}}
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MENU</li>
                <li {{ Route::is('')? 'class=active':'' }}>
                    <a href="/">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                <li {{ Route::is('fakultas.index')? 'class=active':'' }}>
                    <a href="{{ route('fakultas.index') }}">
                        <i class="material-icons">school</i>
                        <span>Fakultas</span>
                    </a>
                </li>
                <li {{ Route::is('program-studi.index')? 'class=active':'' }}>
                    <a href="{{ route('program-studi.index') }}">
                        <i class="material-icons">school</i>
                        <span>Program Studi</span>
                    </a>
                </li>
                <li {{ Route::is('dosen.index')? 'class=active':'' }}>
                    <a href="{{ route('dosen.index') }}">
                        <i class="material-icons">people</i>
                        <span>Dosen</span>
                    </a>
                </li> 
                <li {{ Route::is('mahasiswa.index')? 'class=active':'' }}>
                    <a href="{{ route('mahasiswa.index') }}">
                        <i class="material-icons">people</i>
                        <span>Mahasiswa</span>
                    </a>
                </li>  
                <li {{ Route::is('tahun-akademik.index')? 'class=active':'' }}>
                    <a href="{{ route('tahun-akademik.index') }}">
                        <i class="material-icons">date_range</i>
                        <span>Tahun Akademik</span>
                    </a>
                </li> 
                <li {{ Route::is('ruang.index')? 'class=active':'' }}>
                    <a href="{{ route('ruang.index') }}">
                        <i class="material-icons">date_range</i>
                        <span>Ruang</span>
                    </a>
                </li> 
                <li {{ Route::is('jadwal.index')? 'class=active':'' }}>
                    <a href="{{ route('jadwal.index') }}">
                        <i class="material-icons">date_range</i>
                        <span>Jadwal</span>
                    </a>                    
                </li>                 
                <li {{ Route::is('krs.index')? 'class=active':'' }}>
                    <a href="{{ route('krs.index') }}">
                        <i class="material-icons">date_range</i>
                        <span>KRS</span>
                    </a>
                </li>                                                               
                <!--  -->
            </ul>
        </div>
        <!-- #Menu -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>