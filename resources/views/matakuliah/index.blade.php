@extends('layout.wrap')

@section('title', 'Matakuliah')

@section('css-plugins')
    @parent
    <!-- JQuery DataTable Css -->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />
    <!-- Sweetalert Css -->
    <link href="{{ asset('plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <button type="button" id="modal-options" class="btn bg-teal btn-block btn-lg waves-effect" onclick="addForm()">
            <i class="large material-icons">add</i>
        </button>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            MATAKULIAH
                        </h2> 
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable" id="matakuliah-table" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>SKS</th>
                                        <th>Semester</th>                                        
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>                                      
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>SKS</th>
                                        <th>Semester</th>                                        
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row clearfix">
                            <form method="post" id="modal-form">
                                {{csrf_field()}} {{method_field('post')}}
                                <input type="hidden" name="id" id="id">                  
                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="kode" name="kode">
                                            <label class="form-label">Kode Matakuliah</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="nama" name="nama">
                                            <label class="form-label">Nama Matakuliah</label>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" min="1" max="5" class="form-control" id="sks" name="sks">
                                            <label class="form-label">SKS</label>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" min="1" max="8" class="form-control" id="semester" name="semester">
                                            <label class="form-label">Semester</label>
                                        </div>
                                    </div>
                                </div>                                                                                                 
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-primary" value="Simpan" id="action">
                    </div>

                </div>
            </div>
        </div>  
        {{-- End Modal --}}        
    </div>
</section>
@endsection

@section('script-plugins')
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>      
    <!-- SweetAlert Plugin Js -->
    <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>      
@endsection

@section('script-custom')
    @parent
    <script src="{{ asset('js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('js/pages/ui/notifications.js') }}"></script>    
@endsection

@section('script-bottom')
    @parent
    <script type="text/javascript">  
        $(document).ready(function(){
            var table = $('#matakuliah-table').DataTable({
                processing : true,
                serverSide : true,
                ajax       : "{{ route('matakuliah.data') }}",
                order      :  [[ 0, 'desc' ], ],  
                aLengthMenu: [[ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ]],                              
                columns:[
                    {data: 'id', name: 'id', visible: false, searchable: false},                                 
                    {data: 'kode', name: 'kode'},
                    {data: 'nama', name: 'nama'},
                    {data: 'sks', name: 'sks'},
                    {data: 'semester', name: 'semester'},                    
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            }) 

            $('#action').on('click',function () {
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('matakuliah') }}";
                else url = "{{ url('matakuliah') . '/' }}" + id;
                console.log($('#modal-form').serialize())
                $.ajax({
                    url:url,
                    type:'POST',
                    data:$('#modal-form').serialize(),
                    success:function (data) {
                        $('#modal-form')[0].reset();                        
                        table.ajax.reload();
                        $('#modal').modal('hide');
                        swal({
                            title: 'Berhasil!',
                            text: data.message,
                            type: 'success',
                            timer: '3000'
                        })                        
                    },
                    error:function (data) {
                        dataResponseJson = data.responseJSON;
                        errors = dataResponseJson.errors
                        $.each( errors , function( key, value ) {
                            $.notify({
                                // options
                                message: value
                            },{
                                // settings
                                type: 'danger',
                                z_index: 9999,
                                delay: 3000,
                                placement: {
                                    from: "top",
                                    align: "center"
                                },                                
                            });
                        });                       
                    }

                })
            })            
        });

        var url='';
        function addForm() {
            save_method = "add";  
            $('#modal-form .form-group .form-line').removeClass('focused');          
            $('#modal').modal('show');
            $('#id').val("");
            $('#kode').val("");
            $('#nama').val("");
            $('#sks').val("");
            $('#semester').val("");            
            $('input[name=_method').val('POST');
            $('#modal-form')[0].reset();            
            $('#modal-title').text('Tambah Matakuliah'); 
            $('#action').val('Simpan');                                                
        }     

        function editForm(id) {
            save_method="edit";
            $('#modal-form .form-group .form-line').addClass('focused');
            $('#modal').modal('show');
            $('input[name=_method').val('PUT');
            $('#modal-form')[0].reset();
            $('#modal-title').text('Edit Matakuliah'); 
            $('#action').val('Update');               
            $.ajax({
                url:"{{url('matakuliah')}}"+"/"+id+"/edit",
                type:"get",
                dataType:"JSON",
                success:function (data) {
                    $('#id').val(data.id);
                    $('#kode').val(data.kode);
                    $('#nama').val(data.nama);
                    $('#sks').val(data.sks);
                    $('#semester').val(data.semester);                    
                }
            })      
        }

        function deleteData(id){
            var csrf_token = $('meta[name="csrf-token"]').attr('content');
            swal({
                    title: "Apa Anda Yakin Akan Menghapus Data ini?",
                    text:"Data yang sudah dihapus tidak bisa dikembalikan!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya, Saya Yakin!",
                    cancelButtonText: "Tidak!",
                    closeOnConfirm: true,
                    closeOnCancel: true 
                },
                function () {
                    $.ajax({
                    url : "{{ url('matakuliah') }}" + '/' + id,
                    type : "POST",
                    data : {'_method' : 'DELETE', '_token' : csrf_token},
                    success : function(data) {
                        swal({
                            title: 'Berhasil!',
                            text: data.message,
                            type: 'success',
                            timer: '3000'
                        })                
                        $('#matakuliah-table').DataTable().ajax.reload();
                    },
                    error : function (data) {
                        dataResponseJson = data.responseJSON;
                        errors = dataResponseJson.errors
                        $.each( errors , function( key, value ) {
                            $.notify({
                                // options
                                message: value
                            },{
                                // settings
                                type: 'danger',
                                z_index: 9999,
                                delay: 3000,
                                placement: {
                                    from: "top",
                                    align: "center"
                                },                                
                            });
                        }); 
                    }
                });
                }
            )
        }         
    </script>    
@endsection  