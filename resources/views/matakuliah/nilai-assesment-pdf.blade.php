<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nilai Assesment - {{ $matakuliah->kode }} | {{ $matakuliah->nama }}</title>
    <style>
        * {
            color: #222;
        }
        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 14px;
        }
        td, th {
            border: 1px solid #333;
            padding: 5px;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .bg-grey {
            background-color: #CFCFCF;
        }       
        .bg-sky-blue {
            background-color: #D0E4F5;
        }
        .mr-b-5 {
            margin-bottom: 5px;
        }
        .pad-8 {
            padding: 8px;
        }
        .w-14 {
            width: 14%;
        }
        .header {
            font-size: 16px; 
            line-height: 1.5;
            border-left: none;
        }        
    </style>
</head>
<body>
    <table class="bg-sky-blue">
        <tr>
            <th class="w-14 text-center" style="border-right: none">
                <img src="images/logo.png" alt="Logo" width="85px">
            </th>
            <th class="header">
                NILAI ASSESMENT <br>
                PROGRAM STUDI TEKNIK INDUSTRI <br>
                FAKULTAS TEKNIK - UNIVERSITAS JENDERAL ACHMAD YANI
            </th>            
        </tr>
    </table>
    <table>
        <tr class="bg-grey">
            <th class="pad-8">Kode MK</th>
            <th>Nama MK</th>
            <th>SKS</th>
            <th>Semester</th>
        </tr>
        <tr>
            <td>{{ $matakuliah->kode }}</td>
            <td>{{ $matakuliah->nama }}</td>
            <td>{{ $matakuliah->sks }}</td>
            <td>{{ $matakuliah->semester }}</td>                                    
        </tr>          
    </table>
    <table>
        <tr class="bg-grey">
            <th colspan="{{ 3 + $matakuliah->assesment_->count() }}" class="pad-8 text-center">
                NILAI ASSESMENT 
            </th>
        </tr>          
        <tr>
            <th colspan="2" class="text-center">Mahasiswa</th>
            @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                @if ($learning_outcome->assesment_->count())
                    <th colspan="{{ $learning_outcome->assesment_->count() }}" class="text-center">{{ $learning_outcome->kode }}</th>
                @endif
            @endforeach
            <th rowspan="2" class="text-center">SUM (Nilai * Bobot)</th>
        </tr>
        <tr>
            <th class="text-center">NIM</th>
            <th class="text-center">Nama</th>                                            
            @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                @foreach ($learning_outcome->assesment_ as $assesment)
                    <th class="text-center">{{ $assesment->metode }} ({{ $assesment->bobot }}%)</th>
                @endforeach
            @endforeach
        </tr>

        @foreach ($matakuliah->detail_krs_ as $detail_krs)
            <tr>
                <td> {{ $detail_krs->krs->mahasiswa->nim }} </td>
                <td> {{ $detail_krs->krs->mahasiswa->nama }} </td>

                @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                    @foreach ($learning_outcome->assesment_ as $assesment)

                        @foreach ($detail_krs->nilai_assesment_ as $nilai_assesment)
                            @if ($nilai_assesment->assesment_id == $assesment->id)
                                <td class="text-center">                                                               
                                    {{ $nilai_assesment->nilai }}
                                </td>
                            @endif
                        @endforeach

                    @endforeach
                @endforeach 
                
                <th class="text-center">{{ round($detail_krs->nilai_assesment_->sum('nilai_bobot'), 2) }}</th>
            </tr>
        @endforeach         
    </table>    
    <p class="text-right" style="font-size: 13px">Dicetak Pada: {{ now()->format('d-m-Y H:i') }}</p>
</body>
</html>