@extends('layout.wrap')

@section('title', 'Setup KKNI Matakuliah')

@section('css-plugins')
    @parent
    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />
    <!-- Sweetalert Css -->
    <link href="{{ asset('plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />    
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
    <style>
        #assesment-table input {
            min-width: 50px;
            text-align: center;
        }
    </style>
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">        
                <ol class="breadcrumb breadcrumb-col-teal">
                    {{-- <li><a href=" {{ url('/') }}"><i class="material-icons">home</i> Home</a></li> --}}
                    <li><a href=" {{ url('/matakuliah') }}"><i class="material-icons">book</i> Matakuliah</a></li>
                    <li class="active"><i class="material-icons">settings</i> Setup Learning Outcome & Assesment Matakuliah {{ $matakuliah->nama }}</li>
                </ol>        
            </div>
        </div>

        <!-- Tabs With Icon Title -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                  
                <div class="card">
                    <div class="header" style="border-bottom: none">

                        {{-- Button PDF --}}
                        @if ($matakuliah->assesment_->count())
                        <ul class="header-dropdown">
                            <li>
                                <a href="{{ url('/matakuliah/'.$matakuliah->id.'/learning-outcome/pdf') }}">
                                    <i class="material-icons" style="font-size: 24px">print</i>
                                </a>
                            </li>
                        </ul>
                        @endif
                        {{-- End Button PDF --}}

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"
                                data-trigger="hover" data-container="body" data-toggle="popover" title="Tab Learning Outcome" data-placement="top" 
                                data-content="Setup Learning Outcome dan Metode Assesment Matakuliah {{ $matakuliah->nama }} berdasarkan Study Outcome Program Studi {{ $matakuliah->program_studi->nama }}"                             
                            >
                                <a href="#learning_outcome" data-toggle="tab">
                                    <i class="material-icons">assignment</i> LEARNING OUTCOME
                                </a>
                            </li>
                            <li role="presentation"
                                data-trigger="hover" data-container="body" data-toggle="popover" title="Tab Bobot Assesment" data-placement="top" 
                                data-content="Setup Bobot Assesment(%) berdasarkan Metode Assesment yang telah di tambahkan di setiap Learning Outcome"                             
                            >
                                <a href="#assesment_table" data-toggle="tab">
                                    <i class="material-icons">assessment</i> BOBOT ASSESMENT
                                </a>
                            </li>
                        </ul>
                        <!-- End Nav tabs -->                        
                    </div>
                    <div class="body">                        

                        <!-- Tab panes -->
                        <div class="tab-content">
                            
                            {{-- Tab Panel Learning Outcome --}}
                            <div role="tabpanel" class="tab-pane fade in active" id="learning_outcome">

                                <button type="button" id="modal-options" class="btn bg-teal btn-circle-lg waves-effect waves-circle waves-float" onclick="modal('POST')"
                                    data-trigger="hover" data-container="body" data-toggle="popover"
                                    data-placement="left" data-content="Tambah Learning Outcome"                                
                                >
                                    <i class="large material-icons">add</i>
                                </button>

                                <div class="panel-group" role="tablist" aria-multiselectable="true"> 

                                    @forelse ($study_outcome_ as $study_outcome)
                                        <div class="panel panel-col-teal">
                                            <div class="panel-heading" role="tab" id="headingThreeSO_{{ $study_outcome->id }}">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseThreeSO_{{ $study_outcome->id }}" aria-expanded="true" aria-controls="collapseThreeSO_{{ $study_outcome->id }}">
                                                        {{-- <i class="material-icons">school</i> {{ $study_outcome->kode }} | <small style="color: white">{{ $study_outcome->deskripsi }}</small> --}}
                                                        <div class="media" style="margin-bottom: 0">
                                                            <div class="media-left" style="padding-right: 0;">
                                                                <i class="material-icons">school</i>
                                                            </div>
                                                            <div class="media-body" style="color: white; font-size: 16px">
                                                                {{ $study_outcome->kode }} |  
                                                                <small style="color: white">{{ $study_outcome->deskripsi }}</small>
                                                            </div>
                                                        </div>                                                                
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThreeSO_{{ $study_outcome->id }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThreeSO_{{ $study_outcome->id }}">
                                                <div class="panel-body">
                                                    @foreach ($study_outcome->learning_outcome_ as $learning_outcome)
                                                        @if ($learning_outcome->matakuliah_id == $matakuliah->id)
                                                            <div class="card">
                                                                <div class="header">
                                                                    <h2>
                                                                        Kode LO : {{ $learning_outcome->kode }} <small>Deskripsi LO : {{ $learning_outcome->deskripsi }}</small>
                                                                    </h2>
                                                                    <ul class="header-dropdown m-r--5">
                                                                        <li>
                                                                            <a href="javascript:void(0);" onclick="modal('PUT', {{ $learning_outcome->study_outcome_id }}, {{ $learning_outcome->id }}, '{{ $learning_outcome->kode }}', '{{ $learning_outcome->deskripsi }}' )"
                                                                                data-trigger="hover" data-container="body" data-toggle="popover"
                                                                                data-placement="left" data-content="Edit {{ $learning_outcome->kode }}"                                                                                          
                                                                            >
                                                                                <i class="material-icons">edit</i>
                                                                            </a>
                                                                            <a href="javascript:void(0);" onclick="confirmDelete('form_delete_{{ $learning_outcome->id }}')"
                                                                                data-trigger="hover" data-container="body" data-toggle="popover"
                                                                                data-placement="left" data-content="Delete {{ $learning_outcome->kode }}"                                                                                         
                                                                            >
                                                                                <i class="material-icons" >delete</i>
                                                                            </a>                                                                                    
                                                                        </li>
                                                                    </ul>
                                                                    <form name="form_delete_{{ $learning_outcome->id }}" action="/matakuliah/learning-outcome/{{ $learning_outcome->id }}" method="POST" style="display: none">
                                                                        {{ csrf_field() }} {{ method_field('delete') }}
                                                                        <input type="hidden" name="matakuliah_id" value="{{ $matakuliah->id }}">
                                                                    </form>                                                                             
                                                                </div>
                                                                <div class="body">
                                                                    <h5>Metode Assesment</h5>
                                                                    @foreach ($learning_outcome->assesment_ as $assesment)
                                                                        <div class="btn-group"
                                                                            data-trigger="hover" data-container="body" data-toggle="popover"
                                                                            data-placement="top" data-content="Klik untuk Edit atau Delete {{ $assesment->metode }}"                                                                                 
                                                                        >
                                                                            <button type="button" class="btn btn-lg btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                {{ $assesment->metode }} <span class="caret"></span>
                                                                            </button>
                                                                            <ul class="dropdown-menu">
                                                                                <li><a href="javascript:void(0);" onclick="modal_assesment('PUT', {{ $learning_outcome->id }}, {{ $assesment->id }},  '{{ $assesment->metode }}')">Edit</a></li>
                                                                                <li><a href="javascript:void(0);" onclick="confirmDelete('form_delete_assesment_{{ $assesment->id }}')">Delete</a></li>
                                                                            </ul>
                                                                        </div> 
                                                                        <form name="form_delete_assesment_{{ $assesment->id }}" action="/matakuliah/learning-outcome/assesment/{{ $assesment->id }}" method="POST" style="display: none;">
                                                                            {{ csrf_field() }} {{ method_field('delete') }}
                                                                            <input type="hidden" name="matakuliah_id" value="{{ $matakuliah->id }}"> 
                                                                        </form>                                                                           
                                                                    @endforeach
                                                                    &nbsp;&nbsp;
                                                                    <button type="button" class="btn bg-teal btn-circle waves-effect waves-circle waves-float" onclick="modal_assesment('POST', {{ $learning_outcome->id }})"
                                                                        data-trigger="hover" data-container="body" data-toggle="popover"
                                                                        data-placement="right" data-content="Tambah Metode Assesment di {{ $learning_outcome->kode }}"                                                                                 
                                                                    >
                                                                        <i class="material-icons">add</i>
                                                                    </button>                                                                             
                                                                </div>
                                                            </div>
                                                        @endif                                                        
                                                    @endforeach
                                                    &nbsp;&nbsp;
                                                        <button type="button" class="btn bg-teal btn-circle-lg waves-effect waves-circle waves-float" 
                                                            onclick="modal('POST', {{ $study_outcome->id }})" 
                                                            data-trigger="hover" data-container="body" data-toggle="popover"
                                                            data-placement="right" data-content="Tambah Learning Outcome di {{ $study_outcome->kode }}"
                                                        >
                                                        <i class="large material-icons">add</i>
                                                    </button>                                                            
                                                </div>
                                            </div>
                                        </div>
                                        <br> 
                                    @empty
                                        <div class="alert alert-info">
                                            Klik float button yang ada di sebelah kanan bawah untuk menambahkan Learning Outcome
                                        </div>                                                                               
                                    @endforelse                                   

                                </div>

                            </div>
                            {{-- End Tab Panel Learning Outcome --}}

                            {{-- Tab Panel Assesment Table --}}
                            <div role="tabpanel" class="tab-pane fade" id="assesment_table">
                                @if ($matakuliah->assesment_->count())
                                    <div class="table-responsive">
                                        <table id="assesment-table" class="table table-bordered">
                                            <tr>
                                                <th>Learning Outcome</th>
                                                @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                                                    @if ($learning_outcome->assesment_->count())
                                                        <th colspan="{{ $learning_outcome->assesment_->count() }}" 
                                                            class="text-center"
                                                        >{{ $learning_outcome->kode }}</th>
                                                    @endif
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <th>Assesment Tools</th>                                            
                                                @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                                                    @foreach ($learning_outcome->assesment_ as $assesment)
                                                        <th class="text-center"
                                                        >{{ $assesment->metode }}</th>
                                                    @endforeach
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <th>Bobot (%)</th>                                            
                                                @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                                                    @foreach ($learning_outcome->assesment_ as $assesment)                                                
                                                        <td class="text-center">
                                                            <input type="number" min="0" max="100"
                                                                form="form-assesment-bobot" 
                                                                name="bobot[{{ $assesment->id }}]" 
                                                                value="{{ $assesment->bobot }}"
                                                                class="bobot bobot_{{ $learning_outcome->id }} form-control"
                                                            >
                                                        </td>                                                    
                                                    @endforeach
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <th>Sub Total (%)</th>
                                                @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                                                    @if ($learning_outcome->assesment_->count())                                                   
                                                        <th colspan="{{ $learning_outcome->assesment_->count() }}"
                                                            id="bobot_{{ $learning_outcome->id }}"
                                                            class="text-center"
                                                        >{{ $learning_outcome->assesment_->sum('bobot') }}</th>
                                                    @endif
                                                @endforeach
                                            </tr>

                                            <tr>
                                                <th>Total (%)</th>
                                                <th colspan="{{ $matakuliah->assesment_->count() }}" class="text-center">
                                                    <form action="/matakuliah/learning-outcome/assesment/bobot" id="form-assesment-bobot" method="post">
                                                        {{csrf_field()}} {{method_field('put')}}
                                                        <input type="hidden" name="matakuliah_id" value="{{ $matakuliah->id }}">                                                    
                                                        <div class="form-group" style="margin-bottom: 0">
                                                            <div class="form-line" style="border-bottom: none">
                                                                <input type="text" style="border: none; background: none"
                                                                    id="total_bobot"
                                                                    name="total_bobot"
                                                                    value="{{ $matakuliah->assesment_->sum('bobot') }}"
                                                                    readonly
                                                                >
                                                            </div>
                                                        </div>
                                                    </form>                                                                                               
                                                </th>
                                            </tr>                                            
                                        </table>
                                    </div> 
                                    <div class="text-right">
                                        <button type="submit" name="submit" class="btn btn-lg btn-primary waves-effect" form="form-assesment-bobot">UPDATE</button>
                                    </div>
                                @else
                                    <div class="alert alert-info">
                                        Isi Terlebih dahulu Data Learning Outcome beserta Assesment nya
                                    </div>
                                @endif
                            </div>
                            {{-- End Tab Panel Assesment Table --}}

                        </div>
                        <!-- End Tab panes -->

                    </div>
                </div>            
            </div>
        </div>               

        <!-- Modal -->
        <div class="modal fade" id="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row clearfix">
                            <form method="post" id="modal-form">
                                {{csrf_field()}} {{method_field('post')}}
                                <input type="hidden" name="matakuliah_id" value="{{ $matakuliah->id }}">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">                                    
                                            <select class="form-control show-tick" name="study_outcome_id">
                                                <option value="">-- Pilih Study Outcome --</option>
                                                @foreach ($study_outcome_all_ as $study_outcome)
                                                    <option value="{{ $study_outcome->id }}">{{ $study_outcome->kode }} | {{ str_limit($study_outcome->deskripsi, 50, '...') }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>                                                  
                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="kode">
                                            <label class="form-label">Kode Learning Outcome</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea rows="5" class="form-control no-resize" name="deskripsi"></textarea>
                                            <label class="form-label">Deskripsi Learning Outcome</label>
                                        </div>
                                    </div>
                                </div>                                                                                                
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <input type="submit" name="submit" class="btn btn-primary" value="Simpan" form="modal-form">
                    </div>

                </div>
            </div>
        </div>  
        {{-- End Modal --}}
        
        <!-- Modal Assesment-->
        <div class="modal fade" id="modal-assesment" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row clearfix">
                            <form method="post" id="modal-form-assesment">
                                {{csrf_field()}} {{method_field('post')}}
                                <input type="hidden" name="matakuliah_id" value="{{ $matakuliah->id }}">
                                <input type="hidden" name="learning_outcome_id">
                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="metode">
                                            <label class="form-label">Metode Assesment</label>
                                        </div>
                                    </div>
                                </div>                                                                                               
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <input type="submit" name="submit" class="btn btn-primary" value="Simpan" form="modal-form-assesment">
                    </div>

                </div>
            </div>
        </div>  
        {{-- End Modal --}}         

    </div>
</section>
@endsection

@section('script-plugins')
    @parent
    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>      
    <!-- SweetAlert Plugin Js -->
    <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>  
    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- Jquery Validation Plugin Js -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>              
@endsection

@section('script-custom')
    @parent
    <script src="{{ asset('js/pages/ui/notifications.js') }}"></script>
    <script src="{{ asset('js/pages/ui/tooltips-popovers.js') }}"></script>    
@endsection

@section('script-bottom')
    @parent  
    <script type="text/javascript">
    $(function () {
        $('#modal-form').validate({
            rules: {
                'study_outcome_id': {
                    required: true
                },
                'kode': {
                    required: true
                },
                'deskripsi': {
                    required: true
                }                    
            },
            messages: {
                study_outcome_id: "Study Outcome harus dipilih",
                kode: "Kode harus diisi",
                deskripsi: "Deskripsi harus diisi",
            },            
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });

        $('#modal-form-assesment').validate({
            rules: {
                'metode': {
                    required: true
                }
            },
            messages: {
                metode: "Metode harus diisi",
            },            
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        }); 

        $('#form-assesment-bobot').validate({
            rules: {
                'total_bobot': {
                    onehundred: true
                },                
            },
            messages: {
                total_bobot: "Total Bobot harus 100"
            },            
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });

        //Custom Validations ===============================================================================
        //Must 100
        $.validator.addMethod('onehundred', function (value, element) {
            return value.match(100);
        },
            'Total harus 100'
        );
        //==================================================================================================                        
    });     

    function modal(method, study_outcome_id, id, kode, deskripsi) {
        switch(method) {
            case 'POST':
                $('#modal .modal-title').text('Tambah Learning Outcome');             
                $('#modal input[name=submit]').val('Simpan');                
                $('#modal-form').attr('action', '/matakuliah/learning-outcome');
                $('#modal-form .form-line').removeClass('focused');                
                break;
            case 'PUT':
                $('#modal .modal-title').text('Edit Learning Outcome');         
                $('#modal input[name=submit]').val('Update');                  
                $('#modal-form').attr('action', '/matakuliah/learning-outcome/'+id);
                $('#modal-form .form-line').addClass('focused');
                break;
        }        

        $('#modal-form select[name=study_outcome_id]').selectpicker('deselectAll');           
        $('#modal-form select[name=study_outcome_id] option[selected="selected"]').each(function() {
            $(this).removeAttr('selected');
        });

        if (study_outcome_id) {
            $('#modal-form select[name=study_outcome_id]').selectpicker('val', study_outcome_id);
            $('#modal-form select[name=study_outcome_id] option[value='+study_outcome_id+']').attr('selected', 'selected'); 
        }

        $('#modal-form .form-line').removeClass('error');
        $('#modal-form label.error').remove(); 
        $('#modal-form input[name=_method]').val(method);
        $('#modal-form input[name=kode]').val(kode);
        $('#modal-form textarea[name=deskripsi]').val(deskripsi);       
        $('#modal').modal('show');
    }

    function modal_assesment(method, learning_outcome_id, id, metode) {
        // console.log(method);
        switch(method) {
            case 'POST':
                $('#modal-assesment .modal-title').text('Tambah Assesment');             
                $('#modal-assesment input[name=submit]').val('Simpan');                
                $('#modal-form-assesment').attr('action', '/matakuliah/learning-outcome/assesment');
                $('#modal-form-assesment .form-line').removeClass('focused');                
                break;
            case 'PUT':
                $('#modal-assesment .modal-title').text('Edit Assesment');         
                $('#modal-assesment input[name=submit]').val('Update');                  
                $('#modal-form-assesment').attr('action', '/matakuliah/learning-outcome/assesment/'+id);
                $('#modal-form-assesment .form-line').addClass('focused');
                break;
        }

        $('#modal-form-assesment .form-line').removeClass('error');
        $('#modal-form-assesment label.error').remove(); 
        $('#modal-form-assesment input[name=_method]').val(method);
        $('#modal-form-assesment input[name=learning_outcome_id]').val(learning_outcome_id);
        $('#modal-form-assesment input[name=metode]').val(metode);
        $('#modal-assesment').modal('show');        
    }    

    function confirmDelete(form_name) {
        event.preventDefault(); // prevent form submit
        // var form = event.target.form; // storing the form
        // var form = $(form_id).html();
        var form = document.forms[form_name];
        console.log(form)
        swal({
            title: "Apa Anda Yakin Akan Menghapus Data ini?",
            text:"Data yang sudah dihapus tidak bisa dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonText: "Tidak!",
            closeOnConfirm: true,
            closeOnCancel: true 
        },
        function(){
            form.submit(); // submitting the form when user press yes
        });
    }        
    </script>

    <script> 
        $(document).on("change", ".bobot", function() {
            var sum = 0;
            $(".bobot").each(function(){
                sum += +$(this).val();
            });
            $("#total_bobot").val(sum);
        });                      
    </script>

    @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
        @if ($learning_outcome->assesment_->count())        
            <script>
                $(document).on("change", ".bobot_{{ $learning_outcome->id }}", function() {
                    var sum = 0;
                    $(".bobot_{{ $learning_outcome->id }}").each(function(){
                        sum += +$(this).val();
                    });
                    $("#bobot_{{ $learning_outcome->id }}").text(sum);
                });            
            </script>
        @endif
    @endforeach

    @if (session('message'))
        <script>
            swal({
                title: 'Berhasil!',
                text: '{{ session('message') }}',
                type: 'success',
                timer: '3000'
            })    
        </script>    
    @endif
    @if (session('error'))
        <script>
            swal({
                title: 'Gagal!',
                text: '{{ session('error') }}',
                type: 'error',
                timer: '5000'
            })    
        </script>    
    @endif           
@endsection 