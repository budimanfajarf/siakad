<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $matakuliah->kode }} - {{ $matakuliah->nama }}</title>
    <style>
        * {
            color: #222;
        }
        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 14px;
        }
        td, th {
            border: 1px solid #333;
            padding: 5px;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .bg-grey {
            background-color: #CFCFCF;
        }
        .bg-sky-blue {
            background-color: #D0E4F5;
        }
        .mr-b-5 {
            margin-bottom: 5px;
        }
        .pad-8 {
            padding: 8px;
        }
        .w-14 {
            width: 14%;
        }
        .header {
            font-size: 16px; 
            line-height: 1.5;
        }        
    </style>
</head>
<body>
    <table class="bg-sky-blue">
        <tr>
            <th class="w-14 text-center" style="border-right: none">
                <img src="images/logo.png" alt="Logo" width="85px">
            </th>
            <th class="header" style="border-left: none">
                RENCANA PEMBELAJARAN SEMESTER <br>
                PROGRAM STUDI TEKNIK INDUSTRI <br>
                FAKULTAS TEKNIK - UNIVERSITAS JENDERAL ACHMAD YANI
            </th>            
        </tr>
    </table>
    <table>
        <tr class="bg-grey">
            <th class="w-14 pad-8">Kode MK</th>
            <th>Nama MK</th>
            <th>SKS</th>
            <th>Semester</th>
        </tr>
        <tr>
            <td>{{ $matakuliah->kode }}</td>
            <td>{{ $matakuliah->nama }}</td>
            <td>{{ $matakuliah->sks }}</td>
            <td>{{ $matakuliah->semester }}</td>                                    
        </tr>
        <tr class="bg-grey">
            <th colspan="4" class="pad-8">
                Capaian Pembelajaran Lulusan / Study Outcome dan
                Capaian Pembelajaran Matakuliah / Learning Outcome
            </th>
        </tr>
        @foreach ($study_outcome_ as $study_outcome)
            <tr>
                <td colspan="4">{{ $study_outcome->kode }} : {{ $study_outcome->deskripsi }}</td>
            </tr>                
            @foreach ($study_outcome->learning_outcome_ as $learning_outcome)
                @if ($learning_outcome->matakuliah_id == $matakuliah->id)
                    <tr>
                        <td>{{ $learning_outcome->kode }}</td>
                        <td colspan="3">{{ $learning_outcome->deskripsi }}</td>                        
                    </tr>
                @endif
            @endforeach
        @endforeach     
    </table>
    <table>
        <tr class="bg-grey">
            <th colspan="{{ $matakuliah->assesment_->count() + 1 }}" class="pad-8">Assesment Table</th>
        </tr>        
        <tr>
            <th class="w-14">Learning Outcome</th>
            @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                @if ($learning_outcome->assesment_->count())
                    <th colspan="{{ $learning_outcome->assesment_->count() }}" 
                        class="text-center"
                    >{{ $learning_outcome->kode }}</th>
                @endif
            @endforeach
        </tr>
        <tr>
            <th>Assesment Tools</th>                                            
            @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                @foreach ($learning_outcome->assesment_ as $assesment)
                    <th class="text-center"
                    >{{ $assesment->metode }}</th>
                @endforeach
            @endforeach
        </tr>
        <tr>
            <th>Bobot (%)</th>                                            
            @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                @foreach ($learning_outcome->assesment_ as $assesment)                                                
                    <td class="text-center">
                        {{ $assesment->bobot }}
                    </td>                                                    
                @endforeach
            @endforeach
        </tr>
        <tr>
            <th>Sub Total (%)</th>
            @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                @if ($learning_outcome->assesment_->count())                                                   
                    <td colspan="{{ $learning_outcome->assesment_->count() }}"
                        class="text-center"
                    >{{ $learning_outcome->assesment_->sum('bobot') }}</td>
                @endif
            @endforeach
        </tr>

        <tr>
            <th>Total (%)</th>
            <td colspan="{{ $matakuliah->assesment_->count() }}" class="text-center">
                {{ $matakuliah->assesment_->sum('bobot') }}                                                                                              
            </td>
        </tr>                                            
    </table>
    <p class="text-right" style="font-size: 13px">Dicetak Pada: {{ now()->format('d-m-Y H:i') }}</p>
</body>
</html>