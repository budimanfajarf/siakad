@extends('layout.wrap')

@section('title', 'Nilai Assesment - Matakuliah')

@section('css-plugins')
    @parent
    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />
    <!-- Sweetalert Css -->
    <link href="{{ asset('plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />    
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
    <style>
        #nilai_assesment_table input {
            min-width: 50px;
            text-align: center;
        }
        .input-error {
            color: #F44336;
        }        
    </style>
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">        
                <ol class="breadcrumb breadcrumb-col-teal">
                    {{-- <li><a href=" {{ url('/') }}"><i class="material-icons">home</i> Home</a></li> --}}
                    <li><a href=" {{ url('/matakuliah') }}"><i class="material-icons">book</i> Matakuliah</a></li>
                    <li class="active"><i class="material-icons">assessment</i> Nilai Assesment Mahasiswa Matakuliah {{ $matakuliah->nama }}</li>
                </ol>        
            </div>
        </div>        

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                  
                <div class="card">
                    <div class="header">
                        <h2>
                            Nilai Assesment
                        </h2> 
                        @if ($matakuliah->assesment_->count() && $matakuliah->detail_krs_->count())
                            <ul class="header-dropdown">
                                <li>
                                    <a href="{{ url('/matakuliah/'.$matakuliah->id.'/nilai-assesment/pdf') }}">
                                        <i class="material-icons" style="font-size: 24px">print</i>
                                    </a>
                                </li>
                            </ul>                         
                        @endif                        
                    </div>                    
                    <div class="body">

                        @if (!$matakuliah->assesment_->count())
                            <div class="alert alert-info">
                                Setup terlebih dahulu Data
                                <a href="/matakuliah/{{$matakuliah->id}}/learning-outcome" class="alert-link">Learning Outcome beserta Assesment</a> 
                                Matakuliah {{ $matakuliah->nama }}
                            </div>
                        @endif

                        @if (!$matakuliah->detail_krs_->count())
                            <div class="alert alert-info">
                                Belum ada 
                                <a href="/krs" class="alert-link">KRS Mahasiswa</a> 
                                yang mengambil Matakuliah {{ $matakuliah->nama }} 
                            </div>
                        @endif

                        @if ($matakuliah->assesment_->count() && $matakuliah->detail_krs_->count())
                            <form action="/matakuliah/{{ $matakuliah->id }}/nilai-assesment/nilai" id="form-nilai-assesment" method="post">
                                {{csrf_field()}} {{method_field('put')}}
                                <div class="table-responsive">
                                    <table id="nilai_assesment_table" class="table table-bordered">
                                        <tr>
                                            <th colspan="2" class="text-center">Mahasiswa</th>
                                            @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                                                @if ($learning_outcome->assesment_->count())
                                                    <th colspan="{{ $learning_outcome->assesment_->count() }}" class="text-center">{{ $learning_outcome->kode }}</th>
                                                @endif
                                            @endforeach
                                            <th rowspan="2" style="vertical-align: middle" class="text-center">SUM (Nilai * Bobot)</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center">NIM</th>
                                            <th class="text-center">Nama</th>                                            
                                            @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                                                @foreach ($learning_outcome->assesment_ as $assesment)
                                                    <th class="text-center">{{ $assesment->metode }} ({{ $assesment->bobot }}%)</th>
                                                @endforeach
                                            @endforeach
                                        </tr>

                                        @foreach ($matakuliah->detail_krs_ as $detail_krs)
                                            <tr>
                                                <td> {{ $detail_krs->krs->mahasiswa->nim }} </td>
                                                <td> {{ $detail_krs->krs->mahasiswa->nama }} </td>

                                                @foreach ($matakuliah->learning_outcome_ as $learning_outcome)
                                                    @foreach ($learning_outcome->assesment_ as $assesment)

                                                        @foreach ($detail_krs->nilai_assesment_ as $nilai_assesment)
                                                            @if ($nilai_assesment->assesment_id == $assesment->id)
                                                                <td class="text-center">                                                               
                                                                    <input 
                                                                        name="nilai[{{ $nilai_assesment->id }}]" 
                                                                        value="{{ $nilai_assesment->nilai }}" 
                                                                        class="form-control"
                                                                        type="number"                                                                    
                                                                        min="0" 
                                                                        max="100"
                                                                        data-rule-min="0" 
                                                                        data-rule-max="100" 
                                                                        data-msg-min="Range Nilai Assesment: 0 s.d 100" 
                                                                        data-msg-max="Range Nilai Assesment: 0 s.d 100"
                                                                    >
                                                                    {{-- {{ $nilai_assesment->nilai_bobot }} --}}
                                                                </td>
                                                            @endif
                                                        @endforeach

                                                    @endforeach
                                                @endforeach 
                                                
                                                <th class="text-center" style="vertical-align: middle">{{ round($detail_krs->nilai_assesment_->sum('nilai_bobot'), 2) }}</th>
                                            </tr>
                                        @endforeach         
                                    </table>                            
                                </div>
                                <div class="text-right">
                                    <button type="submit" name="submit" class="btn btn-lg btn-primary waves-effect m-b-15">UPDATE</button>
                                </div>
                                {{-- <div class="row clearfix">
                                    <div class="col-sm-2 right text-right">
                                        <button type="submit" name="submit" class="btn btn-lg btn-primary waves-effect">UPDATE</button>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn bg-teal waves-effect m-b-15" type="button" data-toggle="collapse" data-target="#collapseJadwal" aria-expanded="false"
                                                aria-controls="collapseJadwal">
                                            Lihat Nilai Assesment Mahasiswa, Matakuliah {{ $matakuliah->nama }} di masing-masing Jadwal
                                        </button>                                        
                                    </div>
                                </div>                                 --}}
                            </form>
                            <div class="m-b-15">
                                <a class="font-bold col-teal" data-toggle="collapse" href="#collapseJadwal" aria-expanded="false"
                                aria-controls="collapseJadwal" style="text-decoration: none">
                                Lihat Nilai Assesment Mahasiswa, Matakuliah {{ $matakuliah->nama }} di masing-masing Jadwal?
                                </a> 
                            </div> 
                            <div class="collapse" id="collapseJadwal">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Tahun Akademik</th>
                                            <th colspan="2">Dosen</th>
                                            <th>Kelas</th>
                                            <th></th>
                                        </tr>
                                        @foreach ($matakuliah->jadwal_ as $jadwal)
                                            <tr>
                                                <td>{{ $jadwal->tahun_akademik->nama }}</td>
                                                <td>{{ $jadwal->dosen->nidn }}</td>
                                                <td>{{ $jadwal->dosen->nama }}</td>
                                                <td>{{ $jadwal->kelas }}</td>
                                                <td>
                                                    <a href="{{ url('jadwal/'.$jadwal->id.'/nilai-assesment') }}" class="btn btn-primary waves-effect"><i class="material-icons">visibility</i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>                                                        
                        @endif

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@section('script-plugins')
    @parent
    <!-- SweetAlert Plugin Js -->
    <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
    <!-- Jquery Validation Plugin Js -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>
    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>              
@endsection

@section('script-custom')
    @parent
@endsection

@section('script-bottom')
    @parent
    <script>                   
        $('#form-nilai-assesment').validate({           
            highlight: function (input) {
                $(input).addClass('input-error');
            },
            unhighlight: function (input) {
                $(input).removeClass('input-error');                               
            },
            errorPlacement: function (error, element) {
                // console.log(error.text());
                $.notify({
                    // options
                    message: error.text()
                },{
                    // settings
                    type: 'danger',
                    z_index: 9999,
                    delay: 3000,
                    placement: {
                        from: "top",
                        align: "center"
                    },                                
                });               
            }
        });         
    </script>
    @if (session('message'))
        <script>
            swal({
                title: 'Berhasil!',
                text: '{{ session('message') }}',
                type: 'success',
                timer: '3000'
            })    
        </script>    
    @endif    
    @if (session('error'))
        <script>
            swal({
                title: 'Gagal!',
                text: '{{ session('error') }}',
                type: 'error',
                timer: '5000'
            })    
        </script>    
    @endif       
@endsection  