<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <form action="/jadwal/{jadwal_id}/nilai-assesment" id="form-nilai-assesment">
    </form>
    <table>
        <tr>
            <th rowspan="2">Mahasiswa</th>
        @foreach ($jadwal->matakuliah->learning_outcome_ as $learning_outcome)
            @if ($learning_outcome->assesment_->count())
                <th colspan="{{ $learning_outcome->assesment_->count() }}">{{ $learning_outcome->id }} | {{ $learning_outcome->kode }}</th>
            @endif
        @endforeach
        </tr>
        <tr>
        @foreach ($jadwal->matakuliah->learning_outcome_ as $learning_outcome)
            @foreach ($learning_outcome->assesment_ as $assesment)
                <th>{{ $assesment->id }} |{{ $assesment->metode }}</th>
            @endforeach
        @endforeach
        </tr>

        @foreach ($mahasiswa_ as $mahasiswa)
            <tr>
                <td> {{ $mahasiswa->nama }} </td>

                @foreach ($jadwal->matakuliah->learning_outcome_ as $learning_outcome)
                    @foreach ($learning_outcome->assesment_ as $assesment)

                        @foreach ($mahasiswa->nilai_assesment_ as $nilai_assesment)
                            @if ($nilai_assesment->assesment_id == $assesment->id)
                                <td>lo id: {{ $assesment->learning_outcome_id }} | ass id: {{ $assesment->id }} | id: {{ $nilai_assesment->id }} | nilai: {{ $nilai_assesment->nilai }} </td>                                
                            @endif
                        @endforeach

                    @endforeach
                @endforeach                    

            </tr>
        @endforeach         
    </table>
    <input type="submit" name="submit" class="btn btn-primary" value="Simpan" form="form-nilai-assesment">
</body>
</html>