@extends('layout.wrap')

@section('title', 'Study Outcome Mahasiswa')

@section('css-plugins')
    @parent
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
    <style>
        .progress {
            height: 30px;
        }
        .progress .progress-bar {
            line-height: 30px;
            font-size: 14px;
        }
        .media .media-body {
            font-size: 15px;
        }
    </style>
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">        
                <ol class="breadcrumb breadcrumb-col-teal">
                    <li><a href=" {{ url('/mahasiswa') }}"><i class="material-icons">person</i> Mahasiswa</a></li>
                    <li class="active"><i class="material-icons">school</i> Study Outcome</li>
                </ol>        
            </div>
        </div>        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                  
                <div class="card">
                    <div class="header">
                        <h2>
                            Study Outcome / Capaian Pembelajan Lulusan (CPL)
                        </h2>
                        {{-- @if ($jadwal->matakuliah->assesment_->count() && $jadwal->detail_krs_->count()) --}}
                            <ul class="header-dropdown">
                                <li>
                                    <a href="{{ url('/mahasiswa/'.$mahasiswa_n['id'].'/study-outcome/pdf') }}">
                                        <i class="material-icons" style="font-size: 24px">print</i>
                                    </a>
                                </li>
                            </ul>                         
                        {{-- @endif                         --}}
                    </div>                    
                    <div class="body">
                        {{-- <div class="media">
                            <div class="media-left">
                                <p style="width: 90px">
                                    <a href="javascript:void(0);" type="button" class="btn bg-teal btn-circle-lg waves-effect waves-circle waves-float"
                                    style="width: 80px; height: 80px;">
                                        <i class="media-object material-icons" style="font-size: 46px !important">person</i>
                                    </a>
                                </p>
                            </div> --}}
                            {{-- <div class="media-body"> --}}
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <p class="font-bold" style="width: 250px">Nomor Induk Mahasiswa (NIM)</p>
                                        <p class="font-bold">Nama Mahasiswa</p>
                                        <p class="font-bold">Total Matakuliah yg sudah diambil</p>
                                        <p class="font-bold">Total SKS Matakuliah yg sudah diambil</p>
                                    </div>
                                    <div class="media-body">
                                        <p> : {{ $mahasiswa_n['nim'] }}</p>
                                        <p> : {{ $mahasiswa_n['nama'] }}</p>
                                        <p> : {{ $mahasiswa_n['matakuliah_merge_']->count() }}</p>
                                        <p> : {{ $mahasiswa_n['matakuliah_merge_']->sum('sks') }}</p>
                                    </div>
                                </div> 
                            {{-- </div> --}}
                        {{-- </div>                                                 --}}
                        @foreach ($mahasiswa_n['study_outcome_']->sortByDesc('nilai') as $study_outcome)  
                        <ul class="list-group">                     
                            <li class="list-group-item">
                                <span class="badge bg-teal">
                                    {{ $study_outcome['matakuliah_']->isNotEmpty() ? $study_outcome['matakuliah_']->count().' MK' : '' }}
                                </span>
                                <p style="font-size: 15px">
                                    <strong>
                                        {{ $study_outcome['kode'] }} | 
                                    </strong>
                                    {{ $study_outcome['deskripsi'] }}
                                </p>
                                @isset($study_outcome['nilai'])
                                    <div class="progress">
                                        <div 
                                            class="progress-bar
                                                @if ($study_outcome['nilai'] > 85)
                                                    progress-bar-info    
                                                @elseif($study_outcome['nilai'] > 75)
                                                    progress-bar-success
                                                @elseif($study_outcome['nilai'] > 60)
                                                    progress-bar-warning
                                                @elseif($study_outcome['nilai'] > 50)
                                                    bg-pink
                                                @else
                                                    progress-bar-danger
                                                @endif 
                                            "                                            role="progressbar" aria-valuenow="{{ round($study_outcome['nilai'], 2) }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ round($study_outcome['nilai'], 2) }}%;">
                                            Nilai {{ $study_outcome['kode'] }}: <strong>{{ round($study_outcome['nilai'], 2) }}</strong>
                                        </div>
                                    </div>                                                            
                                @endisset                        
                                @if ($study_outcome['matakuliah_']->isNotEmpty())
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered">
                                        <tr>
                                            <th rowspan="{{ $study_outcome['matakuliah_']->count() + 1 }}"
                                            >Matakuliah yg berkontribusi untuk {{ $study_outcome['kode'] }}
                                            </th>
                                            <th>Kode MK</th>
                                            <th>Nama MK</th>
                                            <th>SKS</th>
                                            <th>Nilai MK untuk {{ $study_outcome['kode'] }}</th>
                                        </tr>
                                    @foreach ($study_outcome['matakuliah_'] as $matakuliah)
                                        <tr>
                                            <td>{{ $matakuliah['kode'] }}</td>
                                            <td>{{ $matakuliah['nama'] }}</td>
                                            <td>{{ $matakuliah['sks'] }}</td>
                                            <td>{{ round($matakuliah['nilai'], 2) }}</td>
                                        </tr>
                                    @endforeach
                                    </table>
                                </div>                    
                                @else
                                    <p class="font-bold col-pink">
                                        Belum ada matakuliah yang diambil oleh {{ $mahasiswa_n['nama'] }} yang berkontribusi untuk {{ $study_outcome['kode'] }}
                                    </p>
                                @endif
                            </li>
                        </ul> 
                        @endforeach                        
                                               
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script-plugins')
    @parent
@endsection

@section('script-custom')
    @parent  
@endsection

@section('script-bottom')
    @parent  
@endsection  