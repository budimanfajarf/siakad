<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Study Outcome - {{ $mahasiswa_n['nim'] }} | {{ $mahasiswa_n['nama'] }}</title>
    <style>
        * {
            color: #222;
        }
        table {
            border-collapse: collapse;
            width: 100%;
            font-size: 14px;
        }
        td, th {
            border: 1px solid #333;
            padding: 5px;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .bg-grey {
            background-color: #CFCFCF;
        }       
        .bg-sky-blue {
            background-color: #D0E4F5;
        }
        .mr-b-5 {
            margin-bottom: 5px;
        }
        .pad-8 {
            padding: 8px;
        }
        .w-14 {
            width: 14%;
        }
        .header {
            font-size: 16px; 
            line-height: 1.5;
            border-left: none;
        }        
    </style>
</head>
<body>
    <table class="bg-sky-blue">
        <tr>
            <th class="w-14 text-center" style="border-right: none">
                <img src="images/logo.png" alt="Logo" width="85px">
            </th>
            <th class="header">
                CAPAIAN PEMBELAJARAN LULUSAN (CPL) / STUDY OUTCOME <br>
                PROGRAM STUDI TEKNIK INDUSTRI <br>
                FAKULTAS TEKNIK - UNIVERSITAS JENDERAL ACHMAD YANI
            </th>            
        </tr>
    </table>
    <table class="table table-bordered">
            <tr class="bg-grey">
                <th>Nomor Induk Mahasiswa</th>
                <th>Nama Mahasiswa</th>
                <th>Total Matakuliah yg sudah diambil</th>
                <th>Total SKS Matakuliah yg sudah diambil</th>
            </tr>
            <tr>
                <td>{{ $mahasiswa_n['nim'] }}</td>
                <td>{{ $mahasiswa_n['nama'] }}</td>
                <td>{{ $mahasiswa_n['matakuliah_merge_']->count() }}</td>
                <td>{{ $mahasiswa_n['matakuliah_merge_']->sum('sks') }}</td>
            </tr>
        </table>
        <table class="table table-bordered">
            <tr class="bg-grey">
                <th colspan="5">Capaian Pembelajaran Lulusan (CPL) / Study Outcome</th>
                <th>Nilai SO</th>
            </tr>
            @foreach ($mahasiswa_n['study_outcome_']->sortByDesc('nilai') as $study_outcome)
                <tr>
                    <th colspan="5">
                        {{ $study_outcome['kode'] }} : 
                        {{ $study_outcome['deskripsi'] }}
                    </th>
                    <th rowspan="{{ $study_outcome['matakuliah_']->isNotEmpty() ? $study_outcome['matakuliah_']->count() + 2 : 2 }}"
                        class="text-center"
                        >
                        @isset($study_outcome['nilai'])
                            {{ round($study_outcome['nilai'], 2) }}                        
                        @endisset                        
                    </th>
                </tr>
                @if ($study_outcome['matakuliah_']->isNotEmpty())
                    <tr>
                        <td rowspan="{{ $study_outcome['matakuliah_']->count() + 1 }}"
                            >Matakuliah yg berkontribusi untuk {{ $study_outcome['kode'] }}
                        </td>
                        <th>Kode MK</th>
                        <th>Nama MK</th>
                        <th>SKS</th>
                        <th>Nilai MK untuk {{ $study_outcome['kode'] }}</th>
                    </tr>
                    @foreach ($study_outcome['matakuliah_'] as $matakuliah)
                        <tr>
                            <td>{{ $matakuliah['kode'] }}</td>
                            <td>{{ $matakuliah['nama'] }}</td>
                            <td>{{ $matakuliah['sks'] }}</td>
                            <td>{{ round($matakuliah['nilai'], 2) }}</td>
                        </tr>
                    @endforeach                    
                @else
                    <tr>
                        <td colspan="5">
                            Belum ada matakuliah yang diambil oleh {{ $mahasiswa_n['nama'] }} yang berkontribusi untuk {{ $study_outcome['kode'] }}
                        </td>
                    </tr>
                @endif
            @endforeach
        </table>   
    <p class="text-right" style="font-size: 13px">Dicetak Pada: {{ now()->format('d-m-Y H:i') }}</p>
</body>
</html>