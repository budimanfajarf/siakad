@extends('layouts.wrap')

@section('title', 'Tahun Akademik')

@section('css-plugins')
    @parent
    <!-- Sweetalert Css -->
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <button type="button" id="modal-options" class="btn bg-teal btn-block btn-lg waves-effect" onclick="addForm()">
            <i class="material-icons">add</i>
        </button>        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            TAHUN AKADEMIK
                        </h2> 
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable" id="tahun-akademik-table" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Tahun Akademik</th>
                                        <th>Tanggal Awal</th>
                                        <th>Tanggal Akhir</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Tahun Akademik</th>
                                        <th>Tanggal Awal</th>
                                        <th>Tanggal Akhir</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- Modal -->
      <div class="modal fade" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
              <form method="post" id="modal-form">
                {{csrf_field()}} {{method_field('post')}}
                <input type="hidden" name="id" id="id">                                                     
                <div class="row clearfix">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="datepicker form-control" name="tanggal_awal" id="tanggal_awal" placeholder="Tanggal Awal" data-dtp="dtp_s5UMn">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="datepicker form-control" name="tanggal_akhir" id="tanggal_akhir" placeholder="Tanggal Akhir">
                            </div>
                        </div>
                    </div>                    
                </div>                
              </form>                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <input type="submit" class="btn btn-primary" value="Simpan" id="action">
            </div>

          </div>
        </div>
      </div>  
      {{-- End Modal --}}
    </div>
</section>
@endsection

@section('script-plugins')
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <script src="plugins/sweetalert/sweetalert.min.js"></script>
    <script src="plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>    
@endsection

@section('script-custom')
    @parent
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <script src="js/pages/ui/notifications.js"></script>
    <script src="js/pages/forms/basic-form-elements.js"></script>   
@endsection

@section('script-bottom')
    <script type="text/javascript">  
        $('.datepicker').bootstrapMaterialDatePicker({ weekStart : 0, time: false });            
        $(document).ready(function(){
            var table = $('#tahun-akademik-table').DataTable({
                processing : true,
                serverSide : true,
                ajax       : "{{ route('tahun-akademik.data') }}",
                order      :  [[ 0, 'desc' ], ],  
                aLengthMenu: [[ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ]],                              
                columns:[
                    {data: 'id', name: 'id', visible: false, searchable: false},                                 
                    {data: 'nama', name: 'nama'},
                    {data: 'tanggal_awal', name: 'tanggal_awal'},
                    {data: 'tanggal_akhir', name: 'tanggal_akhir'},                    
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            }) 

            $('#action').on('click',function () {
                console.log($('#modal-form').serialize())
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('tahun-akademik') }}";
                else url = "{{ url('tahun-akademik') . '/' }}" + id;
                $.ajax({
                    url:url,
                    type:'POST',
                    data:$('#modal-form').serialize(),
                    success:function (data) {
                        console.log(data)
                        $('#modal-form')[0].reset();                        
                        table.ajax.reload();
                        $('#modal').modal('hide');
                        swal({
                            title: 'Berhasil!',
                            text: data.message,
                            type: 'success',
                            timer: '3000'
                        })                        
                    },
                    error:function (data) {
                        dataResponseJson = data.responseJSON;
                        errors = dataResponseJson.errors
                        $.each( errors , function( key, value ) {
                            $.notify({
                                // options
                                message: value
                            },{
                                // settings
                                type: 'danger',
                                z_index: 9999,
                                delay: 3000,
                                placement: {
                                    from: "top",
                                    align: "center"
                                },                                
                            });
                        });                       
                    }

                })
            })

    });

    var url='';
    function addForm() {
        save_method = "add";  
        $('#modal-form .form-group .form-line').removeClass('focused');          
        $('#modal').modal('show');
        $('#tanggal_awal').val("");
        $('#tanggal_akhir').val("");                       
        $('input[name=_method').val('POST');
        $('#modal-form')[0].reset();            
        $('#modal-title').text('Tambah Tahun Akademik'); 
        $('#action').val('Simpan');                                                
    }     

    function editForm(id) {
        save_method="edit";
        $('#modal-form .form-group .form-line').addClass('focused');
        $('#modal').modal('show');
        $('input[name=_method').val('PUT');
        $('#modal-form')[0].reset();
        $('#modal-title').text('Edit Tahun Akademik'); 
        $('#action').val('Update');               
        $.ajax({
            url:"{{url('tahun-akademik')}}"+"/"+id+"/edit",
            type:"get",
            dataType:"JSON",
            success:function (data) {
                $('#id').val(data.id);
                $('#tanggal_awal').val(data.tanggal_awal);
                $('#tanggal_akhir').val(data.tanggal_akhir);               
            },
            error:function (errors){
                console.log(errors)
            }
        })      
    }

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
                title: "Apa Anda Yakin Akan Menghapus Data ini?",
                text:"Data yang sudah dihapus tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Saya Yakin!",
                cancelButtonText: "Tidak!",
                closeOnConfirm: true,
                closeOnCancel: true 
            },
            function () {
                $.ajax({
                url : "{{ url('tahun-akademik') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    swal({
                        title: 'Berhasil!',
                        text: data.message,
                        type: 'success',
                        timer: '3000'
                    })                
                    $('#tahun-akademik-table').DataTable().ajax.reload();
                },
                error : function (data) {
                    dataResponseJson = data.responseJSON;
                    errors = dataResponseJson.errors
                    $.each( errors , function( key, value ) {
                        $.notify({
                            // options
                            message: value
                        },{
                            // settings
                            type: 'danger',
                            z_index: 9999,
                            delay: 3000,
                            placement: {
                                from: "top",
                                align: "center"
                            },                                
                        });
                    }); 
                }
              });
            }
        )
    }    
    </script>    
@endsection  