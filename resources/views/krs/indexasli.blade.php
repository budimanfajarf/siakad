@extends('layouts.wrap')

@section('title', 'fakultas')

@section('css-plugins')
    @parent
    <!-- Sweetalert Css -->
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header"><h2>KRS</h2></div>
        <button type="button" id="modal-options" class="btn bg-teal btn-block btn-lg waves-effect" onclick="addForm()">
            <i class="material-icons">add</i>
        </button>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            DAFTAR KRS UNJANI
                        </h2> 
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable" id="fakultas-table" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Kode Fakultas</th>
                                        <th>Nama Fakultas</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>                                      
                                        <th>Kode Fakultas</th>
                                        <th>Nama Fakultas</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- Modal KRS-->
      <div class="modal fade" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
              <div class="row clearfix">
                <form method="post" id="modal-form">
                  {{csrf_field()}} {{method_field('post')}}
                  <input type="hidden" name="id" id="id">                  
                  <div class="col-sm-4">
                    <select class="form-control show-tick" name="tahun_akademik" id="tahun_akademik" class="mar-bot">
                        <option value="">-- Pilih Tahun Akademik --</option>
                        @foreach($tahun_akademik as $thnakdmk)
                            <option value="{{$thnakdmk->id}}">{{$thnakdmk->nama}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <select class="form-control show-tick" name="dosen" id="dosen" class="mar-bot">
                        <option value="">-- Pilih Dosen Wali --</option>
                        @foreach($dosen as $dosens)
                            <option value="{{$dosens->id}}">{{$dosens->nama}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <select class="form-control show-tick" name="mahasiswa" id="mahasiswa" class="mar-bot">
                        <option value="">-- Pilih NIM --</option>
                        @foreach($mahasiswa as $mahasiswas)
                            <option value="{{$mahasiswas->id}}">{{$mahasiswas->nama}}</option>
                        @endforeach
                    </select>
                  </div>
                </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <input type="submit" class="btn btn-primary" value="Simpan" id="action">
            </div>

          </div>
        </div>
      </div>  
      <!-- End Modal -->

      <!-- Modal Detail Krs -->
        <div class="modal fade" id="modal-detail-krs" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="modal-title-detail-krs">Modal title</h4>
            </div>
            <div class="modal-body">
              <div class="row clearfix">
                <form method="post" id="modal-form-detail-krs">
                  {{csrf_field()}} {{method_field('post')}}                 
                  <div class="body table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Matakuliah</th>
                                    <th>SKS</th>
                                    <th>Semester</th>
                                </tr>
                            </thead>
                            <tbody id="body-input-krs">
                                
                            </tbody>
                        </table>
                    </div>
                </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <input type="submit" class="btn btn-primary" value="Simpan" id="action-detail-krs">
            </div>

          </div>
        </div>
      </div>
      <!-- End Modal Detail KRS -->


    </div>
</section>
@endsection

@section('script-plugins')
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <!-- SweetAlert Plugin Js -->
    <script src="plugins/sweetalert/sweetalert.min.js"></script>
@endsection

@section('script-custom')
    @parent
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <script src="js/pages/ui/notifications.js"></script>
@endsection

@section('script-bottom')
    <script type="text/javascript">  
        $(document).ready(function(){
            // var table = $('#fakultas-table').DataTable({
            //     processing : true,
            //     serverSide : true,
            //     ajax       : "{{ route('fakultas.data') }}",
            //     order      :  [[ 0, 'desc' ], ],  
            //     aLengthMenu: [[ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ]],                              
            //     columns:[
            //         {data: 'id', name: 'id', visible: false, searchable: false},                                 
            //         {data: 'kode', name: 'kode'},
            //         {data: 'nama', name: 'nama'},
            //         {data: 'action', name: 'action', orderable: false, searchable: false}
            //     ],
            // })

            getJadwal();     

            $('#action').on('click',function () {
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('krs') }}";
                else url = "{{ url('fakultas') . '/' }}" + id;
                $.ajax({
                    url:url,
                    type:'POST',
                    data:$('#modal-form').serialize(),
                    success:function (data) {
                        // console.log(data)
                        var krs_id=data.data['id'];
                        $('#modal-form-detail-krs').prepend('<input class="krs_id" type="hidden" name="krs_id" value="'+krs_id+'">')
                        $('#modal-form')[0].reset();                        
                        // table.ajax.reload();
                        $('#modal').modal('hide');
                        // swal({
                        //     title: 'Berhasil!',
                        //     text: data.message,
                        //     type: 'success',
                        //     timer: '3000'
                        // })
                        swal({
                            title: data.message,
                            // text:"Apakah Anda Akan Menigisi KRS?",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Klik Untuk mengisi KRS",
                            // cancelButtonText: "Tidak!",
                            // closeOnConfirm: false,
                            // closeOnCancel: false 

                            },
                            function () {

                                $('#modal-detail-krs').modal('show');
                                $("#action-detail-krs").on('click',function () {
                                    // console.log('krs id adalah :'+krs_id)

                                    $.ajax({
                                        url:"{{url('detail-krs')}}",
                                        type:"post",
                                        data:$('#modal-form-detail-krs').serialize(),
                                        success:function(data){
                                            $('#modal-form-detail-krs')[0].reset()
                                            $('.check_matkul').attr('checked',false)
                                            // krs_id='';
                                            console.log(data)
                                            $('#modal-detail-krs').modal('hide')
                                            $('.krs_id').remove()

                                        }
                                    })
                                })
                                
                            }

                        )
                        

                    },
                    error:function (data) {
                        dataResponseJson = data.responseJSON;
                        errors = dataResponseJson.errors
                        $.each( errors , function( key, value ) {
                            $.notify({
                                // options
                                message: value
                            },{
                                // settings
                                type: 'danger',
                                z_index: 9999,
                                delay: 3000,
                                placement: {
                                    from: "top",
                                    align: "center"
                                },                                
                            });
                        });                       
                    }

                })
            })

    });

    var url='';
    function addForm() {
        save_method = "add"; 
        // $('.check_matkul').attr('checked',false)
        $('#tahun_akademik').selectpicker('deselectAll')
        $('#dosen').selectpicker('deselectAll')
        $('#mahasiswa').selectpicker('deselectAll')
        $('#modal-form .form-group .form-line').removeClass('focused');          
        $('#modal').modal('show');
        $('#id').val("");
        $('#kode').val("");
        $('#nama').val("");
        $('input[name=_method').val('POST');
        $('#modal-form')[0].reset();            
        $('#modal-title').text('Tambah Mahasiswa Untuk Mengambil Krs'); 
        $('#action').val('Simpan');                                                
    }     

    function editForm(id) {
        save_method="edit";
        $('#modal-form .form-group .form-line').addClass('focused');
        $('#modal').modal('show');
        $('input[name=_method').val('PUT');
        $('#modal-form')[0].reset();
        $('#modal-title').text('Edit Fakultas'); 
        $('#action').val('Update');               
        $.ajax({
            url:"{{url('fakultas')}}"+"/"+id+"/edit",
            type:"get",
            dataType:"JSON",
            success:function (data) {
                $('#id').val(data.id);
                $('#kode').val(data.kode);
                $('#nama').val(data.nama);
            }
        })      
    }

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
                title: "Apa Anda Yakin Akan Menghapus Data ini?",
                text:"Data yang sudah dihapus tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Saya Yakin!",
                cancelButtonText: "Tidak!",
                closeOnConfirm: true,
                closeOnCancel: true 
            },
            function () {
                $.ajax({
                url : "{{ url('fakultas') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    swal({
                        title: 'Berhasil!',
                        text: data.message,
                        type: 'success',
                        timer: '3000'
                    })                
                    $('#fakultas-table').DataTable().ajax.reload();
                },
                error : function (data) {
                    dataResponseJson = data.responseJSON;
                    errors = dataResponseJson.errors
                    $.each( errors , function( key, value ) {
                        $.notify({
                            // options
                            message: value
                        },{
                            // settings
                            type: 'danger',
                            z_index: 9999,
                            delay: 3000,
                            placement: {
                                from: "top",
                                align: "center"
                            },                                
                        });
                    }); 
                }
              });
            }
        )
    }  


    function getJadwal() {
        var tr='';
       $.ajax({
                url:"{{route('detail-krs.jadwal')}}",
                type:'get',
                dataType:'json',
                success:function (data) {

                    // console.log(data)
                    for(let i=0; i<data.length; i++){
                        tr+='<tr>';
                            tr+='<td><input class="check_matkul" type="checkbox" name="check_matkul[]" value="'+data[i]['id_jadwal']+'" style="left:auto; opacity:1; position: static;"></td>';
                            tr+='<td>'+data[i]['matkul_nama']+'</td>';
                            tr+='<td>'+data[i]['matkul_sks']+'</td>';
                            tr+='<td>'+data[i]['matkul_semester']+'</td>';
                        tr+='</tr>';
                    }
                    // console.log(tr);
                    $('#body-input-krs').html(tr);
                }
            })
    }  
    </script>    
@endsection  