<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<title></title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="media-list">
				  <li class="media">
				    <div class="media-left">
				        <img class="media-object" src="/images/unjanilogo1.jpg" >
				    </div>
				    <div class="media-body" style="vertical-align: middle;">
				      <h4 class="media-heading">UNIVERSITAS JENDRAL ACHAMAD YANI</h4>
				      <h4 class="media-heading">Program Studi Teknik Industri</h4>
				      <h4 class="media-heading">Jl. Ters. Jend. Gatot Subroto. Bandung</h4>
				    </div>
				  </li>
				</ul>
				<hr>
				<h3 class="text-center">Kartu Rencana Studi (KRS)</h3>
				<div class="row">
					<div class="col-sm-6">
						<div class="row">
							<div class="col-sm-3">
								<p>Nama</p>
								<p>Nim</p>
							</div>
							<div class="col-sm-9">
								<p>: {{$data[0]['mahasiswa']}}</p>
								<p>: {{$data[0]['mahasiswa_nim']}}</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="row">
							<div class="col-sm-3">
								<p>Semester</p>
								<p>Tahun Akademik</p>
							</div>
							<div class="col-sm-9">
								<p>: {{$data[0]['matkul_semester']}}</p>
								<p>: {{$data[0]['thn_akademik']}}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>No</th>
							<th>Kode Matakuliah</th>
							<th>Matakuliah</th>
							<th>Semester</th>
							<th>Sks</th>
							<th>Dosen</th>
						</tr>
						<?php $no=1; $total=0;?>
						@foreach($data as $key => $val)
						<tr>
							<td>{{$no++}}</td>
							<td>{{$data[$key]['matkul_kode']}}</td>
							<td>{{$data[$key]['matkul_nama']}}</td>
							<td>{{$data[$key]['matkul_semester']}}</td>
							<td class="sks" data="{{$data[$key]['matkul_sks']}}">
								{{$data[$key]['matkul_sks']}}
								<?php
								$total+=$data[$key]['matkul_sks'];
								?>
							</td>
							<td>{{$data[$key]['matkul_dosen']}}</td>
						</tr>
						@endforeach
						<tr>
							<th colspan="6" style="text-align:center">
								<?="Total SKS : ".$total?>
							</th>
						</tr>
					</table>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p>Mengetahui Mahasiswa</p><br><br><br><br>
						<p>{{$data[0]['mahasiswa']}}</p>
						<p>NIM. <span>{{$data[0]['mahasiswa_nim']}}</span></p>
					</div>
					<div class="col-sm-6 text-right">
						<p>Mengetahui dan Menyetujui Dosen Wali</p><br><br><br><br>
						<p>{{$data[0]['dosen']}}</p>
						<p>NIDN. <span>{{$data[0]['dosen_nidn']}}</span></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>