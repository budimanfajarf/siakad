@extends('layouts.wrap')

@section('title', 'KRS')

@section('css-plugins')
    @parent
    <!-- Sweetalert Css -->
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('css-custom')
    @parent
@endsection

@section('css-bottom')
    @parent
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <button type="button" id="modal-options" class="btn bg-teal btn-block btn-lg waves-effect" onclick="addForm()">
            <i class="material-icons">add</i>
        </button>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            KARTU RENCANA STUDI
                        </h2> 
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable" id="krs-table" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Tahun Akademik</th>
                                        <th></th>
                                        <!-- <th>Kode Fakultas</th>
                                        <th>Nama Fakultas</th>
                                        <th></th> -->
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Tahun Akademik</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- Modal KRS-->
      <div class="modal fade" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
              <div class="row clearfix">
                <form method="post" id="modal-form">
                  {{csrf_field()}} {{method_field('post')}}
                  <input type="hidden" name="id" id="id">                  
                  <div class="col-sm-4">
                    <select class="form-control show-tick" name="tahun_akademik" id="tahun_akademik" class="mar-bot">
                        <option value="">-- Pilih Tahun Akademik --</option>
                        @foreach($tahun_akademik as $thnakdmk)
                            <option value="{{$thnakdmk->id}}">{{$thnakdmk->nama}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <select class="form-control show-tick" name="dosen" id="dosen" class="mar-bot">
                        <option value="">-- Pilih Dosen Wali --</option>
                        @foreach($dosen as $dosens)
                            <option value="{{$dosens->id}}">{{$dosens->nama}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <select class="form-control show-tick" name="mahasiswa" id="mahasiswa" class="mar-bot">
                        <option value="">-- Pilih NAMA MAHASISWA --</option>
                        @foreach($mahasiswa as $mahasiswas)
                            <option value="{{$mahasiswas->id}}">{{$mahasiswas->nama}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="col-sm-12" id="wrap-view-detail-krs" style="display: none;">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-6">Nama</div>
                                <div class="col-sm-6">
                                    <span>:</span>
                                    <span id="nama-krs"></span>
                                </div>
                                <div class="col-sm-6">Nim</div>
                                <div class="col-sm-6">
                                    <span>:</span>
                                    <span id="nim-krs"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-6">Tahun Akademik</div>
                                <div class="col-sm-6">
                                    <span>:</span>
                                    <span id="thn-krs"></span>
                                </div>
                                <div class="col-sm-6">Semester</div>
                                <div class="col-sm-6">
                                    <span>:</span>
                                    <span id="semester-krs"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                      <div class="body table-responsive table-bordered">
                          <table class="table">
                            <thead>
                                <tr id="tr-detail-krs">
                                    <th>Matakuliah</th>
                                    <th>Semester</th>
                                    <th>SKS</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                    <th>Matakuliah</th>
                                    <th>Semester</th>
                                    <th>SKS</th>
                                </tr>
                            </tfoot> -->
                            <tbody id="view-detail-krs"></tbody>
                          </table>
                      </div>
                      <div class="row">
                          <div class="col-sm-12 text-center" style="margin-top:10px">
                              <span>Total SKS : </span>  
                              <span id='total-sks-view'></span>
                          </div>
                      </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <input type="submit" class="btn btn-danger" value="Hapus" id="delete-krs" style="display: none;">
              <input type="submit" class="btn btn-primary" id="action" value="Simpan">
              <a href="" class="btn btn-primary" style="display: none;" id="print" target="_blank">Print</a>
            </div>

          </div>
        </div>
      </div>  
      <!-- End Modal -->

      <!-- Modal Message -->
      <div class="modal" id="modal-message" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-md" role="document" id="modal-dialog-detail-krs">
            <div class="modal-content" id="modal-content-detail-krs">
              <div id='icon-content'><i class="material-icons" id="icon-done">done</i></div>
              <h4 class="modal-title" style="margin-bottom: 15px; color: #575757;"></h4>
              <input type="submit" class="btn btn-success" id="btn-message">
            </div>
          </div>
      </div>
      <!-- End Modal Message -->


      <!-- Modal Detail Krs -->
        <div class="modal fade" id="modal-detail-krs" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="modal-title-detail-krs" >Modal title</h4>
            </div>
            <div class="modal-body">
              <div class="row clearfix">
                <form method="post" id="modal-form-detail-krs">
                  {{csrf_field()}} {{method_field('post')}}                 
                  <div class="body table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr >
                                    <th>#</th>
                                    <th>Matakuliah</th>
                                    <th>SKS</th>
                                    <th>Semester</th>
                                </tr>
                            </thead>
                            <tbody id="body-input-krs">
                                
                            </tbody>
                            <tfoot>
                                <th>#</th>
                                <th>Matakuliah</th>
                                <th>SKS</th>
                                <th>Semester</th>
                            </tfoot>
                        </table>
                    </div>
                </form>
              </div>
              <div class="total-sks-wrap" style="text-align: center;">
                  <b>Total</b> : <input type="text" style="border: none;border-bottom:1px solid gray; width:25px; background-color: white; text-align: center;" class="total-sks-value" value="0" disabled>
              </div>
            
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <input type="submit" class="btn btn-primary" value="Simpan" id="action-detail-krs">
            </div>

          </div>
        </div>
      </div>
      <!-- End Modal Detail KRS -->


    </div>
</section>
@endsection

@section('script-plugins')
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <!-- SweetAlert Plugin Js -->
    <script src="plugins/sweetalert/sweetalert.min.js"></script>
@endsection

@section('script-custom')
    @parent
    <script src="js/pages/tables/jquery-datatable.js"></script>
    <script src="js/pages/ui/notifications.js"></script>
@endsection

@section('script-bottom')
    <script type="text/javascript">  
        $(document).ready(function(){

            

            var table = $('#krs-table').DataTable({
                processing : true,
                serverSide : true,
                ajax       : "{{ route('krs.data') }}",
                order      :  [[ 0, 'desc' ], ],  
                aLengthMenu: [[ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ]],                              
                columns:[
                    // {data: 'id', name: 'id', visible: false, searchable: false},                                 
                    {data: 'Nama'},
                    {data:'tahun_akademik'},
                    // {data: 'nama', name: 'nama'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            })

            getJadwal();  


            $('#action').on('click',function () {
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('krs') }}";
                else url = "{{ url('fakultas') . '/' }}" + id;
                $.ajax({
                    url:url,
                    type:'POST',
                    data:$('#modal-form').serialize(),
                    success:function (data) {
                        // console.log(data)
                        console.log(data)
                        var krs_id=data.data['id'];
                        $('#modal-form-detail-krs').prepend('<input class="krs_id" type="hidden" name="krs_id" value="'+krs_id+'">')
                        $('#modal-form')[0].reset();                        
                        // table.ajax.reload();
                        $('#btn-message').val('Klik disini Untuk Pengisian KRS')
                        $('.modal-title').text(data.message)
                        $('#modal').modal('hide');
                        $('#modal-message').modal({backdrop:'static'})
                        $('#modal-message').modal('show');
                        // swal({
                        //     title: 'Berhasil!',
                        //     text: data.message,
                        //     type: 'success',
                        //     timer: '3000'
                        // })

                        // swal({
                        //     title: data.message,
                            // text:"Apakah Anda Akan Menigisi KRS?",
                            // type: "success",
                            // showCancelButton: false,
                            // confirmButtonColor: "#DD6B55",
                            // confirmButtonText: "Klik Untuk mengisi KRS",
                            // cancelButtonText: "Tidak!",
                            // closeOnConfirm: false,
                            // closeOnCancel: false 

                        //     },
                        //     function () {

                        //         $('#modal-detail-krs').modal('show');
                        //         $("#action-detail-krs").on('click',function () {
                        //             // console.log('krs id adalah :'+krs_id)

                        //             $.ajax({
                        //                 url:"{{url('detail-krs')}}",
                        //                 type:"post",
                        //                 data:$('#modal-form-detail-krs').serialize(),
                        //                 success:function(data){
                        //                     $('#modal-form-detail-krs')[0].reset()
                        //                     $('.check_matkul').attr('checked',false)
                        //                     // krs_id='';
                        //                     console.log(data)
                        //                     $('#modal-detail-krs').modal('hide')
                        //                     $('.krs_id').remove()

                        //                 }
                        //             })
                        //         })
                                
                        //     }

                        // )
                        

                    },
                    error:function (data) {
                        dataResponseJson = data.responseJSON;
                        errors = dataResponseJson.errors
                        $.each( errors , function( key, value ) {
                            $.notify({
                                // options
                                message: value
                            },{
                                // settings
                                type: 'danger',
                                z_index: 9999,
                                delay: 3000,
                                placement: {
                                    from: "top",
                                    align: "center"
                                },                                
                            });
                        });                       
                    }

                })
            })

            $('#btn-message').on('click',function () {
                $('#modal-message').modal('hide')
                $('#modal-detail-krs').modal('show');
                $('#modal-title-detail-krs').text('Pilih Matakuliah');
                             
            })

            $("#action-detail-krs").on('click',function () {
                $.ajax({
                    url:"{{url('detail-krs')}}",
                    type:"post",
                    data:$('#modal-form-detail-krs').serialize(),
                    success:function(data){
                        $('#modal-form-detail-krs')[0].reset()
                        $('.check_matkul').attr('checked',false)
                        console.log(data)
                        $('#modal-detail-krs').modal('hide')
                        $('.krs_id').remove()

                    }
                })
            })

            $('#delete-krs').on('click',function () {
                var csrf_token = $('meta[name="csrf-token"]').attr('content');
                var checkbox_id=$('.delete-id-detail-krs')
                var data_id=[];
                for(let i=0; i<checkbox_id.length;i++){
                    if (checkbox_id[i].checked) {
                        data_id[i]=checkbox_id[i].getAttribute('data-sks');
                    }
                    
                }
                $.ajax({
                    url : "{{ url('detail-krs') }}",
                    type : "POST",
                    data : {'_method' : 'DELETE', '_token' : csrf_token, 'id':data_id},
                    success : function(data) {
                        swal({
                            title: 'Berhasil!',
                            text: data.message,
                            type: 'success',
                            timer: '3000'
                        }) 
                        console.log(data)               
                        $('#krs-table').DataTable().ajax.reload();
                    },
                    error:function(xhr) {
                            var err = JSON.parse(xhr.responseText);
                            alert(err.message);
                            console.log(err.message)
                        }  
                });
                // console.log(data_id)
            })


    });

    var url='';
    function addForm() {
        save_method = "add";
        $('#print').css('display','none')
        $('.col-sm-4').css('display','block') 
        $('#wrap-view-detail-krs').css('display','none')
        // $('.check_matkul').attr('checked',false)
        $('#tahun_akademik').selectpicker('deselectAll')
        $('#dosen').selectpicker('deselectAll')
        $('#mahasiswa').selectpicker('deselectAll')
        $('#modal-form .form-group .form-line').removeClass('focused');          
        $('#modal').modal('show');
        $('#id').val("");
        $('#kode').val("");
        $('#nama').val("");
        $('input[name=_method').val('POST');
        $('#modal-form')[0].reset();            
        $('#modal-title').text('Tambah Mahasiswa Untuk Mengambil Krs'); 
        $('#action').val('Simpan');                                                
    }

    function viewData(id) {
        var sks=0;
        var tr='';
        var href="{{ url('detail-krs')}}";
        $('#wrap-view-detail-krs').show()
        $('#action').css('display','none');
        $('#print').css('display','inline-block')
        $('#thadded').remove();
        $('#delete-krs').css('display','none')
        $('#modal').modal('show');
        $('#modal-title').text('KRS sudah diambil'); 
        $('.col-sm-4').css('display','none')
        $.ajax({
            url:"{{url('detail-krs')}}"+"/"+id,
            type:"get",
            dataType:"JSON",
            success:function (data) {
                $('#print').attr('href',href+'/'+data[0]['krs_id']+'/pdf')
                $('#nama-krs').text(data[0]['mahasiswa_nama'])
                $('#nim-krs').text(data[0]['mahasiswa_nim'])
                $('#thn-krs').text(data[0]['thn_akademik'])
                $('#semester-krs').text(data[0]['matkul_semester'])
                for(var i=0; i<data.length; i++){
                    
                   tr+='<tr>'
                    tr+='<td>'+data[i]['matkul_nama']+'</td>';
                    tr+='<td>'+data[i]['matkul_semester']+'</td>';
                    tr+='<td>'+data[i]['matkul_sks']+'</td>';
                tr+='</tr>' 

                sks+=data[i]['matkul_sks']

                }
                $('#total-sks-view').text(sks)
                console.log(sks);
                $('#view-detail-krs').html(tr)
                // console.log(data)
            },
            // error:function(xhr) {
            //             var err = JSON.parse(xhr.responseText);
            //             alert(err.message);
            //             console.log(err.message)
            //         }
        })
    }     

    function editData(id) {
        var tr='';
        $('#print').css('display','none')
        $('#delete-krs').css('display','inline-block')
        $('#thadded').remove()
        $('#modal').modal('show');
        $('#modal-title').text('Hapus Matakuliah Yang Sudah Diambil'); 
        $('.col-sm-4').css('display','none')
        $.ajax({
            url:"{{url('detail-krs')}}"+"/"+id,
            type:"get",
            dataType:"JSON",
            success:function (data) {
                $('#tr-detail-krs').prepend('<th id="thadded">#</th>')

                for(var i=0; i<data.length; i++){
                   tr+='<tr>'
                    tr+='<td>'+'<input type="checkbox" data-sks="'+data[i]['id_detail_krs']+'" name="check_matkul[]" style="position:static;left:0;opacity:1;" class="delete-id-detail-krs">'
                    tr+='<td>'+data[i]['matkul_nama']+'</td>';
                    tr+='<td>'+data[i]['matkul_semester']+'</td>';
                    tr+='<td>'+data[i]['matkul_sks']+'</td>';
                tr+='</tr>' 
                }
                $('#view-detail-krs').html(tr)
                $('#wrap-view-detail-krs').show()
                console.log(data)
            },
            // error:function(xhr) {
            //             var err = JSON.parse(xhr.responseText);
            //             alert(err.message);
            //             console.log(err.message)
            //         }
        })      
    }

    function deleteData(id){
        $('#print').css('display','none')
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
                title: "Apa Anda Yakin Akan Menghapus Data ini?",
                text:"Data yang sudah dihapus tidak bisa dikembalikan!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Saya Yakin!",
                cancelButtonText: "Tidak!",
                closeOnConfirm: true,
                closeOnCancel: true 
            },
            function () {
                $.ajax({
                url : "{{ url('krs') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : csrf_token},
                success : function(data) {
                    swal({
                        title: 'Berhasil!',
                        text: data.message,
                        type: 'success',
                        timer: '3000'
                    }) 
                    console.log(data)               
                    $('#krs-table').DataTable().ajax.reload();
                },
                error:function(xhr) {
                        var err = JSON.parse(xhr.responseText);
                        alert(err.message);
                        console.log(err.message)
                    }  
              });
            }
        )
    }  


    function getJadwal() {
        var tr='';
       $.ajax({
                url:"{{route('detail-krs.jadwal')}}",
                type:'get',
                dataType:'JSON',
                success:function (data) {

                    console.log(data)
                    for(let i=0; i<data.length; i++){
                        tr+='<tr>';
                            tr+='<td><input class="check_matkul" data-sks="'+data[i]['matkul_sks']+'" type="checkbox" name="check_matkul[]" value="'+data[i]['id_jadwal']+'" style="left:auto; opacity:1; position: static;" onclick="checkbox( this );"></td>';
                            tr+='<td>'+data[i]['matkul_nama']+'</td>';
                            tr+='<td>'+data[i]['matkul_sks']+'</td>';
                            tr+='<td>'+data[i]['matkul_semester']+'</td>';
                        tr+='</tr>';
                    }
                    console.log(tr);
                    $('#body-input-krs').html(tr);
                }
                // error:function(xhr) {
                //         var err = JSON.parse(xhr.responseText);
                //         alert(err.message);
                //         console.log(err.message)
                //     }  
            })
       
    }

    var total=0;
    function checkbox(a) {
        
        // var check=document.querySelectorAll('.check_matkul');
        // for (var i = 0; i < check.length; i++) {
        //     console.log(check[i])
        // }
        var z=$(a).attr('data-sks')
        var value=parseInt(z)
        if (a.checked) 
            total+=value; 
        else 
            total-=value;
        $('.total-sks-value').val(total)

    }  




    
    </script>    
@endsection  