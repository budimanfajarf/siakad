<?php

use Illuminate\Database\Seeder;

class FakultasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fakultas')->insert([
            [
                'kode' => 'F01',            
                'nama' => 'Teknik',
            ],
            [
                'kode' => 'F02',            
                'nama' => 'MIPA',
            ]            
        ]);
    }
}
