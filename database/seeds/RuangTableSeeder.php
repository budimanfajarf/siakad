<?php

use Illuminate\Database\Seeder;

class RuangTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ruang')->insert([
            [            
                'nama' => 'Ruang 1',
                'kapasitas' => 40,
            ],
            [
                'nama' => 'Ruang 2',
                'kapasitas' => 50,
            ]            
        ]);
    }
}
