<?php

use Illuminate\Database\Seeder;

class AssesmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assesment')->insert([
            [
                'metode'  => 'Quiz',            
                'bobot' => 10,                
                'learning_outcome_id' => 1,                
            ],
            [
                'metode'  => 'Tugas',            
                'bobot' => 15,                
                'learning_outcome_id' => 1,                
            ],            
            [
                'metode'  => 'Tugas',            
                'bobot' => 20,                
                'learning_outcome_id' => 2,                
            ], 
            [
                'metode'  => 'UTS',            
                'bobot' => 20,                
                'learning_outcome_id' => 3,                
            ],
            [
                'metode'  => 'Tugas',            
                'bobot' => 15,                
                'learning_outcome_id' => 3,                
            ],            
            [
                'metode'  => 'UAS',            
                'bobot' => 20,                
                'learning_outcome_id' => 4,                
            ],                        
        ]);
    }
}
