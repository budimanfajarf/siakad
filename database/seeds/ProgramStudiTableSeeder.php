<?php

use Illuminate\Database\Seeder;

class ProgramStudiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('program_studi')->insert([
            [
                'kode' => 'T01',            
                'nama' => 'Teknik Industri',
                'fakultas_id' => 1,                
            ],
            [
                'kode' => 'T02',            
                'nama' => 'Teknik Mesin',
                'fakultas_id' => 1,                                
            ]
        ]);
    }
}
