<?php

use Illuminate\Database\Seeder;

class JadwalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jadwal')->insert([
            [
                'kelas' => 'A',
                'hari' => 'Senin',
                'jam_mulai' => '07:00',
                'jam_selesai' => '08:00',
                'semester' => 1,
                'matakuliah_id' => 1,                
                'dosen_id' => 1, 
                'tahun_akademik_id' => 1,                                                
                'ruang_id' => 1,                                                
            ],
            [
                'kelas' => 'A',
                'hari' => 'Senin',
                'jam_mulai' => '09:00',
                'jam_selesai' => '10:00',
                'semester' => 1,                
                'matakuliah_id' => 2,                
                'dosen_id' => 2, 
                'tahun_akademik_id' => 1,                                                
                'ruang_id' => 1,                                                
            ],                                                                        
        ]);
    }
}
