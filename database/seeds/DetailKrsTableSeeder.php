<?php

use Illuminate\Database\Seeder;

class DetailKrsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('detail_krs')->insert([
            [                
                'krs_id' => 1, 
                'jadwal_id' => 1,                                                
            ],
            [                
                'krs_id' => 1, 
                'jadwal_id' => 2,                                                
            ],
            [                
                'krs_id' => 2, 
                'jadwal_id' => 1,                                                
            ],
            [                
                'krs_id' => 2, 
                'jadwal_id' => 2,                                                
            ],             
        ]);
    }
}
