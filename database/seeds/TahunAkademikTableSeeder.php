<?php

use Illuminate\Database\Seeder;

class TahunAkademikTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tahun_akademik')->insert([
            [
                'nama'  => '2016/2017',            
                'tanggal_awal' => '2016-08-09',
                'tanggal_akhir' => '2017-08-10',                
                'program_studi_id' => 1,                
            ],
            [
                'nama'  => '2017/2018',            
                'tanggal_awal' => '2017-08-09',
                'tanggal_akhir' => '2018-08-10',                
                'program_studi_id' => 1,                                    
            ]
        ]);
    }
}
