<?php

use Illuminate\Database\Seeder;

class MatakuliahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('matakuliah')->insert([
            [
                'kode' => 'TI1001',            
                'nama' => 'Pengantar Teknik Industri',
                'sks' => 3,
                'semester' => 1,
                'program_studi_id' => 1,                
            ],
            [
                'kode' => 'TI1005',            
                'nama' => 'Tata Tulis Laporan Ilmiah',
                'sks' => 2,
                'semester' => 1,
                'program_studi_id' => 1,                
            ],         
            [
                'kode' => 'TI1022',            
                'nama' => 'Mekanika Teknik',
                'sks' => 2,
                'semester' => 2,
                'program_studi_id' => 1,                
            ],
            [
                'kode' => 'TI1034',            
                'nama' => 'Dasar Perancangan Teknik',
                'sks' => 2,
                'semester' => 2,
                'program_studi_id' => 1,                
            ],
            [
                'kode' => 'TI1052',            
                'nama' => 'Dasar Analisis Sistem Kerja',
                'sks' => 3,
                'semester' => 2,
                'program_studi_id' => 1,                
            ],                                                            
        ]);
    }
}
