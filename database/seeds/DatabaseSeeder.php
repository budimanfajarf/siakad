<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);        
        $this->call(FakultasTableSeeder::class);
        $this->call(ProgramStudiTableSeeder::class);        
        $this->call(MahasiswaTableSeeder::class);        
        $this->call(DosenTableSeeder::class);        
        $this->call(TahunAkademikTableSeeder::class); 
        $this->call(StudyOutcomeTableSeeder::class);
        $this->call(MatakuliahTableSeeder::class); 
        $this->call(RuangTableSeeder::class); 
        $this->call(JadwalTableSeeder::class);
        $this->call(LearningOutcomeTableSeeder::class); 
        $this->call(AssesmentTableSeeder::class);
        $this->call(KrsTableSeeder::class); 
        $this->call(DetailKrsTableSeeder::class); 
        $this->call(NilaiAssesmentTableSeeder::class);                                        
    }
}
