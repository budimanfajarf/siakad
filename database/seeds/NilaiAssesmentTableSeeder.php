<?php

use Illuminate\Database\Seeder;

class NilaiAssesmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nilai_assesment')->insert([
            [
                'nilai' => 95.5,
                'detail_krs_id' => 1,                
                'assesment_id' => 1, 
            ],
            [
                'nilai' => 90.4,
                'detail_krs_id' => 1,                
                'assesment_id' => 2, 
            ],                                                                        
            [
                'nilai' => 70.7,
                'detail_krs_id' => 1,                
                'assesment_id' => 3, 
            ], 
            [
                'nilai' => 70.3,
                'detail_krs_id' => 1,                
                'assesment_id' => 4, 
            ],
            [
                'nilai' => 80.2,
                'detail_krs_id' => 1,                
                'assesment_id' => 5, 
            ],                                                                        
            [
                'nilai' => 70.3,
                'detail_krs_id' => 1,                
                'assesment_id' => 6, 
            ],                                                                                    
        ]);
    }
}
