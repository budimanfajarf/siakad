<?php

use Illuminate\Database\Seeder;

class StudyOutcomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('study_outcome')->insert([
            [
                'kode'  => 'SO1',            
                'deskripsi' => 'Menguasai prinsip sistem terintegrasi dengan pendekatan sistem',                
                'program_studi_id' => 1,                
            ],
            [
                'kode'  => 'SO2',            
                'deskripsi' => 'Mampu mengidentifikasi, memformulasikan dan menganalisis masalah rekayasa kompleks pada sistem terintegrasi berdasarkan analitik, komputasional atau eksperimental',                
                'program_studi_id' => 1,                
            ],
            [
                'kode'  => 'SO3',            
                'deskripsi' => 'Mampu meneliti dan menyelidiki masalah rekayasa kompleks pada sistem terintegrasi menggunakan dasar prinsip-prinsip rekayasa dan dengan melaksanakan riset, analisis, interpretasi data dan sintesa informasi untuk memberikan solusi',                
                'program_studi_id' => 1,                
            ],
        ]);
    }
}
