<?php

use Illuminate\Database\Seeder;

class LearningOutcomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('learning_outcome')->insert([
            [
                'kode'  => 'LO1',            
                'deskripsi' => 'Learning Outcome Menguasai prinsip sistem terintegrasi dengan pendekatan sistem',                
                'matakuliah_id' => 1,                
                'study_outcome_id' => 1,                
            ],
            [
                'kode'  => 'LO2',            
                'deskripsi' => 'Learning Outcome Menguasai prinsip sistem terintegrasi dengan pendekatan bukan sistem',                
                'matakuliah_id' => 1,                
                'study_outcome_id' => 1,                
            ],            
            [
                'kode'  => 'LO3',            
                'deskripsi' => 'Learning Outcome Mampu mengidentifikasi, memformulasikan dan menganalisis masalah rekayasa kompleks pada sistem terintegrasi berdasarkan analitik, komputasional atau eksperimental',                
                'matakuliah_id' => 1,                
                'study_outcome_id' => 2,                
            ],
            [
                'kode'  => 'LO4',            
                'deskripsi' => 'Learning Outcome Mampu meneliti dan menyelidiki masalah rekayasa kompleks pada sistem terintegrasi menggunakan dasar prinsip-prinsip rekayasa dan dengan melaksanakan riset, analisis, interpretasi data dan sintesa informasi untuk memberikan solusi',                
                'matakuliah_id' => 1,                
                'study_outcome_id' => 3,                
            ],
        ]);
    }
}
