<?php

use Illuminate\Database\Seeder;

class KrsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('krs')->insert([
            [
                'mahasiswa_id' => 1,                
                'dosen_id' => 1, 
                'tahun_akademik_id' => 1,                                                
            ],
            [
                'mahasiswa_id' => 2,                
                'dosen_id' => 2, 
                'tahun_akademik_id' => 1,                                                
            ],                                                                        
        ]);
    }
}
