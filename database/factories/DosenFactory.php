<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Dosen::class, function (Faker $faker) {
    return [
        'nidn' => $faker->randomNumber(9),
        'nama' => $faker->name,
        'alamat' => $faker->address,
        'program_studi_id' => 1,
    ];
});
