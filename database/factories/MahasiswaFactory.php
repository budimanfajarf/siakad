<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Mahasiswa::class, function (Faker $faker) {
    return [
        'nim' => $faker->randomNumber(9),
        'nama' => $faker->name,
        'alamat' => $faker->address,       
        'program_studi_id' => 1,
    ];
});
