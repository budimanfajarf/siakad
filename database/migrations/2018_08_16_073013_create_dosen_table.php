<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosen', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('nidn', 20)->unique();
            $table->string('nama', 50);
            $table->string('alamat');            
            $table->tinyInteger('program_studi_id')->unsigned(); 
            $table->foreign('program_studi_id')->references('id')->on('program_studi');                         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosen');
    }
}
