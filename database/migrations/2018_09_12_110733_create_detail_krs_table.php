<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailKrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_krs', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->mediumInteger('krs_id')->unsigned(); 
            $table->foreign('krs_id')->references('id')->on('krs')->onDelete('cascade');               
            $table->smallInteger('jadwal_id')->unsigned(); 
            $table->foreign('jadwal_id')->references('id')->on('jadwal');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_krs');
    }
}
