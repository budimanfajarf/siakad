<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal', function (Blueprint $table) {
            $table->smallIncrements('id');            
            $table->string('kelas',25);
            $table->string('hari',15);
            $table->string('jam_mulai',15);
            $table->string('jam_selesai',15);
            $table->tinyInteger('semester');
            $table->smallInteger('matakuliah_id')->unsigned(); 
            $table->foreign('matakuliah_id')->references('id')->on('matakuliah');  
            $table->smallInteger('dosen_id')->unsigned(); 
            $table->foreign('dosen_id')->references('id')->on('dosen');  
            $table->tinyInteger('tahun_akademik_id')->unsigned(); 
            $table->foreign('tahun_akademik_id')->references('id')->on('tahun_akademik');                                      
            $table->tinyInteger('ruang_id')->unsigned();
            $table->foreign('ruang_id')->references('id')->on('ruang');
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal');
    }
}
