<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyOutcomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_outcome', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('kode', 20)->unique();
            $table->text('deskripsi');
            $table->tinyInteger('program_studi_id')->unsigned(); 
            $table->foreign('program_studi_id')->references('id')->on('program_studi');                          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('study_outcome');
    }
}
