<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNilaiAssesmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_assesment', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->decimal('nilai', 5, 2)->nullable()->unsigned();
            $table->mediumInteger('detail_krs_id')->unsigned(); 
            $table->foreign('detail_krs_id')->references('id')->on('detail_krs')->onDelete('cascade');  
            $table->mediumInteger('assesment_id')->unsigned(); 
            $table->foreign('assesment_id')->references('id')->on('assesment')->onDelete('cascade');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_assesment');
    }
}
