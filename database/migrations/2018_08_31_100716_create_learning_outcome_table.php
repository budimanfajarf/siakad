<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLearningOutcomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learning_outcome', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('kode', 20);
            $table->text('deskripsi');
            $table->smallInteger('matakuliah_id')->unsigned(); 
            $table->foreign('matakuliah_id')->references('id')->on('matakuliah');              
            $table->smallInteger('study_outcome_id')->unsigned(); 
            $table->foreign('study_outcome_id')->references('id')->on('study_outcome');              
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('learning_outcome');
    }
}
