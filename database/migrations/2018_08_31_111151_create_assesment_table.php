<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssesmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesment', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('metode', 50);
            $table->tinyInteger('bobot')->nullable()->unsigned();
            $table->smallInteger('learning_outcome_id')->unsigned(); 
            $table->foreign('learning_outcome_id')->references('id')->on('learning_outcome');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesment');
    }
}
