<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function(){

    Route::get('/', function () {
        $count['study_outcome'] = App\Models\StudyOutcome::count();
        $count['tahun_akademik'] = App\Models\TahunAkademik::count();
        $count['matakuliah'] = App\Models\Matakuliah::count();
        $count['dosen'] = App\Models\Dosen::count();
        $count['mahasiswa'] = App\Models\Mahasiswa::count();
        $count['ruang'] = App\Models\Ruang::count();        
        $count['jadwal'] = App\Models\Jadwal::count();
        $count['krs'] = App\Models\Krs::count();

        return view('dashboard', compact('count'));
    })->name('dashboard');    

    Route::group(['prefix' => 'dosen', 'as' => 'dosen.'], function () {
        Route::get('/', 'DosenController@index')->name('index');
        Route::post('/', 'DosenController@store')->name('store');    
        Route::get('/{id}/edit','DosenController@edit')->name('edit');
        Route::get('/data', 'DosenController@data')->name('data');     
        Route::put('/{id}', 'DosenController@update')->name('update'); 
        Route::delete('/{id}', 'DosenController@destroy')->name('destroy');        
    });

    Route::group(['prefix' => 'mahasiswa', 'as' => 'mahasiswa.'], function () {
        Route::get('/', 'MahasiswaController@index')->name('index');
        Route::post('/', 'MahasiswaController@store')->name('store');    
        Route::get('/{id}/edit','MahasiswaController@edit')->name('edit');
        Route::get('/data', 'MahasiswaController@data')->name('data');
        Route::get('/{id}/study-outcome', 'MahasiswaController@study_outcome_index')->name('study-outcome.index');     
        Route::get('/{id}/study-outcome/pdf', 'MahasiswaController@study_outcome_pdf')->name('study-outcome.pdf');     
        Route::put('/{id}', 'MahasiswaController@update')->name('update'); 
        Route::delete('/{id}', 'MahasiswaController@destroy')->name('destroy');        
    });

    Route::group(['prefix' => 'tahun-akademik', 'as' => 'tahun-akademik.'], function () {
        Route::get('/', 'TahunAkademikController@index')->name('index');
        Route::post('/', 'TahunAkademikController@store')->name('store');    
        Route::get('/{id}/edit','TahunAkademikController@edit')->name('edit');
        Route::get('/data', 'TahunAkademikController@data')->name('data');     
        Route::put('/{id}', 'TahunAkademikController@update')->name('update'); 
        Route::delete('/{id}', 'TahunAkademikController@destroy')->name('destroy');        
    });

    Route::group(['prefix' => 'study-outcome', 'as' => 'study-outcome.'], function () {
        Route::get('/', 'StudyOutcomeController@index')->name('index');    
        Route::get('/data', 'StudyOutcomeController@data')->name('data');         
        Route::get('/{id}/edit','StudyOutcomeController@edit')->name('edit');
        Route::post('/', 'StudyOutcomeController@store')->name('store');    
        Route::put('/{id}', 'StudyOutcomeController@update')->name('update'); 
        Route::delete('/{id}', 'StudyOutcomeController@destroy')->name('destroy');        
    });

    Route::group(['prefix' => 'matakuliah', 'as' => 'matakuliah.'], function () {
        Route::get('/', 'MatakuliahController@index')->name('index');    
        Route::get('/data', 'MatakuliahController@data')->name('data');
        Route::get('/{id}/edit','MatakuliahController@edit')->name('edit');
        Route::post('/', 'MatakuliahController@store')->name('store');    
        Route::put('/{id}', 'MatakuliahController@update')->name('update'); 
        Route::delete('/{id}', 'MatakuliahController@destroy')->name('destroy');  

        Route::get('/{matakuliah_id}/learning-outcome/pdf', 'LearningOutcomeController@pdf')->name('learning-outcome.pdf'); 
        Route::get('/{matakuliah_id}/learning-outcome', 'LearningOutcomeController@index')->name('learning-outcome.index'); 
        Route::group(['prefix' => '/learning-outcome', 'as' => 'learning-outcome.'], function () {
            Route::post('/', 'LearningOutcomeController@store')->name('store'); 
            Route::put('/{id}', 'LearningOutcomeController@update')->name('update'); 
            Route::delete('/{id}', 'LearningOutcomeController@destroy')->name('destroy');

            Route::group(['prefix' => '/assesment', 'as' => 'assesment.'], function () {
                Route::post('/', 'AssesmentController@store')->name('store'); 
                Route::put('/bobot', 'AssesmentController@bobot_bulk_update')->name('bobot.bulk.update');     
                Route::put('/{id}', 'AssesmentController@update')->name('update');
                Route::delete('/{id}', 'AssesmentController@destroy')->name('destroy');        
            });        
        });          

        Route::get('/{matakuliah_id}/nilai-assesment', 'NilaiAssesmentController@matakuliah')->name('nilai-assesment.index');
        Route::put('/{matakuliah_id}/nilai-assesment/nilai', 'NilaiAssesmentController@matakuliah_nilai_bulk_update')->name('nilai-assesment.nilai.bulk.update');    
        Route::get('/{matakuliah_id}/nilai-assesment/pdf', 'NilaiAssesmentController@matakuliah_pdf')->name('nilai-assesment.pdf');        
    });            

    Route::group(['prefix' => 'jadwal', 'as' => 'jadwal.'], function () {
        Route::get('/{jadwal_id}/nilai-assesment', 'NilaiAssesmentController@index')->name('nilai-assesment.index');
        Route::put('/{jadwal_id}/nilai-assesment/nilai', 'NilaiAssesmentController@nilai_bulk_update')->name('nilai-assesment.nilai.bulk.update');    
        Route::get('/{jadwal_id}/nilai-assesment/pdf', 'NilaiAssesmentController@pdf')->name('nilai-assesment.pdf');                
        
        Route::get('/', 'JadwalController@index')->name('index');
        Route::post('/', 'JadwalController@store')->name('store');    
        // Route::get('/{id}/edit','TahunAkademikController@edit')->name('edit');
        Route::get('/bulkdelete','JadwalController@bulkdelete')->name('jadwal.bulkdelete');
        Route::get('/{id}/select','JadwalController@select')->name('select');
        Route::get('/{idt}/{idk}/{ids}/getview', 'JadwalController@getview')->name('getview'); 
        Route::get('/data', 'JadwalController@data')->name('data'); 
        Route::put('/', 'JadwalController@bulkupdate')->name('bulkupdate'); 
        // Route::put('/{id}', 'JadwalController@update')->name('update'); 
        Route::delete('/{id}', 'TahunAkademikController@destroy')->name('destroy');        
    });

    Route::group(['prefix' => 'ruang', 'as' => 'ruang.'], function () {
        Route::get('/', 'RuangController@index')->name('index');
        Route::post('/', 'RuangController@store')->name('store');    
        Route::get('/{id}/edit','RuangController@edit')->name('edit');
        Route::get('/data', 'RuangController@data')->name('data');     
        Route::put('/{id}', 'RuangController@update')->name('update'); 
        Route::delete('/{id}', 'RuangController@destroy')->name('destroy');        
    });

    Route::group(['prefix' => 'krs', 'as' => 'krs.'], function () {
        Route::get('/', 'KrsController@index')->name('index');
        Route::post('/', 'KrsController@store')->name('store');    
        Route::get('/{id}/edit','KrsController@edit')->name('edit');
        Route::get('/data', 'KrsController@data')->name('data');     
        Route::put('/{id}', 'KrsController@update')->name('update'); 
        Route::delete('/{id}', 'KrsController@destroy')->name('destroy');        
    });

    Route::group(['prefix' => 'detail-krs', 'as' => 'detail-krs.'], function () {
        Route::get('/', 'DetailkrsController@index')->name('index');
        Route::post('/', 'DetailkrsController@store')->name('store'); 
        Route::get('/{id}/pdf', 'DetailkrsController@pdf')->name('pdf');   
        Route::get('/{id}/edit','DetailkrsController@edit')->name('edit');
        Route::get('/data', 'DetailkrsController@data')->name('data');
        Route::get('/jadwal', 'DetailkrsController@jadwal')->name('jadwal'); 
        Route::get('/{id}', 'DetailkrsController@show')->name('show');
        Route::put('/{id}', 'DetailkrsController@update')->name('update'); 
        Route::delete('/', 'DetailkrsController@destroy')->name('destroy');        
    });

});

Route::get('pdf/print', function ()
        {
            return view('krs.print');
        });
